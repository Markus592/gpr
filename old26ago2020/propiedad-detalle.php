<?php
	switch ($_REQUEST['vivienda']) {
		case 'misti':
				define('og_image', 'images/misti-11.jpg');
				define('og_title', '[Casa Misti] - Las Lomas de Yura -GPR Inmobiliaria Arequipa');
				define('og_type','website');
				define('og_desc','Continuamos con el proyecto
				En Las Lomas de Yura seguimos avanzando con nuestro proyecto, instalando campamentos para 	verificar y 	monitorear el proceso de construcción. Estamos muy orgullosos y felices de 	trabajar de manera 	responsable y consecuente.');
				define('keywords',',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
				define('title_complemento','Inmobiliaria');
			
				break;		
		case 'aleli':
				define('og_image', 'images/aleli-11.jpg');
				define('og_title', '[Casa Aleli] - Las Lomas de Yura -GPR Inmobiliaria Arequipa');
				define('og_type','website');
				define('og_desc','Continuamos con el proyecto
				En Las Lomas de Yura seguimos avanzando con nuestro proyecto, instalando campamentos para verificar y 	monitorear el proceso de construcción. Estamos muy orgullosos y felices de trabajar de manera 	responsable y consecuente.');
				define('keywords',',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
				define('title_complemento','Inmobiliaria');
				break;		
		case 'capuli':
				define('og_image', 'images/capuli-11.jpg');
				define('og_title', '[Casa Capuli] - Las Lomas de Yura -GPR Inmobiliaria Arequipa');
				define('og_type','website');
				define('og_desc','Continuamos con el proyecto
				En Las Lomas de Yura seguimos avanzando con nuestro proyecto, instalando campamentos para verificar y 	monitorear el proceso de construcción. Estamos muy orgullosos y felices de trabajar de manera 	responsable y consecuente.');
				define('keywords',',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
				define('title_complemento','Inmobiliaria');	
			break;		
		case 'texao':
				define('og_image', 'images/texao-11.jpg');
				define('og_title', '[Casa Texao] - Las Lomas de Yura -GPR Inmobiliaria Arequipa');
				define('og_type','website');
				define('og_desc','Continuamos con el proyecto
				En Las Lomas de Yura seguimos avanzando con nuestro proyecto, instalando campamentos para verificar y 	monitorear el proceso de construcción. Estamos muy orgullosos y felices de trabajar de manera 	responsable y consecuente.');
				define('keywords',',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
				define('title_complemento','Inmobiliaria');	
				break;		
		case 'wititi':
				define('og_image', 'images/wititi-11.jpg');
				define('og_title', '[Casa Wititi] - Las Lomas de Yura -GPR Inmobiliaria Arequipa');
				define('og_type','website');
				define('og_desc','Continuamos con el proyecto
				En Las Lomas de Yura seguimos avanzando con nuestro proyecto, instalando campamentos para verificar y 	monitorear el proceso de construcción. Estamos muy orgullosos y felices de trabajar de manera 	responsable y consecuente.');
				define('keywords',',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
				define('title_complemento','Inmobiliaria');	
				break;		
		case 'yaravi':
				define('og_image', 'images/yaravi-11.jpg');
				define('og_title', '[Casa Yaravi] - Las Lomas de Yura -GPR Inmobiliaria Arequipa');
				define('og_type','website');
				define('og_desc','Continuamos con el proyecto
				En Las Lomas de Yura seguimos avanzando con nuestro proyecto, instalando campamentos para verificar y 	monitorear el proceso de construcción. Estamos muy orgullosos y felices de trabajar de manera 	responsable y consecuente.');
				define('keywords',',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
				define('title_complemento','Inmobiliaria');
				break;		
		default:												
			break;
	}//end switch										
?>	
<?php require 'include/config.php'; ?>
<?php require 'include/header.php'; ?>

		<section id="property-content" class="fixed-p-t-170">

			<div class="container">
				<div class="row">
					<div class="col-md-9">

						<!-- 2. Price -->
						<span class="large-price">														
							<?php
							switch ($_REQUEST['vivienda']) {
								case 'misti':
									echo '<sup><small class="little">Desde</small></sup>S/ '.GPR_PRICE_MISTI_FINAL;
									break;		
								case 'aleli':
									echo '<sup><small class="little">Desde</small></sup>S/ '.GPR_PRICE_ALELI_FINAL;
									break;		
								case 'capuli':
									echo '<sup><small class="little">Desde </small></sup>S/ '.GPR_PRICE_CAPULI_FINAL;
									break;		
								case 'texao':
									echo '<sup><small class="little">Desde </small></sup>S/.  '.GPR_PRICE_TEXAO_FINAL;
									break;		
								case 'wititi':
									echo '<sup><small class="little">Desde </small></sup>S/.  '.GPR_PRICE_WITITI_FINAL;
									break;		
								case 'yaravi':
									echo '<sup><small class="little">Desde </small></sup>S/.  '.GPR_PRICE_YARAVI_FINAL;
									break;		
								default:												
									break;
							}//end switch										
							?>	
						</span>

						<!-- 1. Images gallery -->
						<div class="fotorama" data-autoplay="3000" data-stopautoplayontouch="false" data-width="100%" data-fit="cover" data-max-width="100%" data-nav="thumbs" data-transition="crossfade">
						
							<?php
							switch ($_REQUEST['vivienda']) {
								case 'misti':
									echo '
										<img src="images/misti-11.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=misti alt="Misti">
										<img src="images/misti-12.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=misti alt="Misti">
										<img src="images/misti-13.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=misti alt="Misti">
										<img src="images/misti-14.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=misti alt="Misti">
										<img src="images/misti-15.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=misti alt="Misti">
										<img src="images/misti-16.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=misti alt="Misti">
										<img src="images/misti-17.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=misti alt="Misti">									
										<img src="images/misti-1.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=misti alt="Misti">
									';
									break;		
								case 'aleli':
									echo '
										<img src="images/aleli-11.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=aleli alt="aleli">
										<img src="images/aleli-12.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=aleli alt="aleli">
										<img src="images/aleli-13.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=aleli alt="aleli">
										<img src="images/aleli-14.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=aleli alt="aleli">
										<img src="images/aleli-15.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=aleli alt="aleli">
										<img src="images/aleli-1.jpg"  title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=aleli alt="aleli">
									';
									break;		
								case 'capuli':
									echo '
										<img src="images/capuli-11.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=capuli alt="capuli">
										<img src="images/capuli-12.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=capuli alt="capuli">
										<img src="images/capuli-13.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=capuli alt="capuli">	
										<img src="images/capuli-2_850x510.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=capuli alt="capuli">
									';
									break;		
								case 'texao':
									echo '
										<img src="images/texao-11.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=texao alt="texao">
										<img src="images/texao-12.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=texao alt="texao">
										<img src="images/texao-13.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=texao alt="texao">		
										<img src="images/texao-1_850x510.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=texao alt="texao">
									';
									break;		
								case 'wititi':
									echo '
										<img src="images/dpto-11.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=wititi alt="wititi">
										<img src="images/dpto-12.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=wititi alt="wititi">
										<img src="images/dpto-13.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=wititi alt="wititi">
										<img src="images/dpto-14.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=wititi alt="wititi">
										<img src="images/dpto-15.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=wititi alt="wititi">										
										<img src="images/wititi-1.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=wititi alt="wititi">
									';
									break;		
								case 'yaravi':
									echo '
										<img src="images/dpto-11.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=yaravi alt="yaravi">
										<img src="images/dpto-12.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=yaravi alt="yaravi">
										<img src="images/dpto-13.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=yaravi alt="yaravi">
										<img src="images/dpto-14.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=yaravi alt="yaravi">
										<img src="images/dpto-15.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=yaravi alt="yaravi">
										<img src="images/yaravi-1.jpg" title='.GPR_ROOT_PATH.'propiedad-detalle?vivienda=yaravi alt="yaravi">
									';
									break;		
								default:												
									break;
							}//end switch										
							?>							
						</div>

						<!-- /.Secondo Row -->
						<div class="row">
							<div class="col-md-4">
								<!-- 9. Mortage -->
								<div class="section-title line-style">
									<h2 class="title">Cotizar Vivienda</h2>
								</div>
								<div class="search-box-page">
									<div class="row">
										<?php require 'include/form-cotizar.php'; ?>
									</div><!-- ./row -->
								</div><!-- ./.search -->								
							</div>
							<div class="col-md-8">

								<!-- 6. Description -->
								<div class="section-title line-style">
									<h1 class="title">									
										<?php
										switch ($_REQUEST['vivienda']) {
											case 'misti':
												echo 'Casa Misti (93 m<sup>2</sup>)';
												break;		
											case 'aleli':
												echo 'Casa Aleli 78 m<sup>2</sup>';
												break;		
											case 'capuli':
												echo 'Casa Capuli 42 m<sup>2</sup>';
												break;		
											case 'texao':
												echo 'Casa Texao 35 m<sup>2</sup>';
												break;		
											case 'wititi':
												echo 'Dpto. Wititi 53 m<sup>2</sup>';
												break;		
											case 'yaravi':
												echo 'Dpto. Yaravi 46 m<sup>2</sup>';
												break;		
											default:												
												break;
										}//end switch										
										?>	
									</h1>
								</div>
							
							
								<div class="description">								
									<?php
									switch ($_REQUEST['vivienda']) {
										case 'misti':
											echo '<strong>Primer Nivel:</strong> Sala, comedor, cocina, lavandería, baño completo, 01 estudio, escaleras interiores, cochera, áreas verdes.<br /><br /><strong>Segundo Nivel:</strong> 01 dormitorio principal, 01 baño principal, 01 walking closet, 02 dormitorios secundarios, baño secundario.';
											break;		
										case 'aleli':
											echo '<strong>Primer Nivel:</strong> Sala, Comedor, Cocina, Lavandería, Baño completo, 01 Estudio, Escaleras interiores, Cochera, Áreas verdes.<br /><br /><strong>Segundo Nivel:</strong> 03 Dormitorios, Baño completo y Sala de estar.';
											break;
										case 'capuli':
											echo 'Sala, comedor, cocina, lavandería, baño completo, 02 dormitorios, terraza, cochera, áreas verdes.';
											break;		
										case 'texao':
											echo 'Sala, comedor, cocina americana, lavandería, baño completo, 02 dormitorios, terraza, cochera, áreas verdes.';
											break;		
										case 'wititi':
											echo 'Sala, comedor, cocina, lavandería, baño completo, 03 dormitorios.';
											break;		
										case 'yaravi':
											echo 'Sala, comedor, cocina, lavandería, baño completo, 02 dormitorios.';
											break;		
										default:												
											break;
									}//end switch										
									?>									
								</div>
								
								
								<!-- 6. Description -->
								<div class="section-title line-style">
									<h2 class="title">Resumen</h2>
								</div>
								<div class="description">																
									<div class="box-ads box-home">	


										<?php
										switch ($_REQUEST['vivienda']) {
											case 'misti':
												echo '
												<dl class="detail" style="min-height: 150px;">
													<dt class="area" style="background-position: left 0 top -5px;">Nro. de pisos:</dt><dd><span>2</span></dd>
													<dt class="area">Área construida:</dt><dd><span>93 m<sup>2</sup></span></dd>
													<dt class="area">Terreno:</dt><dd><span>90 m<sup>2</sup></span></dd>
													<dt class="bed">Dormitorios:</dt><dd><span>3</span></dd>
													<dt class="bath">Baños:</dt><dd><span>3</span></dd>
													<dt class="status">Cochera:</dt><dd><span>1</span></dd>
												</dl>												
												';
												break;		
											case 'aleli':
												echo '
												<dl class="detail" style="min-height: 150px;">
													<dt class="area" style="background-position: left 0 top -5px;">Nro. de pisos:</dt><dd><span>2</span></dd>
													<dt class="area">Área construida:</dt><dd><span>78 m<sup>2</sup></span></dd>
													<dt class="area">Terreno:</dt><dd><span>90 m<sup>2</sup></span></dd>
													<dt class="bed">Dormitorios:</dt><dd><span>3</span></dd>
													<dt class="bath">Baños:</dt><dd><span>2</span></dd>
													<dt class="status">Cochera:</dt><dd><span>1</span></dd>
												</dl>												
												';
												break;		
											case 'capuli':
												echo '
												<dl class="detail" style="min-height: 150px;">
													<dt class="area" style="background-position: left 0 top -5px;">Nro. de pisos:</dt><dd><span>1</span></dd>
													<dt class="area">Área construida:</dt><dd><span>42 m<sup>2</sup></span></dd>
													<dt class="area">Terreno:</dt><dd><span>90 m<sup>2</sup></span></dd>
													<dt class="bed">Dormitorios:</dt><dd><span>2</span></dd>
													<dt class="bath">Baños:</dt><dd><span>1</span></dd>
													<dt class="status">Cochera:</dt><dd><span>1</span></dd>
												</dl>												
												';
												break;		
											case 'texao':
												echo '
												<dl class="detail" style="min-height: 150px;">
													<dt class="area" style="background-position: left 0 top -5px;">Nro. de pisos:</dt><dd><span>1</span></dd>
													<dt class="area">Área construida:</dt><dd><span>35 m<sup>2</sup></span></dd>
													<dt class="area">Terreno:</dt><dd><span>90 m<sup>2</sup></span></dd>
													<dt class="bed">Dormitorios:</dt><dd><span>2</span></dd>
													<dt class="bath">Baños:</dt><dd><span>1</span></dd>
													<dt class="status">Cochera:</dt><dd><span>1</span></dd>
												</dl>
												';
												break;		
											case 'wititi':
												echo '
												<dl class="detail" style="min-height: 80px;">													
													<dt class="area">Área:</dt><dd><span>53 m<sup>2</sup></span></dd>
													<dt class="bed">Dormitorios:</dt><dd><span>3</span></dd>
													<dt class="bath">Baños:</dt><dd><span>1</span></dd>
												</dl>												
												';
												break;		
											case 'yaravi':
												echo '
												<dl class="detail" style="min-height: 80px;">													
													<dt class="area">Área:</dt><dd><span>46 m<sup>2</sup></span></dd>
													<dt class="bed">Dormitorios:</dt><dd><span>2</span></dd>
													<dt class="bath">Baños:</dt><dd><span>1</span></dd>
												</dl>												
												';
												break;		
											default:												
												break;
										}//end switch										
										?>

									</div>							
								</div>

								<!-- 8. Maps -->
								<div class="section-title line-style">
									<h2 class="title">Visítanos</h2>
								</div>
								<div class="map-container" id="map-canvas"></div>
								<br /><br />
								<?php require 'include/oficinas-listado.php'; ?>								

							</div>
						</div>


					</div>
					<div class="col-md-3">
						<?php require 'include/avance-de-obra.php'; ?>
						<!-- Other property -->
						<div class="section-title line-style line-style">
							<h2 class="title">Otras propiedades</h2>
						</div>						
						<?php require 'include/propiedades-thumbs.php'; ?>
					</div>
				</div>
			</div>
		</section>







<?php require 'include/footer.php'; ?>