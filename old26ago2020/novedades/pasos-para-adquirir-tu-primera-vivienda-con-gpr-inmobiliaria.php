<?php
define('og_image', 'images/novedades/04pasosparaadquirir.jpg');
define('og_title', 'PASOS PARA ADQUIRIR TU PRIMERA VIVIENDA CON GPR INMOBILIARIA');
define('og_type', 'website');
define('og_desc', 'A lo largo de la vida+ nos vemos en la necesidad de tomar decisiones de especial importancia que se verán reflejadas en nuestro futuro más inmediato; decisiones que nos ofrecen la oportunidad de cumplir con algunas metas y sueños. Una de ellas está relacionada a la posibilidad de comprar nuestra primera vivienda.');
define('keywords', ',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
define('title_complemento', 'Novedades | ' . og_title)
?>
<?php define('GPR_SECTION_CLASS','novedades04'); ?>
<?php require(__DIR__ . "/../include/config.php"); ?>
<?php require(__DIR__ . "/../include/header.php"); ?>

<section id="agent-page" class="header-margin-base fixed-no-header page-blog">

	<div class="hero-page">
		<div class="info-hero">
			<h1 class="title-name name">PASOS PARA ADQUIRIR TU PRIMERA VIVIENDA CON GPR INMOBILIARIA</h1>
			<div class="info-name cotizar-btn">
				<span class="title">Cotiza ahora tu casa</span>
				<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" alt="arrow-up" title="<?= GPR_ROOT_PATH ?>como-postular-a-mi-vivienda-y-las-ventajas-que-ofrece-gpr-inmobiliaria" /></a>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row indice-content">
			<div class="col-md-12">
				<div class="section-title">
					<h2 class="title title-grand">Indice de Contenidos</h2>
				</div>
				<li><a href="#indice1">¿Cómo podemos ser favorecido por el Programa Techo Propio y qué beneficios otorga?</a></li>
				<li><a href="#indice2">
						¿Cuáles son los pasos que debemos seguir para comprar nuestra primera casa con GPR INMOBILIARIA?
					</a></li>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<!-- <h1 class="name">Financiar tu nuevo hogar es más sencillo con Crédito Mi Vivienda</h1> -->
						<div class="bs-callout callout-info">

							<p class="text">
								A lo largo de la vida nos vemos en la necesidad de tomar decisiones de especial importancia que se verán reflejadas en nuestro futuro más inmediato; decisiones que nos ofrecen la oportunidad de cumplir con algunas metas y sueños. Una de ellas está relacionada a la posibilidad de comprar nuestra primera vivienda. </p>
						</div>
						<p>Cuando llega este momento, además de la emoción y el entusiasmo que puede provocarnos esta importante iniciativa, también nos llenamos de muchas dudas, inquietudes y temores. Nuestra mente comienza a desbordarse con preguntas ¿Qué tipo de vivienda puedo y debo comprar? ¿Puedo pagarla? ¿Tengo posibilidades de obtener un crédito? ¿Qué debo hacer? ¿Cuáles son los requisitos que tengo que cumplir? Todas estas interrogantes, y muchas más, se van aclarando de forma rápida y sencilla cuando logramos organizarnos y encontramos especialistas que nos asesoran y guían para recorrer este camino de forma más fácil. </p>
						<p>Inicialmente hay que estar claros en qué queremos y cómo obtenerlo, para ser lo más acertados posibles a la hora de comprar. En GPR INMOBILIARIA, le sugerimos algunos aspectos para tomar en consideración:</p>
						<ul>
							<li>Confiar en que es una de las mejores formas de inversión, pues la propiedad con el pasar del tiempo tendrá mayor valor dentro del mercado inmobiliario.</li>
							<li>Tener la seguridad de que, al comprar una vivienda, le estamos ofreciendo a nuestra familia mayor estabilidad, seguridad y bienestar.</li>
							<li>Decidir qué tipo de vivienda se adapta más a nuestras necesidades familiares. Hay que pensar en el tamaño, la distribución interna, dónde estará ubicada, cuáles son los espacios de recreación con los que cuenta para el disfrute de niños y ancianos.</li>
							<li>Informarnos si el conjunto habitacional donde se encuentra ubicada la vivienda cuenta con todos los servicios básicos indispensables para su funcionamiento.</li>
							<li>Conocer cuáles son las ofertas hipotecarias que ofrece el mercado bancario y decidir la más conveniente, de acuerdo a nuestro nivel de pago.</li>
							<li>Consultar cuáles son las políticas y propuestas de vivienda social que ofrece el Gobierno. </li>
						</ul>
						<p>En Perú existe el Programa Techo Propio del Fondo MIVIVIENDA que da, a los sectores más vulnerables o con menos ingresos económicos en el país, beneficios adicionales con el fin de promover e incentivar su primera compra dentro del mundo inmobiliario. </p>
						<div id=indice1 class="section-title">
							<h2 class="title">¿Cómo podemos ser favorecido por el Programa Techo Propio y qué beneficios otorga? </h2>
						</div>
						<p>El programa Techo Propio ofrece el Bono Familiar Habitacional (BFH). Se trata de un aporte completamente gratuito que establece el Estado a una familia, sin necesidad de devolución, y que se puede obtener una vez que se cumplan todas las condiciones, como disponer de un ahorro mínimo. Sin embargo, y debido a la situación de emergencia de salud que vivimos actualmente por la pandemia, se excluye de cumplir con este requerimiento a todos los beneficiarios hasta el 31de diciembre del 2020. Al respecto, y con el fin de favorecer a la ciudadanía, se ajustó el <b>BFH</b> para la modalidad de <b>Adquisición de Vivienda Nueva</b> , quedando esta bonificación en <b>S/ 37,625.</b> </p>
						<p>Para participar en este programa se deben cumplir cuatro requisitos básicos, muy fáciles de alcanzar:</p>
						<ol>
							<li>Conformar un Grupo Familiar (G.F.): Nuestro grupo familiar debe estar integrado por un Jefe de Familia, que declarará a uno o más dependientes que pueden ser su esposa, su conviviente, sus hijos, hermanos o nietos menores de 25 años o hijos mayores de 25 años con discapacidad, sus padres o abuelos. Los grupos familiares deben estar compuestos por un mínimo de dos personas.</li>
							<li>Límite de Ingreso Familiar Mensual (IFM): Para obtener este beneficio el ingreso de la familia, en su totalidad, no puede ser mayor de <b>S/ 3,715.</b> </li>
							<li>No haber recibido con anterioridad apoyo habitacional del Estado. Recuerde que este beneficio se otorga sólo una vez por grupo familiar, y quienes se postulan al mismo no pueden estar incluidos en otros de los programas relacionados con la compra, construcción o reparación de viviendas como: Enace, Fonavi, BANMAT o el FMV.</li>
							<li>Para comprar una vivienda, no podrán ser propietarios de otra vivienda o terreno a nivel nacional. </li>
						</ol>
						<p>En GPR INMOBILIARIA le guiamos por este camino de oportunidades, y le ofrecemos pasos sencillos para lograr la adquisición de su primera vivienda de forma rápida, segura y con las posibilidades de optar al financiamiento que concede el programa de Techo Propio.</p>
						<p>Actualmente tenemos en desarrollo el proyecto inmobiliario Las Lomas de Yura, con viviendas económicas, construidas en concreto, de ambiente ecológico y acondicionadas con todos los servicios necesarios de agua, desagües, electricidad, áreas verdes y un de estacionamiento. Son de bajo costo y su propósito es beneficiar a grupos familiares que aspiran a nuevas oportunidades de crecimiento.</p>
						<div id=indice2 class="section-title">
							<h2 class="title">¿Cuáles son los pasos que debemos seguir para comprar nuestra primera casa con GPR INMOBILIARIA? </h2>
						</div>
						<p>Tenga en cuenta que uno de nuestros asesores comerciales, siempre le estará apoyando y guiando, de manera personalizada, en cada uno de las etapas de este proceso.</p>
						<ol>
							<li>Seleccionar entre una casa o un apartamento. Se pueden ver todas las opciones de vivienda que tenemos disponibles y que se adaptan a sus necesidades <b>(colocar imágenes de vivienda).</b> </li>
							<li>Asistir a cualquiera de nuestras oficinas para llenar el formulario de Adquisición de Vivienda Nueva (AVN), ya que GPR INMOBILIARIA es un Centro Autorizado por el Fondo Mi Vivienda.</li>
							<li>Realizar el depósito de reserva de S/ 150 en el BBVA</li>
							<li>Consignar el recibo de depósito para seleccionar la ubicación de su vivienda en los planos del urbanismo.</li>
							<li>Presentar la fotocopia de su DNI, del cónyuge o de su carga familiar, y contar con los datos completos de todos los integrantes (Nombre, DNI, fecha de nacimiento), así como una boleta de luz o de agua.</li>
							<li>Firmar el contrato de separación que le entregará GPR Inmobiliaria.</li>
							<li>Esperar la confirmación del crédito por parte del Banco.</li>
							<li>Firmar en Notaría.</li>
							<li>Recibir su vivienda. Las primeras entregas serán realizadas a partir de febrero de 2021.</li>
						</ol>
						<p>Con GPR INMOBILIARIA usted está más cerca de cumplir con uno de sus grandes sueños: la compra de su primera vivienda con poco dinero y sin trámites complejos. En todo momento contará con nuestro asesoramiento, para garantizar el éxito de este proceso.</p>
						<p>Solicite ya su cotización y será contactado por uno de nuestros promotores de ventas. Recuerde que tenemos sede en tres localidades diferentes de Arequipa, para atenderle con mayor calidad y tiempo. </p>
						<p>Nuestros teléfonos:</p>
						<ul>
							<li>Maria : <b> <a href="tel:+51 923 104 967">+51 923 104 967</a></b>
							</li>
							<li>Daniel : <b> <a href="tel:+51 947 327 082">+51 947 327 082</a></b>
							</li>
							<li>Lidia : <b> <a href="tel:+51 947 326 649">+51 947 326 649</a></b>
							</li>
						</ul>
						<div class="cotizar-btn">
							<span class="title">Cotiza ahora tu casa</span>
							<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>como-postular-a-mi-vivienda-y-las-ventajas-que-ofrece-gpr-inmobiliaria" alt="arrow-up" /></a>
						</div>
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div>

		</div><!-- ./row -->
	</div>
	<br /><br /><br />
	<div id=cotizar class="cotizar-vivienda">
		<div class="container">
			<div class="row">
				<div class="avanze1 col-sm-4 col-md-3">
					<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div>
				<div class="col-sm-12 col-md-9">
					<!-- ===================== 
												SEARCH 
										====================== -->
					<div class="line-style no-margin">
						<h2 class="title-grand">Cotizar Vivienda</h2>
					</div>
					<div class="right-box no-margin">
						<div class="row">
							<?php require(__DIR__ . "/../include/form-cotizar.php"); ?>
						</div><!-- ./row 2 -->
					</div><!-- ./search -->

				</div>
				<div class="avanze2 col-sm-4 col-md-3">
					<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div><!-- /.col-md-12 -->
			</div>
			<!--row-->
		</div>
	</div>
	<br /><br /><br />
	<div class="container">
		<div class="section-title line-style no-margin">
			<h2 class="title">Elige tu nuevo hogar</h2>
		</div>

		<div class="my-property" data-navigation=".my-property-nav">
			<div class="crsl-wrap">
				<?php require(__DIR__ . "/../include/grid-propiedades.php"); ?>
			</div>
			<div class="my-property-nav">
				<p class="button-container">
					<a href="#" class="next">siguiente</a>
					<a href="#" class="previous">anterior</a>
				</p>
			</div>
		</div><!-- /.my-property slide -->

	</div><!-- ./container -->
</section><!-- /#about-us -->
<?php require(__DIR__ . "/../include/footer.php"); ?>