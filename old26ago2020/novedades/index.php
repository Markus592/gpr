<?php  require(__DIR__."/../include/config.php");?>
<?php
define('og_image', 'images/logo.png');
define('og_title', 'GPR Inmobiliaria  Arequipa');
define('og_type','website');
define('og_desc',GPR_OG_DESC);
define('keywords', ',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
define('title_complemento', 'Novedades')
?>

<?php require(__DIR__."/../include/header.php");?>

<section id="agent-page" class="header-margin-base fixed-no-header page-novedad ">

	<div class="novedad">
		<h1 class="name">Novedades</h1>

	</div>

	<div class="container ">
		<div class="">
			<h2 class="title-name"><b>Descubre m&aacute;s de nuestros Proyectos</b> </h2>
			<p>Pensando en cada familia peruana hemos realizado diferentes tipos de vivienda, 
			todas nuestras viviendas cuentan con áreas verdes, estacionamiento y áreas en común para 
			que podamos desarrollarnos plenamente</p>
		</div>
		<div id="" class="row container-filter">
			<!-- <div class="col-md-3">
				<div class="box-filters">
					<p class="title-filter"><b>Filtrar por Temas</b></p>
					<div class="">
						<div class="checkbox">
							<label>
								<input type="checkbox" value="">
								Herramientas
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" value="">
								Productividad
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" value="">
								Tips, trucos y herramientas
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" value="">
								Redes Sociales
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" value="">
								Facebook Ads
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" value="">
								Automatizaci&oacute;n de marketing
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" value="">
								Ganar dinero con tu blog
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" value="">
								Marketing de contenidos
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" value="">
								Analitica
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" value="">
								Community Manager
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" value="">
								Instagram
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" value="">
								Crear cursos online
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" value="">
								Facebook
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" value="">
								Blogging
							</label>
						</div>
					</div>
				</div> -->
				<!-- <div class="">
					<?php require(__DIR__."/../include/avance-de-obra.php");?>
				</div> -->
			<!-- </div> -->
			<div class="col-md-12">
				<div class="">
					<div class="row box-novedades">
					    <div class="item-novedades">
								<h2 class="title"><a href="<?=GPR_ROOT_PATH?>novedades/invertir-en-vivienda-en-tiempos-de-pandemia">INVERTIR</a></h2>
								<a class="hrefnovedad" href="<?=GPR_ROOT_PATH?>novedades/invertir-en-vivienda-en-tiempos-de-pandemia">
									<div class="image img-hover-zoom">
										<img class="img-responsive" alt="GPR" title="<?= GPR_ROOT_PATH ?>"  src="<?=GPR_ROOT_PATH?>images/blog-16.jpg">
									</div>
									<div class="text">
										<h3 class="subtitle">¿Invertir en vivienda en tiempos de  pandemia?</h3>
										Las crisis siempre han existido, desde los tiempos más remotos en la historia de la humanidad y por diversos motivos.
									</div>
									<a href="<?=GPR_ROOT_PATH?>novedades/invertir-en-vivienda-en-tiempos-de-pandemia" class="btn btn-default button-read">Leer m&aacute;s</a>
								</a>
						</div>
						<div class="item-novedades">
							
								
									<h2 class="title"><a href="<?=GPR_ROOT_PATH?>novedades/como-postular-a-mi-vivienda-y-las-ventajas-que-ofrece-gpr-inmobiliaria">POSTULAR</a></h2>
									<a class="hrefnovedad" href="<?=GPR_ROOT_PATH?>novedades/como-postular-a-mi-vivienda-y-las-ventajas-que-ofrece-gpr-inmobiliaria">
										<div class="image img-hover-zoom">
											<img class="img-responsive" alt="GPR" title="<?= GPR_ROOT_PATH ?>"   src="<?=GPR_ROOT_PATH?>/images/novedades/07comopostularamivivienda2.jpg">
											
										</div>
										<div class="text">
											<h3 class="subtitle">Cómo postular a mi vivienda y las ventajas que ofrece GPR Inmobiliaria</h3>
											Es un préstamos hipotecario que facilita la compra de una vivienda completa, finalizada, en proceso de constucción o el proyecto. Así mismo, la vivienda puede ser de primera venta o usada; en donde el valor mínimo de dicho inmueble sea de S/ 58,800 hasta los S/ 419,600.
											
										</div>
										<a href="<?=GPR_ROOT_PATH?>novedades/como-postular-a-mi-vivienda-y-las-ventajas-que-ofrece-gpr-inmobiliaria" class="btn btn-default button-read">Leer m&aacute;s</a>
									</a>
						</div>
						
						<!-- <div class="item-novedades">
								<h2 class="title"><a href="../blog-nuevo-credito-mi-vivienda-beneficios">TRABAJAR</a></h2>
								<a class="hrefnovedad" href="<?=GPR_ROOT_PATH?>novedades/como-postular-a-mi-vivienda-y-trabajar-con-gpr-inmobiliaria">
									<div class="image img-hover-zoom">
										<img class="img-responsive" alt="GPR" title="<?= GPR_ROOT_PATH ?>"  src="<?=GPR_ROOT_PATH?>images/blog-12b.jpg">
										
									</div>
									<div class="text">
										<h3 class="subtitle">Cómo postular a mi vivienda y trabajar con GPR Inmobiliaria</h3>
										El beneficio principal que brinda Nuevo Crédito Mi Vivienda es el Bono al Buen Pagador, que vendría a ser un bono de hasta S/. 17,700 que es otorgado para poder adquirir una casa o departamento.
										
									</div>
									<a href="<?=GPR_ROOT_PATH?>novedades/como-postular-a-mi-vivienda-y-trabajar-con-gpr-inmobiliaria" class="btn btn-default button-read">Leer m&aacute;s</a>
								</a>
							
						</div> -->
						<div class="item-novedades">
									<h2 class="title"><a href="<?=GPR_ROOT_PATH?>novedades/adquiere-tu-vivienda-te-ayudamos">ADQUIERE TU VIVIENDA</a></h2>
									<a class="hrefnovedad" href="<?=GPR_ROOT_PATH?>novedades/adquiere-tu-vivienda-te-ayudamos">
										<div class="image img-hover-zoom">
											<img class="img-responsive" alt="GPR" title="<?= GPR_ROOT_PATH ?>"   src="<?=GPR_ROOT_PATH?>/images/novedades/06adquieretuvivienda.jpg">
											
										</div>
										<div class="text">
											<h3 class="subtitle">¡Adquiere tu vivienda, te ayudamos!</h3>
											Nos enfrentamos a una nueva realidad mundial debido a la pandemia, pero esto también nos lleva a buscar nuevas oportunidades de avances.
											
										</div>
										<a href="<?=GPR_ROOT_PATH?>novedades/adquiere-tu-vivienda-te-ayudamos" class="btn btn-default button-read">Leer m&aacute;s</a>
									</a>
							
						</div>
						<div class="item-novedades">
								<h2 class="title"><a href="<?=GPR_ROOT_PATH?>novedades/adquiere-tu-casa-economica-en-yura">LAS LOMAS DE YURA</a></h2>
								<a class="hrefnovedad" href="<?=GPR_ROOT_PATH?>novedades/adquiere-tu-casa-economica-en-yura">
									<div class="image img-hover-zoom">
										<img class="img-responsive" alt="GPR" title="<?= GPR_ROOT_PATH ?>"  src="<?=GPR_ROOT_PATH?>images/novedades/05encuentratucasapropia.jpg">
										
									</div>
									<div class="text">
										<h3 class="subtitle">Encuentra tu casa propia en Las Lomas de Yura</h3>
										Entre los bienes que podrás adquirir a lo largo de toda tu vida, podría decirse que el más importante es una vivienda.
										
									</div>
									<a href="<?=GPR_ROOT_PATH?>novedades/adquiere-tu-casa-economica-en-yura" class="btn btn-default button-read">Leer m&aacute;s</a>
								</a>
							
						</div>
						<div class="item-novedades">
								<h2 class="title"><a href="<?=GPR_ROOT_PATH?>novedades/pasos-para-adquirir-tu-primera-vivienda-con-gpr-inmobiliaria">PASOS PARA ADQUIRIR TU VIVIENDA</a></h2>
								<a class="hrefnovedad" href="<?=GPR_ROOT_PATH?>novedades/pasos-para-adquirir-tu-primera-vivienda-con-gpr-inmobiliaria">
									<div class="image img-hover-zoom">
										<img class="img-responsive" alt="GPR" title="<?= GPR_ROOT_PATH ?>"  src="<?=GPR_ROOT_PATH?>images/novedades/04pasosparaadquirir.jpg">
										
									</div>
									<div class="text">
										<h3 class="subtitle">Pasos para adquirir tu primera vivienda con GPR inmobiliaria</h3>
										A lo largo de la vida nos vemos en la necesidad de tomar decisiones de especial importancia que se verán reflejadas en nuestro futuro más inmediato; decisiones que nos ofrecen la oportunidad de cumplir con algunas metas y sueños. Una de ellas está relacionada a la posibilidad de comprar nuestra primera vivienda.
										
									</div>
									<a href="<?=GPR_ROOT_PATH?>novedades/pasos-para-adquirir-tu-primera-vivienda-con-gpr-inmobiliaria" class="btn btn-default button-read">Leer m&aacute;s</a>
								</a>
							
						</div>
						<div class="item-novedades">
								<h2 class="title"><a href="<?=GPR_ROOT_PATH?>novedades/blog-no-tengo-boleta-de-pago-podre-acceder-a-un-credito">BOLETA DE PAGO</a></h2>
								<a class="hrefnovedad" href="<?=GPR_ROOT_PATH?>novedades/blog-no-tengo-boleta-de-pago-podre-acceder-a-un-credito">
									<div class="image img-hover-zoom">
										<img class="img-responsive" alt="GPR" title="<?= GPR_ROOT_PATH ?>"  src="<?=GPR_ROOT_PATH?>images/blog-11.jpg">
										
									</div>
									<div class="text">
										<h3 class="subtitle">No tengo boleta de pago ¿Podré acceder a un crédito</h3>
										Olvídate de las trabas. Acceder a un préstamo sin contar con un trabajo formal es una realidad.
										
									</div>
									<a href="<?=GPR_ROOT_PATH?>novedades/blog-no-tengo-boleta-de-pago-podre-acceder-a-un-credito" class="btn btn-default button-read">Leer m&aacute;s</a>
								</a>
							
						</div>
						<div class="item-novedades">
								<h2 class="title"><a href="<?=GPR_ROOT_PATH?>novedades/blog-nuevo-credito-mi-vivienda-beneficios">Beneficios</a></h2>
								<a class="hrefnovedad" href="<?=GPR_ROOT_PATH?>novedades/blog-nuevo-credito-mi-vivienda-beneficios">
									<div class="image img-hover-zoom">
										<img class="img-responsive" alt="GPR" title="<?= GPR_ROOT_PATH ?>"  src="<?=GPR_ROOT_PATH?>images/blog-12b.jpg">
										
									</div>
									<div class="text">
										<h3 class="subtitle">Conoce los beneficios del Nuevo Crédito MIVIVIENDA</h3>
										El Nuevo Crédito MIVIVIENDA es un préstamo hipotecario que permite financiar la compra de vivienda terminada, en construcción o en proyecto, que sean de primera venta o usadas.
										
									</div>
									<a href="<?=GPR_ROOT_PATH?>novedades/blog-nuevo-credito-mi-vivienda-beneficios" class="btn btn-default button-read">Leer m&aacute;s</a>
								</a>
							
						</div>
						<div class="item-novedades">
								<h2 class="title"><a href="<?=GPR_ROOT_PATH?>novedades/blog-que-es-nuevo-credito-mi-vivienda">MI VIVIENDA</a></h2>
								<a class="hrefnovedad" href="<?=GPR_ROOT_PATH?>novedades/blog-que-es-nuevo-credito-mi-vivienda">
									<div class="image img-hover-zoom">
										<img class="img-responsive" alt="GPR" title="<?= GPR_ROOT_PATH ?>"  src="<?=GPR_ROOT_PATH?>images/blog-15.jpg">
									</div>
									<div class="text">
										<h3 class="subtitle">Financiar tu nuevo hogar es más sencillo con Crédito Mi Vivienda</h3>
										Cada vez más peruanos apuestan por la compra de una casa o departamento, por ende, necesitan de apoyos financieros que hagan esto posible.
										
									</div>
									<a href="<?=GPR_ROOT_PATH?>novedades/blog-que-es-nuevo-credito-mi-vivienda" class="btn btn-default button-read">Leer m&aacute;s</a>
								</a>
						</div>
						
					</div>
				</div>
				<div class="">
					<div class="pagination">
						<a href="">
							<<</a> <a href="">
								<</a> <a href="">1
						</a>
						<a href="">2</a>
						<a href="">3</a>
						<a href="">4</a>
						<a href="">5</a>
						<a href="">6</a>
						<a href="">></a>
						<a href="">>></a>
					</div>
				</div>
			</div>
		</div><!-- ./row -->
	</div>
	<br /><br /><br />
	<div class="cotizar-vivienda">
		<div class="container">
			<div class="row">
				<div class="avanze1 col-sm-4 col-md-3">
					<?php require(__DIR__."/../include/avance-de-obra.php");?>
				</div>
				<div class="col-sm-12 col-md-9">
					<!-- ===================== 
												SEARCH 
										====================== -->
					<div class="line-style no-margin">
						<h3 class="title-grand">Cotizar Vivienda</h3>
					</div>
					<div class="right-box no-margin">
						<div class="row">
							<?php require(__DIR__."/../include/form-cotizar.php");?>
						</div><!-- ./row 2 -->
					</div><!-- ./search -->

				</div>
				<div class="avanze2 col-sm-4 col-md-3">
					<?php require(__DIR__."/../include/avance-de-obra.php");?>
				</div><!-- /.col-md-12 -->
			</div>
			<!--row-->
		</div>
	</div>
	<br /><br /><br />
	<div class="container">
		<div class="section-title line-style no-margin">
			<h3 class="title">Elige tu nuevo hogar</h3>
		</div>

		<div class="my-property" data-navigation=".my-property-nav">
			<div class="crsl-wrap">
				<?php require(__DIR__."/../include/grid-propiedades.php");?>
			</div>
			<div class="my-property-nav">
				<p class="button-container">
					<a href="#" class="next">siguiente</a>
					<a href="#" class="previous">anterior</a>
				</p>
			</div>
		</div><!-- /.my-property slide -->

	</div><!-- ./container -->
</section><!-- /#about-us -->

<?php require(__DIR__."/../include/footer.php");?>
