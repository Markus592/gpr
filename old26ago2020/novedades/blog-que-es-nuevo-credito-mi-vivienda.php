<?php
define('og_image', 'images/novedades/aleli-11.jpg');
define('og_title', 'Financiar tu nuevo hogar es más sencillo con Crédito Mi Vivienda');
define('og_type', 'website');
define('og_desc', 'Cada vez más peruanos apuestan por la compra de una casa o departamento, por ende, necesitan de apoyos financieros que hagan esto posible.');
define('title_complemento', 'Novedades | '.og_title)
?>
<?php require(__DIR__ . "/../include/config.php"); ?>
<?php require(__DIR__ . "/../include/header.php"); ?>
<section id="agent-page" class="header-margin-base fixed-no-header page-blog">
	
	<div class="hero-page">
		<div class="info-hero">
			<h1 class="title-name name">Financiar tu nuevo hogar es más sencillo con Crédito Mi Vivienda</h1>
			<div class="info-name cotizar-btn">
				<span class="title">Cotiza ahora tu casa</span>	
				<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>" alt="arrow-up" /></a>
			</div>
		</div> 
	</div>
	
	<div class="container">
		<div class="row indice-content">
			<div class="col-md-12">
				<div  class="section-title">
					<h2 class="title title-grand">Indice de Contenidos</h2>
				</div>
				<li><a href="#indice1">¿Qué Es Nuevo Crédito MI VIVIENDA?</a></li>
				<li><a href="#indice2">¿Por Qué Elegir Nuevo Crédito MI VIVIENDA?</a></li>
				<li><a href="#indice3">¿Cuáles son los Requisitos que necesito?</a></li>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<!-- <h1 class="name">Financiar tu nuevo hogar es más sencillo con Crédito Mi Vivienda</h1> -->
						<div class="bs-callout callout-info">
							<h4 class="title">Cada vez más peruanos apuestan por la compra de una casa o departamento, por ende, necesitan de apoyos financieros que hagan esto posible.</h4>
							<p class="text">Es por eso que vemos importante que se sepa qué es Nuevo Crédito MI VIVIENDA y en qué consiste.</p>
						</div>
						<div id=indice1 class="section-title">
							<h2 class="title">¿Qué es Nuevo Crédito MI VIVIENDA?</h2>
						</div>
						<p>Es un préstamo hipotecario que facilita la compra de una vivienda completa, finalizada, en proceso de constucción o el proyecto. Así mismo, la vivienda puede ser de primera venta o usada; en donde el valor mínimo de dicho inmueble sea de S/ 58,800 hasta los S/ 419,600.</p>
						<div id=indice2 class="section-title">
							<h2 class="title">¿Por qué elegir Nuevo Crédito MI VIVIENDA?</h2>
						</div>
						<p>El beneficio principal que brinda Nuevo Crédito Mi Vivienda es el Premio al Buen Pagador, que vendría a ser un descuento de S/. 17,700 o S/. 6,400, que es otorgado como un premio a la puntualidad en el pago de las cuotas mensuales.</p>
						<div id=indice3 class="section-title">
							<h2 class="title">¿Cuáles son los requisitos que necesito?</h2>
						</div>
						<p>Los requisitos a cumplir para acceder a Nuevo Crédito Mi Vivienda son los siguientes:</p>
						<ul>
							<li>Ser <b>mayor de edad</b>.</li>
							<li>Ser <b>calificado</b> por la <b>Entidad Financiera</b> para el crédito MIVIVIENDA.</li>
							<li>No tener <b>ningún crédito pendiente de pago con el FMV</b>.</li>
							<li><b>No ser propietario o copropietario</b> de otra vivienda a nivel nacional.</li>
							<li>Contar con una <b>cuota inicial mínima del 10%</b> del valor de la vivienda que vas a comprar.</li>
						</ul>
						<p>Para acceder a este crédito hipotecario es muy sencillo y útil, ya que el trámite es corto y los beneficios son muchos.</p>
						<div class="cotizar-btn">
							<span class="title">Cotiza ahora tu casa</span>
							<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>" alt="arrow-up" /></a>
						</div>	
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div>

		</div><!-- ./row -->
	</div>
	<br /><br /><br />
	<div id=cotizar class="cotizar-vivienda">
		<div class="container">
			<div class="row">
				<div class="avanze1 col-sm-4 col-md-3">
				<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div>
				<div class="col-sm-12 col-md-9">
					<!-- ===================== 
												SEARCH 
										====================== -->
					<div class="line-style no-margin">
						<h2 class="title-grand">Cotizar Vivienda</h2>
					</div>
					<div class="right-box no-margin">
						<div class="row">
						<?php require(__DIR__ . "/../include/form-cotizar.php"); ?>
						</div><!-- ./row 2 -->
					</div><!-- ./search -->

				</div>
				<div class="avanze2 col-sm-4 col-md-3">
				<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div><!-- /.col-md-12 -->
			</div>
			<!--row-->
		</div>
	</div>
	<br /><br /><br />
	<div class="container">
		<div class="section-title line-style no-margin">
			<h2 class="title">Elige tu nuevo hogar</h2>
		</div>

		<div class="my-property" data-navigation=".my-property-nav">
			<div class="crsl-wrap">
			<?php require(__DIR__ . "/../include/grid-propiedades.php"); ?>
			</div>
			<div class="my-property-nav">
				<p class="button-container">
					<a href="#" class="next">siguiente</a>
					<a href="#" class="previous">anterior</a>
				</p>
			</div>
		</div><!-- /.my-property slide -->

	</div><!-- ./container -->
</section><!-- /#about-us -->





<?php require(__DIR__ . "/../include/footer.php"); ?>