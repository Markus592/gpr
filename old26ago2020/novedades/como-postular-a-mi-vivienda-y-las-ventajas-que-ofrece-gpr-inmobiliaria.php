<?php
define('og_image', 'images/novedades/07comopostularamivivienda2.jpg');
define('og_title', 'CÓMO POSTULAR A MI VIVIENDA Y LAS VENTAJAS QUE OFRECE GPR INMOBILIARIA. ');
define('og_type','website');
define('og_desc','En otros tiempos, comprar una propiedad podía convertirse en un proyecto personal o familiar relativamente costoso. Los altos precios de las viviendas nos obligaban a tener un capital bastante extenso; sin embargo, hoy en día esta meta se hace mucho más fácil de alcanzar por simples motivos: el desarrollo de proyectos inmobiliarios a bajo costo y el financiamiento bajo la figura de crédito hipotecario que nos ofrece la banca privada, que es la forma clásica de préstamos que se utilizan para respaldar las adquisiciones de inmuebles. ');
define('keywords', ',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
define('title_complemento', 'Novedades | '.og_title);
define('GPR_SECTION_CLASS','novedades07');
?>
<?php  require(__DIR__."/../include/config.php");?>
<?php require(__DIR__."/../include/header.php");?>
<section id="agent-page" class="header-margin-base fixed-no-header page-blog">
	
	<div class="hero-page">
		<div class="info-hero">
			<h1 class="title-name name">CÓMO POSTULAR A MI VIVIENDA Y LAS VENTAJAS QUE OFRECE GPR INMOBILIARIA. </h1>
			<div class="info-name cotizar-btn">
				<span class="title">Cotiza ahora tu casa</span>	
				<a href="#cotizar"><img src="<?=GPR_ROOT_PATH?>images/arrow-up.png" alt="arrow-up" title="<?=GPR_ROOT_PATH?>como-postular-a-mi-vivienda-y-las-ventajas-que-ofrece-gpr-inmobiliaria"/></a>
			</div>
		</div> 
	</div>
	
	<div class="container">
		<!-- <div class="row indice-content">
			<div class="col-md-12">
				<div  class="section-title">
					<h2 class="title title-grand">Indice de Contenidos</h2>
				</div>
				<li><a href="#indice1">¿Qué Es Nuevo Crédito MI VIVIENDA?</a></li>
				<li><a href="#indice2">¿Por Qué Elegir Nuevo Crédito MI VIVIENDA?</a></li>
				<li><a href="#indice3">¿Cuáles son los Requisitos que necesito?</a></li>
			</div>
		</div> -->
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<!-- <h1 class="name">Financiar tu nuevo hogar es más sencillo con Crédito Mi Vivienda</h1> -->

						<div class="content-doublecolumn">
							<div class="bs-callout callout-info">
								<p class="text">En otros tiempos, comprar una propiedad podía convertirse en un proyecto personal o familiar relativamente costoso. Los altos precios de las viviendas nos obligaban a tener un capital bastante extenso; sin embargo, hoy en día esta meta se hace mucho más fácil de alcanzar por simples motivos: el desarrollo de proyectos inmobiliarios a bajo costo y el financiamiento bajo la figura de crédito hipotecario que nos ofrece la banca privada, que es la forma clásica de préstamos que se utilizan para respaldar las adquisiciones de inmuebles. </p>
							</div>
							
							<div class="video-responsive">
							<iframe src="https://www.youtube.com/embed/aJ1VXCdJYdY?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
						</div>


						<p>Adicionalmente a los créditos hipotecarios que otorga el mercado bancario, en diversos países del mundo existen planes gubernamentales de subsidios que buscan aumentar el nivel de vida de sus ciudadanos. En Perú, por ejemplo, tenemos la posibilidad de elegir algunas de las propuestas que nos presenta el Fondo Mi Vivienda (FMV), cuyo objetivo es “Promover el acceso a la vivienda única y adecuada, principalmente de las familias con menores ingresos, a través de la articulación entre el Estado y los Sectores Inmobiliario y Financiero, impulsando su desarrollo”. Entre sus alternativas se encuentra el <b> Nuevo Crédito Mi Vivienda</b> (NCMV) que se gestiona a través de Instituciones Financieras Intermediarias (IFI), ubicadas en todo el territorio nacional. </p>
						<p>¿Y cómo podemos postular o ser elegibles a este financiamiento?</p>
						<p>Sólo debemos cumplir con ciertos requisitos ya establecidos:</p>
						<ul>
							<li>Tener 18 años cumplidos (mayoría de edad).</li>
							<li>Haber obtenido el calificativo como beneficiario del crédito por parte de alguna Institución Financiera Intermediaria (IFI). En GPR INMOBILIARIA trabajamos directamente con el BBVA.</li>
							<li>Estar solvente. No tener ningún crédito pendiente de pago por el Fondo Mi Vivienda. Es importante destacar que toda persona que solicita un préstamo, será verificada en la lista de Infocorp para conocer su historial crediticio. Si usted se encuentra dentro de este registro con notas de morosidad sobre alguna deuda, la solicitud del crédito puede verse comprometida de forma desfavorable.</li>
							<li>No puede ser propietario o co-propietario de otra vivienda a nivel nacional. El solicitante y/o, en su caso, su cónyuge o su conviviente legalmente reconocido independientemente de su régimen patrimonial e hijos menores de edad, no pueden ser propietarios o copropietarios de otra vivienda en cualquier localidad del país.</li>
							<li>Contar con una cuota mínima inicial del 10% del valor de la vivienda que se quiere comprar. Al respecto, los montos de financiamiento se ubican a partir de S/ 60,000 hasta S/ 427,600. A manera de modelo, si usted desea comprar una vivienda por el valor de S/ 84,100 debe tener una inicial de S/ 8.410</li>
							<li>Los plazos de pago están ubicados entre 05 y 20 años.</li>
							<li>La tasa de interés es fija y su pago es en Soles.</li>
						</ul>
						<div id=indice1 class="section-title">
							<h2 class="title">¿Qué ventajas ofrece GPR INMOBILIARIA? </h2>
						</div>

						<div>
						<p>En GPR INMOBILIARIA nos distinguimos por dos simples razones: La primera, comprendemos el significado de la combinación perfecta entre calidad y economía; motivándonos a desarrollar, en la ciudad de Arequipa, un sistema constructivo habitacional a bajo precio, en espacios que van desde 35M2 hasta 93 M2. La segunda, es que le brindamos atención personalizada. Desde el mismo instante en que usted solicita una cotización, recibirá la guía y orientación de un Asesor de Ventas, quien estará en capacidad de recomendarle las mejores opciones, así como apoyar en todo el proceso de recaudación de documentos. </p>
						<p>Adicionalmente, ser propietario de alguna de nuestras viviendas ofrece especiales ventajas:</p>
						</div>

						<ul>
							<li>Independientemente de su tamaño interior, las casas han sido desarrolladas en terrenos que van desde 90M<sup>2</sup> a 210M<sup>2</sup></li>
							<li>Garantizamos una mejor y mayor calidad de vida porque están acondicionadas con todos los servicios básicos necesarios como agua, luz, desagües, pistas y veredas; así como la entrega del título de propiedad.</li>
							<li>En su totalidad, estas residencias disponen de áreas verdes.</li>
							<li>Todas las casas cuentan con su propio puesto de estacionamiento.</li>
						</ul>
						<p>Recuerde que GPR INMOBILIARIA trabaja con los programas crediticios que otorga el Fondo Mi Vivienda, manteniendo además alianzas con el BBVA.</p>
						<p>Visítenos en cualquiera de nuestras oficinas ubicadas en la región de Arequipa, donde recibirá un trato cordial y directo, para que su compra pueda ser efectiva.</p>
						<ul>
							<li>Maria : <b> <a href="tel:+51 923 104 967">+51 923 104 967</a></b>
							</li>
							<li>Daniel : <b> <a href="tel:+51 947 327 082">+51 947 327 082</a></b>
							</li>
							<li>Lidia : <b> <a href="tel:+51 947 326 649">+51 947 326 649</a></b>
							</li>
						</ul>
                        <p>Contáctenos y solicite su cotización.</p>
                        <div class="cotizar-btn">
								<span class="title">Cotiza ahora tu casa</span>
								<a href="#cotizar"><img src="<?=GPR_ROOT_PATH?>images/arrow-up.png" title="<?=GPR_ROOT_PATH?>como-postular-a-mi-vivienda-y-las-ventajas-que-ofrece-gpr-inmobiliaria" alt="arrow-up" /></a>
							</div>
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div>

		</div><!-- ./row -->
	</div>
	<br /><br /><br />
	<div id=cotizar class="cotizar-vivienda">
		<div class="container">
			<div class="row">
				<div class="avanze1 col-sm-4 col-md-3">
                <?php require(__DIR__."/../include/avance-de-obra.php");?>
				</div>
				<div class="col-sm-12 col-md-9">
					<!-- ===================== 
												SEARCH 
										====================== -->
					<div class="line-style no-margin">
						<h2 class="title-grand">Cotizar Vivienda</h2>
					</div>
					<div class="right-box no-margin">
						<div class="row">
                        <?php require(__DIR__."/../include/form-cotizar.php");?>
						</div><!-- ./row 2 -->
					</div><!-- ./search -->

				</div>
				<div class="avanze2 col-sm-4 col-md-3">
                <?php require(__DIR__."/../include/avance-de-obra.php");?>
				</div><!-- /.col-md-12 -->
			</div>
			<!--row-->
		</div>
	</div>
	<br /><br /><br />
	<div class="container">
		<div class="section-title line-style no-margin">
			<h2 class="title">Elige tu nuevo hogar</h2>
		</div>

		<div class="my-property" data-navigation=".my-property-nav">
			<div class="crsl-wrap">
				<?php require(__DIR__."/../include/grid-propiedades.php");?>
			</div>
			<div class="my-property-nav">
				<p class="button-container">
					<a href="#" class="next">siguiente</a>
					<a href="#" class="previous">anterior</a>
				</p>
			</div>
		</div><!-- /.my-property slide -->

	</div><!-- ./container -->
</section><!-- /#about-us -->
<?php require(__DIR__."/../include/footer.php");?>