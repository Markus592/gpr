<?php
define('og_image', 'images/novedades/04pasosparaadquirir.jpg');
define('og_title', '¿INVERTIR EN VIVIENDA EN TIEMPOS DE PANDEMIA?');
define('og_type', 'website');
define('og_desc', 'Las crisis siempre han existido, desde los tiempos más remotos en la historia de la humanidad y por diversos motivos: desastres naturales, sociales, económicos y de salud, como sucede ahora.');
define('keywords', ',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
define('title_complemento', 'Novedades | ' . og_title)
?>
<?php define('GPR_SECTION_CLASS', 'novedades08'); ?>
<?php require(__DIR__ . "/../include/config.php"); ?>
<?php require(__DIR__ . "/../include/header.php"); ?>

<section id="agent-page" class="header-margin-base fixed-no-header page-blog">

	<div class="hero-page">
		<div class="info-hero">
			<h1 class="title-name name">¿INVERTIR EN VIVIENDA EN TIEMPOS DE PANDEMIA?</h1>
			<div class="info-name cotizar-btn">
				<span class="title">Cotiza ahora tu casa</span>
				<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" alt="arrow-up" title="<?= GPR_ROOT_PATH ?>como-postular-a-mi-vivienda-y-las-ventajas-que-ofrece-gpr-inmobiliaria" /></a>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row indice-content">
			<div class="col-md-12">
				<div class="section-title">
					<h2 class="title title-grand">Indice de Contenidos</h2>
				</div>
				<li><a href="#indice1">¿Qu&eacute; ventajas tengo para poder comprar una vivienda?</a></li>
				<li><a href="#indice2">¿Y d&oacute;nde tengo la oportunidad de comprar?</a></li>
				<li><a href="#indice3">¿Necesitas m&aacute;s informaci&oacute;n?</a></li>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<!-- <h1 class="name">Financiar tu nuevo hogar es más sencillo con Crédito Mi Vivienda</h1> -->
						<div class="bs-callout callout-info">
							<p>La respuesta es: Sí.</p>
							<p class="text">
							Las crisis siempre han existido, desde los tiempos más remotos en la historia de la humanidad y por diversos motivos: desastres naturales, sociales, económicos y de salud, como sucede ahora. Pero todos los períodos críticos también han estado acompañados de grandes oportunidades, y la posibilidad de adquisición de vivienda, en estos momentos, es una de ellas.</p>
						</div>
						<p>Y es que debemos lanzar nuestra mirada más allá de las preocupaciones inmediatas que generan estas circunstancias, ya que es precisamente en tiempos difíciles cuando se refuerzan propuestas y planes dirigidos a aquellas comunidades que cuentan con menos recursos económicos, o que son más vulnerables frente a situaciones de riesgo.</p>
						<div id=indice1 class="section-title">
							<h2 class="title">¿Qu&eacute; ventajas tengo para poder comprar una vivienda?</h2>
						</div>
						<p>A nivel mundial, todos los Gobiernos tienen la responsabilidad de implementar políticas que permitan cubrir las necesidades básica de sus ciudadanos con respecto a la compra de viviendas; y estas cobran especial importancia cuando las mismas está dirigidas a dar atención a las poblaciones con mayores niveles de desigualdad, que viven en condiciones inadecuadas de hacinamiento, tanto en ciudades como en medios rurales. </p>
						<p>En el caso de Perú, por ejemplo, durante estos largos meses de alarma, debido a la situación generada por la pandemia del COVID-19, el Gobierno ha tomado medidas de protección para la familia, como:</p>
						<ul>
							<li>Promover la construcción de por lo menos unas 65.000 viviendas.</li>
							<li>Aumentar el Bono Familiar Habitacional (BFH) para la compra de vivienda, de S/ 34,400 a S/ 37,625</li>
							<li>Aumentar el Bono al Buen Pagador (BBP) del Nuevo Crédito Mi Vivienda, desde los S/ 10 mil hasta los S/ 24 mil.</li>
						</ul>
						<p>Adicionalmente a estos beneficios que otorga o impulsa el Estado, hay otros factores que puedes aprovechar a tu favor en estos tiempos, como: </p>
						<ul>
							<li>El desarrollo de proyectos habitacionales a bajo costo</li>
							<li>La reducción de las tasas de interés de los créditos hipotecarios.</li>
							<li>Planes que se ajustan a la capacidad de pago de los compradores</li>
							<li>Los procesos para el trámite de documentos se hacen más fáciles.</li>
						</ul>
						<div id=indice2 class="section-title">
							<h2 class="title">¿Y d&oacute;nde tengo la oportunidad de comprar?</h2>
						</div>
						<p>GPR INMOBILIARIA te presenta una propuesta ideal con características únicas en toda la región de Arequipa. Se trata del mega proyecto residencial Las  <a href=""> Lomas de Yura </a>que abarca 2.157 viviendas de interés social, de precios asequibles y financiamiento bancario. </p>
						<p>Por supuesto que, al tratarse de un plan residencial tan amplio, su construcción está programada por etapas. La primera, que estamos desarrollando en estos momentos, está integrada por 288 viviendas, distribuidas en 244 casas y 40 departamentos, que van desde 35M<sup>2</sup> hasta 93 M<sup>2</sup>, con fecha de entrega a partir del mes de febrero de 2021.</p>
						<p>La distribución de los espacios internos, de cada una de estas viviendas, varía según su tamaño, sin embargo, todas comparten características similares en cuanto a:</p>
						<ul>
							<li>Comodidad.</li>
							<li>Están construidas en áreas de terreno de 90 M2.</li>
							<li>Cada una posee un puesto de estacionamiento.</li>
							<li>Todas tienen un área de jardinería.</li>
						</ul>						
						<p>¡Esta es tu oportunidad de invertir! ¡No lo dudes!</p>
						<div id="indice3" class="section-title">
							<h2 class="title">¿Necesitas m&aacute;s informaci&oacute;n?</h2>
						</div>
						<p>Puedes solicitar una <a href="">cotización de la vivienda</a> que más se adapte a tus necesidades de espacio , e inmediatamente serás contactado por uno de nuestros promotores de ventas. </p>
						<p>De igual forma, tenemos <a href="">sede en tres localidades de Arequipa</a> donde obtendrás una atención personalizada, respetando los protocolos sanitarios correspondientes, implementados por el Gobierno nacional debido al COVID-19.  </p>
						<p>Nuestros tel&eacute;fonos de contacto:</p>
						<ul>
							<li>María: <b> <a href="tel:+51 923 104 967">+51 923 104 967</a></b></li>
							<li>Daniel:<b> <a href="tel:+51 947 327 082">+51 947 327 082</a></b></li>
							<li>Lidia: <b> <a href="tel:+51 947 326 649">+51 947 326 649</a></b></li>
						</ul>
						<div class="cotizar-btn">
							<span class="title">Cotiza ahora tu casa</span>
							<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>como-postular-a-mi-vivienda-y-las-ventajas-que-ofrece-gpr-inmobiliaria" alt="arrow-up" /></a>
						</div>
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div>

		</div><!-- ./row -->
	</div>
	<br /><br /><br />
	<div id=cotizar class="cotizar-vivienda">
		<div class="container">
			<div class="row">
				<div class="avanze1 col-sm-4 col-md-3">
					<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div>
				<div class="col-sm-12 col-md-9">
					<!-- ===================== 
												SEARCH 
										====================== -->
					<div class="line-style no-margin">
						<h2 class="title-grand">Cotizar Vivienda</h2>
					</div>
					<div class="right-box no-margin">
						<div class="row">
							<?php require(__DIR__ . "/../include/form-cotizar.php"); ?>
						</div><!-- ./row 2 -->
					</div><!-- ./search -->

				</div>
				<div class="avanze2 col-sm-4 col-md-3">
					<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div><!-- /.col-md-12 -->
			</div>
			<!--row-->
		</div>
	</div>
	<br /><br /><br />
	<div class="container">
		<div class="section-title line-style no-margin">
			<h2 class="title">Elige tu nuevo hogar</h2>
		</div>

		<div class="my-property" data-navigation=".my-property-nav">
			<div class="crsl-wrap">
				<?php require(__DIR__ . "/../include/grid-propiedades.php"); ?>
			</div>
			<div class="my-property-nav">
				<p class="button-container">
					<a href="#" class="next">siguiente</a>
					<a href="#" class="previous">anterior</a>
				</p>
			</div>
		</div><!-- /.my-property slide -->

	</div><!-- ./container -->
</section><!-- /#about-us -->
<?php require(__DIR__ . "/../include/footer.php"); ?>