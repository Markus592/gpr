<?php
define('og_image', 'images/novedades/aleli-11.jpg');
define('og_title', 'No tengo boleta de pago ¿Podré acceder a un crédito?');
define('og_type', 'website');
define('og_desc', '¿Cuentas con un trabajo pero no sabes cómo sustentarlo? Está claro que la mayoría de los trabajadores peruanos no recibe una boleta de pago mensual, requisito básico que piden los bancos para acceder a un crédito hipotecario.');
define('keywords', ',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
define('title_complemento', 'Novedades | '.og_title)
?>
<?php require(__DIR__ . "/../include/config.php"); ?>
<?php require(__DIR__ . "/../include/header.php"); ?>
<section id="agent-page" class="header-margin-base fixed-no-header page-blog">

	<div class="hero-page">
		<div class="info-hero">
			<h1 class="title-name name">No tengo boleta de pago ¿Podré acceder a un crédito?</h1>
			<div class="info-name cotizar-btn">
				<span class="title">Cotiza ahora tu casa</span>
				<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>" alt="arrow-up" /></a>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-sm-12 col-md-12">

						<div class="bs-callout callout-info">
							<h4 class="title">Olvídate de las trabas. Acceder a un préstamo sin contar con un trabajo formal es una realidad.</h4>
						</div>
						<p class="text">¿Cuentas con un trabajo pero no sabes cómo sustentarlo? Está claro que la mayoría de los trabajadores peruanos no recibe una boleta de pago mensual, requisito básico que piden los bancos para acceder a un crédito hipotecario.</p>
						<p class="text">Sin embargo, no tienes de qué preocuparte. Las Sociedades Financieras brindan las facilidades para aquellos consumidores de nivel socioeconómico C y D que no tienen posibilidad de estar en planilla.</p>
						<div class="cotizar-btn">
							<span class="title">Cotiza ahora tu casa</span>
							<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>" alt="arrow-up" /></a>
						</div>
					</div><!-- /.col-md-8 -->
				</div><!-- /.row -->
			</div>

		</div><!-- ./row -->
	</div>
	<br /><br /><br />
	<div id=cotizar class="cotizar-vivienda">
		<div class="container">
			<div class="row">
				<div class="avanze1 col-sm-4 col-md-3">
				<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div>
				<div class="col-sm-12 col-md-9">
					<!-- ===================== 
												SEARCH 
										====================== -->
					<div class="line-style no-margin">
						<h2 class="title-grand">Cotizar Vivienda</h2>
					</div>
					<div class="right-box no-margin">
						<div class="row">
						<?php require(__DIR__ . "/../include/form-cotizar.php"); ?>
						</div><!-- ./row 2 -->
					</div><!-- ./search -->

				</div>
				<div class="avanze2 col-sm-4 col-md-3">
				<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div><!-- /.col-md-12 -->
			</div>
			<!--row-->
		</div>
	</div>
	<br /><br /><br />
	<div class="container">
		<div class="section-title line-style no-margin">
			<h2 class="title">Elige tu nuevo hogar</h2>
		</div>

		<div class="my-property" data-navigation=".my-property-nav">
			<div class="crsl-wrap">
			<?php require(__DIR__ . "/../include/grid-propiedades.php"); ?>
			</div>
			<div class="my-property-nav">
				<p class="button-container">
					<a href="#" class="next">siguiente</a>
					<a href="#" class="previous">anterior</a>
				</p>
			</div>
		</div><!-- /.my-property slide -->

	</div><!-- ./container -->
</section><!-- /#about-us -->




<?php require(__DIR__ . "/../include/footer.php"); ?>