<?php
define('og_image', 'images/novedades/05encuentratucasapropia.jpg');
define('og_title', 'Encuentra tu casa propia en Las Lomas de Yura');
define('og_type','website');
define('og_desc','Entre los bienes que podrás adquirir a lo largo de toda tu vida, podría decirse que el más importante es una vivienda. Y es que un inmueble, ya sea casa o apartamento, es más que una propiedad: representa tu hogar, tu estabilidad, tu seguridad, tu estilo de vida, tu tranquilidad individual o familiar para enfrentar el futuro.');
define('keywords', ',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
define('title_complemento', 'Novedades | '.og_title)
?>
<?php define('GPR_SECTION_CLASS','novedades05'); ?>
<?php require(__DIR__ . "/../include/config.php"); ?>
<?php require(__DIR__ . "/../include/header.php"); ?>
<section id="agent-page" class="header-margin-base fixed-no-header page-blog">

	<div class="hero-page">
		<div class="info-hero">
			<h1 class="title-name name">Encuentra tu casa propia en Las Lomas de Yura</h1>
			<div class="info-name cotizar-btn">
				<span class="title">Cotiza ahora tu casa</span>
				<a href="#cotizar"><img src="<?=GPR_ROOT_PATH?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>"  alt="arrow-up" /></a>
			</div>
		</div>
	</div>

	<div class="container">
		<!-- <div class="row indice-content">
			<div class="col-md-12">
				<div  class="section-title">
					<h2 class="title title-grand">Indice de Contenidos</h2>
				</div>
				<li><a href="#indice1">¿Qué Es Nuevo Crédito MI VIVIENDA?</a></li>
				<li><a href="#indice2">¿Por Qué Elegir Nuevo Crédito MI VIVIENDA?</a></li>
				<li><a href="#indice3">¿Cuáles son los Requisitos que necesito?</a></li>
			</div>
		</div> -->
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<!-- <h1 class="name">Financiar tu nuevo hogar es más sencillo con Crédito Mi Vivienda</h1> -->
						<div class="bs-callout callout-info">

							<p class="text">Entre los bienes que podrás adquirir a lo largo de toda tu vida, podría decirse que el más importante es una vivienda. Y es que un inmueble, ya sea casa o apartamento, es más que una propiedad: representa tu hogar, tu estabilidad, tu seguridad, tu estilo de vida, tu tranquilidad individual o familiar para enfrentar el futuro.</p>
						</div>
						<p>Un hogar no es solo el techo que nos protege, también es el lugar donde pasamos la mayor parte de nuestra vida, donde vivimos plenos con nuestra familia y el sitio donde más seguros nos sentimos, por eso, cuando no tenemos vivienda propia, esto se convierte en un sueño por el cual luchamos diariamente.</p>
						<p>Y es que cumplir este sueño no es nada difícil. Es muy simple, y en GPR INMOBILIARIA estamos dispuestos a ayudarte, por eso te presentamos el proyecto inmobiliario más revelador de Arequipa, se trata de <b>Las Lomas de Yura.</b>  Hemos diseñado 2,157 viviendas acogidas al Fondo MiVivienda, que te ofrece créditos hipotecarios que permiten financiar la compra de inmuebles terminados, en construcción o en proyecto. Es un beneficio para que toda la población tenga un espacio propio, con buenos servicios para vivir.</p>
						<p>El proyecto está dispuesto en siete etapas. En estos momentos nos encontramos desarrollando la 1era etapa que consta 244 casas y 40 apartamentos, que suman 288 viviendas. Están acondicionadas con agua, electricidad, desagües, zonas verdes, un puesto de estacionamiento y, lo más importante, el bienestar y confort de sus dueños.</p>
						<p>En cada una de las viviendas hemos cuidado hasta el más mínimo detalle. Todos los espacios han sido pensados para el disfrute de momentos únicos en familia y con amigos, por eso todas las viviendas cuentan con sala, comedor y habitaciones cómodas.</p>
						<p>El proyecto Las Lomas de Yura, se encuentra en una zona perfectamente comunicada: en el Km 17 de la carretera Arequipa - Puno, sector Las Laderas, Distrito de Yura. A escasos minutos de la ciudad y del comercio, y muy cerca de la naturaleza.</p>
						<p>Como valor agregado, recuerda que el tener un título de propiedad te otorga seguridad jurídica por ser el dueño de la vivienda, lo que representa y valida tu inversión. Además de evitar conflictos, favorece tu historial crediticio y la oportunidad de heredarle un bien a tu familia.</p>
						<p>Elige la vivienda que mejor se adapte a tu estilo de vida. Sólo necesitas disponer del 10% del valor de la misma, de la que más te guste. </p>
						<p>Solicita tu cotización a través de nuestra página web o contáctanos mediante nuestros números telefónicos:</p>
						<ul>
							<li>Maria : <b> <a href="tel:+51 923 104 967">+51 923 104 967</a></b>
							</li>
							<li>Daniel : <b> <a href="tel:+51 947 327 082">+51 947 327 082</a></b>
							</li>
							<li>Lidia : <b> <a href="tel:+51 947 326 649">+51 947 326 649</a></b>
							</li>
						</ul>
						<p>También te esperamos en cualquiera de nuestras oficinas ubicadas en diversas zonas de Arequipa.</p>
						<p>Nuestros Asesores de Ventas te guiarán y recomendarán las mejores opciones, adaptadas a tus necesidades.</p>
			

							<div class="cotizar-btn">
								<span class="title">Cotiza ahora tu casa</span>
								<a href="#cotizar"><img src="<?=GPR_ROOT_PATH?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>"  alt="arrow-up" /></a>
							</div>
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div>

		</div><!-- ./row -->
	</div>

	<br /><br /><br />
	<div id=cotizar class="cotizar-vivienda">
		<div class="container">
			<div class="row">
				<div class="avanze1 col-sm-4 col-md-3">
					<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div>
				<div class="col-sm-12 col-md-9">
					<!-- ===================== 
												SEARCH 
										====================== -->
					<div class="line-style no-margin">
						<h2 class="title-grand">Cotizar Vivienda</h2>
					</div>
					<div class="right-box no-margin">
						<div class="row">
							<?php require(__DIR__ . "/../include/form-cotizar.php"); ?>
						</div><!-- ./row 2 -->
					</div><!-- ./search -->

				</div>
				<div class="avanze2 col-sm-4 col-md-3">
					<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div><!-- /.col-md-12 -->
			</div>
			<!--row-->
		</div>
	</div>
	<br /><br /><br />
	<div class="container">
		<div class="section-title line-style no-margin">
			<h2 class="title">Elige tu nuevo hogar</h2>
		</div>

		<div class="my-property" data-navigation=".my-property-nav">
			<div class="crsl-wrap">
				<?php require(__DIR__ . "/../include/grid-propiedades.php"); ?>
			</div>
			<div class="my-property-nav">
				<p class="button-container">
					<a href="#" class="next">siguiente</a>
					<a href="#" class="previous">anterior</a>
				</p>
			</div>
		</div><!-- /.my-property slide -->

	</div><!-- ./container -->
</section><!-- /#about-us -->
<?php require(__DIR__ . "/../include/footer.php"); ?>