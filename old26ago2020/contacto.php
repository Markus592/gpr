<?php
define('og_image', 'images/visita-guiada.jpg');
define('og_title', 'GPR Inmobiliaria Arequipa - Contacto');
define('og_type','website');
define('og_desc','Oficina Central: Av. Aviación #610, KM 06 (Referencia: Interior de Maquinarias ABSI).
Teléfono: 923 104 967
Horario de atención: De Lunes a Viernes de 09:30 a.m a 06:00 p.m y Sábados de 09:30 a.m a 04:15 p.m, los sábados visita nuestro terreno de 02:30 p.m a 04:15 p.m.');
define('keywords','venta Mini Departamento,Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
define('title_complemento','venta Mini Departamento');
?>
<?php require 'include/config.php'; ?>
<?php require 'include/header.php'; ?>
		<section id="header-page" class="header-margin-base">
			<div id="map-canvas" class="header-map"></div>
			<div id="breadcrumb">
				<div class="container">
					<ol class="breadcrumb">
						<li><a href="#"><i class="fa fa-home"></i></a></li>
						<li><a href="#">Inicio</a></li>
						<li class="active">Contacto</li>
					</ol>
				</div>
			</div><!-- /#breadcrumb -->
			<span class="cover"></span>
		</section><!-- /#header -->
		
		<section id="property-content" style="padding-top:30px;">
			<div class="container">
				<div class="row">
					<div class="col-md-9">

						<!-- /.Secondo Row -->
						<div class="row">
							<div class="col-md-4">
								<!-- 9. Mortage -->
								<div class="section-title line-style">
									<h2 class="title">Cotizar Vivienda</h2>
								</div>
								<div class="search-box-page">
									<div class="row">
										<?php require 'include/form-cotizar.php'; ?>
									</div><!-- ./row -->
								</div><!-- ./.search -->								
							</div>
							<div class="col-md-8">							
								<!-- 8. Maps -->
								<div class="section-title line-style">
									<h2 class="title">Visítanos</h2>
								</div>
								<?php require 'include/oficinas-listado.php'; ?>								
							</div>
						</div>
						
					</div>
					<div class="col-md-3">
						<?php require 'include/visita-guiada.php'; ?>						
					</div>
				</div>
			</div>
		</section>
		

<?php require 'include/footer.php'; ?>