	

							
							
							<div class="agent-box-card grey <?php if( !empty($_REQUEST['vivienda'])){ echo ' hidden-xs hidden-sm '; } ?>" >
								<div class="image-content">
									<div class="image image-fill">
										<img alt="GPR Inmobiliaria" src="images/visita-guiada.jpg">
									</div>						
								</div>
								<div class="info-agent">									
									<div class="text">
										<i class="fa fa-quote-left"></i> ¿Te gustaría conocer Las Lomas de Yura? Te llevamos todos los sábados a conocer nuestro proyecto. Solo tienes que separar tu asiento visitando nuestras oficinas o contactando a uno de nuestros asesores. Recuerda hacerlo con tiempo, los cupos son limitados. <i class="fa fa-quote-right"></i>
									</div>
									<ul class="contact">
										<li><a class="icon" href="https://www.facebook.com/gpr.inmobiliaria/" target="_blank"><i class="fa fa-facebook"></i></a></li>
										<li><a class="icon" href="https://instagram.com/gpr_inmobiliaria?igshid=130h4s55e28lp" target="_blank"><i class="fa fa-instagram"></i></a></li>
										<li><a class="icon" href="https://api.whatsapp.com/send?phone=+51933876285&amp;text=Hola%20GPR%20Inmobiliaria,%20envíame%20información%20de%20los%20inmuebles.%20"><i class="fa fa-whatsapp"></i></a></li>
										<li><a class="icon" href="mailto:ventas@gprperu.com?subject=Hola%20GPR%20Inmobiliaria,%20envíame%20información%20de%20los%20inmuebles.%20"><i class="fa fa-envelope-o"></i></a></li>
									</ul>
								</div>
							</div>
							