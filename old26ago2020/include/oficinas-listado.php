
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">No esperes más y ven a visitarnos a nuestras oficinas de venta:</div>							
		<!-- Table -->
		<table class="table">									
			<tr>
				<td><i class="fa fa-map-marker"></i></td>
				<td>
					<p><b>Oficina Central:</b>  Av. Aviación #610, KM 06 (Referencia: Interior de "Maquinarias ABSI").</p>
					<p><b>Teléfono:</b> 923 104 967</p>
					<p><b>Horario de atención:</b> De Lunes a Viernes de 09:30 a.m a 06:00 p.m y Sábados de 09:30 a.m a 04:15 p.m, los sábados visita nuestro terreno de 02:30 p.m a 04:15 p.m.</p>
				</td>
			</tr>
			<tr>
				<td><i class="fa fa-map-marker"></i></td>
				<td>
					<p><b>Of. Terminal Pesquero:</b> Urb. La perla del chachani A-09, Río Seco, Cerro Colorado (Referencia: Paradero de subida del terminal pesquero).</p>
					<p><b>Teléfono:</b> 947 327 082</p>
					<p><b>Horario de atención:</b> De Martes a Sábados de 09:30 a.m a 06:00 p.m, los sábados visita nuestro terreno de 02:30 p.m a 04:15 p.m. y domingos de 09:30 a.m a 5:00 p.m.</p>
				</td>
			</tr>
			<tr>
				<td><i class="fa fa-map-marker"></i></td>
				<td>
					<p><b>Of. Interior Municipalidad de Yura:</b> Arequipa (Referencia: Km. 15.5 Pasando Colegio Solaris, Carretera Arequipa - Yura).</p>
					<p><b>Teléfono:</b> 947 326 649</p>
					<p><b>Horario de atención:</b> De Lunes a Viernes de 09:00 a.m a 05:00 p.m y Sábados de 08:00 a.m a 04:15 p.m</p>
				</td>
			</tr>
		</table>
	</div>
