<?php
/********************************************************
 * GPR_ROOT_PATH : Path Base para usar en imagenes,urls
********************************************************/
$adicional = "";
if($_SERVER["SERVER_NAME"]=="localhost"){
    $exploded_array = explode("/",$_SERVER["REQUEST_URI"]); 
    $adicional = $exploded_array[1]."/";
}else{
    $exploded_array = explode("/",$_SERVER["REQUEST_URI"]); 
    if(count($exploded_array) > 2){
        $adicional = $exploded_array[1]."/";
    }
}
//define('GPR_ROOT_PATH', $_SERVER["REQUEST_SCHEME"]."://".$_SERVER["SERVER_NAME"]."/".$adicional);
define("GPR_ROOT_PATH","https://gprperu.com/");
// Esta variable es para sacar la url actual no tocar por que se usa en el opengraph.
define('GPR_ROOT_PATH_LAST', $_SERVER["REQUEST_SCHEME"]."://".$_SERVER["SERVER_NAME"].$_SERVER['REQUEST_URI']);

/*******************************************************
 *  DEFINE DE PRECIOS
 ******************************************************/
define('GPR_OG_DESC','Nuestro proyecto Lomas de Yura contara con 7 etapas, la primera con 288 viviendas. Ubicado en el Km 17 de la carretera Arequipa - Puno, sector Las Laderas, distrito de Yura. Pensando en cada familia peruana hemos realizado diferentes tipos de vivienda, todas nuestras viviendas cuentan con áreas verdes,estacionamiento y áreas en común para que podamos desarrollarnos plenamente.');

define('GPR_PRICE_MISTI_SEPARACION','28,377.60');
define('GPR_PRICE_MISTI_FINAL','283,776');
define('GPR_PRICE_ALELI_SEPARACION','18,998.80');
define('GPR_PRICE_ALELI_FINAL','180,998');
define('GPR_PRICE_CAPULI_SEPARACION','13,700.00');
define('GPR_PRICE_CAPULI_FINAL','137,000');
define('GPR_PRICE_TEXAO_SEPARACION','8,570.00');
define('GPR_PRICE_TEXAO_FINAL','85,700.00');
define('GPR_PRICE_WITITI_SEPARACION','12,790.00');
define('GPR_PRICE_WITITI_FINAL','127,900');
define('GPR_PRICE_YARAVI_SEPARACION','10,700.00');
define('GPR_PRICE_YARAVI_FINAL','107,000');

?>