

						<figure class="crsl-item" >
							<a href="<?= GPR_ROOT_PATH ?>propiedad/casa-texao">
							<div class="box-ads box-grid">
								<div class="hover-effect image image-fill">
									<span class="cover"></span>
									<img alt="Vivienda" src="<?=GPR_ROOT_PATH?>images/texao_408x251.jpg" title="<?= GPR_ROOT_PATH ?>" >
									<h3 class="title">Casa Texao 35 m<sup>2</sup></h3>
								</div>
								<span class="price">Con un inicial desde S/ <?= GPR_PRICE_TEXAO_SEPARACION ?></span>						
							</div>
							</a>
						</figure>
						<figure class="crsl-item" >
							<a href="<?= GPR_ROOT_PATH ?>propiedad/casa-capuli">
							<div class="box-ads box-grid">								
								<div class="hover-effect image image-fill">
									<span class="cover"></span>
									<img alt="Vivienda" src="<?=GPR_ROOT_PATH?>images/capuli_408x251.jpg" title="<?= GPR_ROOT_PATH ?>" >
									<h3 class="title">Casa Capuli 42 m<sup>2</sup></h3>
								</div>
								<span class="price">Con un inicial desde S/ <?= GPR_PRICE_CAPULI_SEPARACION ?></span>						
							</div>								
							</a>
						</figure>
						<figure class="crsl-item">
							<a href="<?= GPR_ROOT_PATH ?>propiedad/casa-aleli">
							<div class="box-ads box-grid">								
								<div class="hover-effect image image-fill">
									<span class="cover"></span>
									<img alt="Vivienda" src="<?=GPR_ROOT_PATH?>images/aleli_408x251.jpg" title="<?= GPR_ROOT_PATH ?>" >
									<h3 class="title">Casa Aleli 78 m<sup>2</sup></h3>
								</div>
								<span class="price">Con un inicial desde S/ <?= GPR_PRICE_ALELI_SEPARACION ?></span>						
							</a>
						</figure>
						<figure class="crsl-item">
							<a href="<?= GPR_ROOT_PATH ?>propiedad/casa-misti">
							<div class="box-ads box-grid">
								<div class="hover-effect image image-fill">
									<span class="cover"></span>
									<img alt="Vivienda" src="<?=GPR_ROOT_PATH?>images/misti_408x251.jpg" title="<?= GPR_ROOT_PATH ?>" >
									<h3 class="title">Casa Misti 93 m<sup>2</sup></h3>
								</div>
								<span class="price">Con un inicial desde S/ <?= GPR_PRICE_MISTI_SEPARACION ?></span>
							</div>
							</a>
						</figure>						
						<figure class="crsl-item" >
							<a href="<?= GPR_ROOT_PATH ?>propiedad/departamento-yaravi">
							<div class="box-ads box-grid">
								<div class="hover-effect image image-fill">
									<span class="cover"></span>
									<img alt="Vivienda" src="<?=GPR_ROOT_PATH?>images/yaravi_408x251.jpg" title="<?= GPR_ROOT_PATH ?>" >
									<h3 class="title">Dpto. Yaravi 46 m<sup>2</sup></h3>
								</div>
								<span class="price">Con un inicial desde S/ <?= GPR_PRICE_YARAVI_SEPARACION ?></span>						
							</a>
						</figure>
						<figure class="crsl-item" >
							<a href="<?= GPR_ROOT_PATH ?>propiedad/departamento-wititi">
							<div class="box-ads box-grid">
								<div class="hover-effect image image-fill">
									<span class="cover"></span>
									<img alt="Vivienda" src="<?=GPR_ROOT_PATH?>images/wititi_408x251.jpg" title="<?= GPR_ROOT_PATH ?>" >
									<h3 class="title">Dpto. Wititi 53 m<sup>2</sup></h3>
								</div>
								<span class="price">Con un inicial desde S/ <?= GPR_PRICE_WITITI_SEPARACION ?></span>						
							</a>
						</figure>
