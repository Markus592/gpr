

		<footer id="footer-page" class="section-color">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-3">						
						<span class="title">GPR INMOBILIARIA</span>
						<span class="text">
							GPR se propone alcanzar el liderazgo en el mercado inmobiliario de viviendas económicas a lo largo de Perú, usando la tecnología y el máximo de las capacidades de la organización para entregar calidad de vida y mayor valor a nuestros clientes.
						</span>
					</div>
					<div class="col-sm-6 col-md-3">
						<span class="title">LLÁMANOS</span>
						<!--<span class="phone"><i class="fa fa-phone"></i> 933 876 285</span>		
						-->
						<!--<p class="address"><i class="fa fa-phone"></i> 923 104 967 <small>(Of. Central)</small></p>		
						<p class="address"><i class="fa fa-phone"></i> 947 327 082 <small>(Of. Terminal Pesquero)</small></p>		
						<p class="address"><i class="fa fa-phone"></i> 947 326 649 <small>(Of. Interior Municipal de Yura)</small></p>		-->
						<p class="address"><i class="fa fa-phone"></i> 923 104 967 <small>Maria</small></p>		
						<p class="address"><i class="fa fa-phone"></i> 947 327 082 <small>Daniel</small></p>		
						<p class="address"><i class="fa fa-phone"></i> 947 326 649 <small>Lidia</small></p>		
						
					</div>					
					<div class="col-sm-6 col-md-4">
						<span class="title">Oficinas de Venta</span>						
						<ul class="link-extra">
							<li><i class="fa fa-map-marker"></i> <small>Oficina Central:  Av. Aviación #610, KM 06 (Referencia: Interior de "Maquinarias ABSI").</small></li>
							<li><i class="fa fa-map-marker"></i> <small>Of. Terminal Pesquero: Urb. La perla del chachani A-09, Río Seco, Cerro Colorado (Referencia: Paradero de subida del terminal pesquero).</small></li>
							<li><i class="fa fa-map-marker"></i> <small>Of. Interior Municipalidad de Yura: Arequipa (Referencia: Km. 15.5 Pasando Colegio Solaris, Carretera Arequipa - Yura).</small></li>
						</ul>
					</div>
						<div class="hidden-xs hidden-sm col-md-2">
						<span class="title">Viviendas</span>
						<small>
						<ul class="link-extra">
							<li><a href="<?=GPR_ROOT_PATH?>propiedad/casa-misti">Casa Misti (93 m<sup>2</sup>)</a></li>
							<li><a href="<?=GPR_ROOT_PATH?>propiedad/casa-aleli">Casa Aleli (78 m<sup>2</sup>)</a></li>									
							<li><a href="<?=GPR_ROOT_PATH?>propiedad/casa-capuli">Casa Capuli (42 m<sup>2</sup>)</a></li>									
							<li><a href="<?=GPR_ROOT_PATH?>propiedad/casa-texao">Casa Texao (35 m<sup>2</sup>)</a></li>									
							<li><a href="<?=GPR_ROOT_PATH?>propiedad/departamento-wititi">Dpto. Wititi (53 m<sup>2</sup>)</a></li>									
							<li><a href="<?=GPR_ROOT_PATH?>propiedad/departamento-yaravi">Dpto. Yaravi (46 m<sup>2</sup>)</a></li>									
						</ul>
						</small>
					</div>					
				</div>
			</div>
			<div class="credits">
				<div class="container">
					<div class="row">						
						<div class="col-md-6">
							<ul class="social-icons">
								<li><a href="https://www.facebook.com/gpr.inmobiliaria/" target="_blank"><i class="fa fa-facebook"></i></a></li>
								<li><a href="https://instagram.com/gpr_inmobiliaria?igshid=130h4s55e28lp" target="_blank"><i class="fa fa-instagram"></i></a></li>
								<li><a class="messageWhatsapp" href="https://api.whatsapp.com/send?phone=+51933876285&amp;text=Hola%20GPR%20Inmobiliaria,%20envíame%20información%20de%20los%20inmuebles.%20"><i class="fa fa-whatsapp"></i></a></li>								
							</ul>
						</div>
						<div class="col-md-6 credits-text">Copyright 2020 <b>GPR Inmobiliaria</b> | All Rights Reserved | <a href="https://www.idgroup.pe/" target="_blank">Designed By ID </a></div>
					</div>
				</div>
			</div>
			
		</footer>


		
		
		
	</div><!-- /#page-container -->

	<script	src="<?=GPR_ROOT_PATH?>script/jquery.min.js"></script>		<!--jQuery	(necessary for Bootstrap's JavaScript plugins)-->
	<script src="<?=GPR_ROOT_PATH?>script/messageWhatsapp.js"></script>
	<script	src="<?=GPR_ROOT_PATH?>script/jquery-ui.min.js"></script>		<!-- jQuery	UI is a	curated	set	of user	interface interactions,	effects, widgets, and themes -->
	<script	src="<?=GPR_ROOT_PATH?>script/bootstrap.min.js"></script>		<!-- Include all compiled plugins (below), or include individual files as needed -->

	<script	src="<?=GPR_ROOT_PATH?>script/vendor/mmenu/mmenu.min.all.js"></script>					<!-- Menu Responsive -->
	<script	src="<?=GPR_ROOT_PATH?>script/vendor/animation-wow/wow.min.js"></script>					<!-- Animate Script	-->
	<script src="<?=GPR_ROOT_PATH?>script/vendor/labelauty/labelauty.min.js"></script>					<!-- Checkbox Script -->
	<script	src="<?=GPR_ROOT_PATH?>script/vendor/parallax/parallax.min.js"></script>						<!-- Parallax Script -->
	<script	src="<?=GPR_ROOT_PATH?>script/vendor/images-fill/imagesloaded.min.js"></script>			<!-- Loaded	image with ImageFill -->
	<script src="<?=GPR_ROOT_PATH?>script/vendor/images-fill/imagefill.min.js"></script>					<!-- ImageFill Script -->
	<script	src="<?=GPR_ROOT_PATH?>script/vendor/easydropdown/jquery.easydropdown.min.js"></script>	<!-- Select	list Script	-->
	<script	src="<?=GPR_ROOT_PATH?>script/vendor/carousel/responsiveCarousel.min.js"></script>		<!-- Carousel Script -->
	<script	src="<?=GPR_ROOT_PATH?>script/vendor/noui-slider/nouislider.all.min.js"></script>				<!-- Range Slider -->
	
	<?php if(isSection("avances-de-obra")){echo '<script src="'.GPR_ROOT_PATH.'script/vendor/slick-1.8.1/slick/slick.js"></script>';}?>
	<?php if(isSection("avances-de-obra")){echo '<script src="'.GPR_ROOT_PATH.'script/avances-de-obra.js"></script>';}?>

	<script	src="<?=GPR_ROOT_PATH?>script/custom.js"></script>		<!-- Custom	Script -->
	
	<script>
		/* CLOSE SEARCH BOX */
		setTimeout(function(){$('.botton-options').click()}, 1000);
	</script>
	
	
	
	
	
	<?php
	//if( $_REQUEST['vivienda'] != "" ){?>
		<!-- Start: propiedad-detalle.php -->
		<script	src="<?=GPR_ROOT_PATH?>script/vendor/fotorama/fotorama.min.js"></script>	<!-- Fotorama Gallery Images -->
		<script	src="https://maps.google.com/maps/api/js?key=AIzaSyBRofETBIH3WcBieh_MydS8AbJAI04kCms"></script>	<!-- Google Map -->
		<!-- End: propiedad-detalle.php -->		
		<!-- Start: propiedad-detalle.php -->	
		<script>
		function initialize() {
		  var marcadores = [
			['Las Lomas de Yura', -16.2954502, -71.6410959],
			['Oficina Central: 923 104 967', -16.3655846, -71.5607549],
			['Of. Terminal Pesquero: 947 327 082', -16.3457666, -71.5765915],
			['Of. Interior Municipalidad de Yura: 947 326 649', -16.3291284, -71.6782146]
		  ];
		  var map = new google.maps.Map(document.getElementById('map-canvas'), {
			zoom: 12,
			center: new google.maps.LatLng(-16.3255846, -71.6199549),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		  });
		  var infowindow = new google.maps.InfoWindow();
		  var marker, i;
		  for (i = 0; i < marcadores.length; i++) {  
			marker = new google.maps.Marker({
			  position: new google.maps.LatLng(marcadores[i][1], marcadores[i][2]),
			  map: map
			});
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
			  return function() {
				infowindow.setContent(marcadores[i][0]);
				infowindow.open(map, marker);
			  }
			})(marker, i));
		  }
		}
		google.maps.event.addDomListener(window, 'load', initialize);


		</script>
		<!-- End: propiedad-detalle.php -->
		<?php
	//}?>
	
	
	
	
	

  </body>
</html>