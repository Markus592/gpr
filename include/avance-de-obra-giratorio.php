
							<div class="agent-box-card grey <?php if( !empty($_REQUEST['vivienda'])){ echo ' hidden-xs hidden-sm '; } ?>" >
								<div class="image-content image-content-avance">

                                <div class="avance-obra-resumen" data-navigation=".my-property-nav">
                                    <div class="crsl-wrap">
                                        <figure class="crsl-item" >
                                            
                                            <div class="box-ads box-grid">
                                                <div class="hover-effect image image-fill">
                                                    
                                                    <img alt="Avance" src="<?=GPR_ROOT_PATH?>images/avances/avance_obra_urbanizacion_29set2020.jpg" title="<?= GPR_ROOT_PATH ?>" >
                                                </div>
                                               
                                            </div>
                                        </figure>
                                        <figure class="crsl-item" >
                                           
                                            <div class="box-ads box-grid">								
                                                <div class="hover-effect image image-fill">
                                                    
                                                    <img alt="Vivienda" src="<?=GPR_ROOT_PATH?>images/avances/avance_obra_pista_29set2020.jpg" title="<?= GPR_ROOT_PATH ?>" >
                                                </div>
                                                					
                                            </div>								
                                        </figure>
                                        <figure class="crsl-item" >
                                           
                                            <div class="box-ads box-grid">								
                                                <div class="hover-effect image image-fill">
                                                    
                                                    <img alt="Vivienda" src="<?=GPR_ROOT_PATH?>images/avances/avance_obra_reservorio_29set2020.jpg" title="<?= GPR_ROOT_PATH ?>" >
                                                </div>
                                                					
                                            </div>								
                                        </figure>
                                        <figure class="crsl-item" >
                                           
                                            <div class="box-ads box-grid">								
                                                <div class="hover-effect image image-fill">
                                                    
                                                    <img alt="Vivienda" src="<?=GPR_ROOT_PATH?>images/avances/avance_obra_departamentos_29set2020.jpg" title="<?= GPR_ROOT_PATH ?>" >
                                                </div>
                                                					
                                            </div>								
                                        </figure>                                           
                                    </div>

                                </div><!-- /.my-property slide -->                                

								</div>
								<div class="info-agent">
									<span class="name">Continuamos con el proyecto</span>
									<!--<div class="text">
										<i class="fa fa-quote-left"></i> En Las Lomas de Yura seguimos avanzando con nuestro proyecto, instalando campamentos para verificar y monitorear el proceso de construcción. Estamos muy orgullosos y felices de trabajar de manera responsable y consecuente. <i class="fa fa-quote-right"></i>
									</div>-->
									<div class="text">
									    <!--
									        <h4>Edificaciones</h4>
									        <ul>
									            <li>Etapa de Estructuras (Comprende Excavación de Zanjas para Vigas de Cementación, vaceado de plateas, vaceado de muros y losa)</li>
									            <li>Arquitectura (Se está iniciando los trabajos de acabados húmedos que comprenden trabajos en piso para acabado frotachado y solaqueo para recibir empaste)</li>
									        </ul>
									        <h4>Habilitación Urbana</h4>
									        <ul>
									            <li>Excavaci&oacute;n de Zanjas para Redes de Desague</li>
									            <li>Vaceado de Buzones para Redes de Desague</li>
									        </ul>
									        <h4>Obras de Cabecera</h4>
									        <ul>
									            <li>Reservorio etapa de estructuras, excavado al 100% de inicio de trabajos de colicación de concreto.</li>
									        </ul>
									        -->
									</div>
									<ul class="contact">
										<li><a class="icon" href="https://www.facebook.com/gpr.inmobiliaria/" target="_blank"><i class="fa fa-facebook"></i></a></li>
										<li><a class="icon" href="https://instagram.com/gpr_inmobiliaria?igshid=130h4s55e28lp" target="_blank"><i class="fa fa-instagram"></i></a></li>
										<li><a class="icon messageWhatsapp" href="https://api.whatsapp.com/send?phone=+51933876285&amp;text=Hola%20GPR%20Inmobiliaria,%20envíame%20información%20de%20los%20inmuebles.%20"><i class="fa fa-whatsapp"></i></a></li>
										<li><a class="icon" href="mailto:ventas@gprperu.com?subject=Hola%20GPR%20Inmobiliaria,%20envíame%20información%20de%20los%20inmuebles.%20"><i class="fa fa-envelope-o"></i></a></li>
									</ul>
								</div>
							</div>