<?php require 'include/config.php'; ?>
<?php
define('og_image', GPR_ROOT_PATH.'images/slide-thumb.jpg');
define('og_title', 'Avances de Obra - GPR Inmobiliaria  Arequipa');
define('og_type','website');
define('og_desc',GPR_OG_DESC);
define('keywords','Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
define('title',"Avances de Obra ".GPR_TITLE_GENERAL.'Inmobiliaria');
define('GPR_SECTION_CLASS','avances-de-obra');
?>
<?php require 'include/header.php'; ?>
		
		

	

		<!-- Solo aparece en responsivo
		<section class="visible-xs">
			<div class="container"  style="margin-top:150px; padding-right: 0; padding-left: 0;">
				<div class="row">
					<div class="col-md-12">
						<div class="image">
							<img src="<?= GPR_ROOT_PATH ?>images/slide-thumb.jpg" title="<?=GPR_ROOT_PATH?>" alt="Proyecto: Las Lomas de Yura" class="img-responsive" />
						</div>					
					</div>
				</div>
			</div>
		</section>-->
		
		
		<section id="SlickWrapper" class="header-margin-base">
				<div class="slick-slider">
				    	<div class="slide" data-title="Avance de Pistas y Veredas">
						<h2 class="slick-titulo">Avance de Pistas y Veredas</h2>
						<img src="<?= GPR_ROOT_PATH ?>images/avances/avance_obra_pista_29set2020.jpg">
						<div class="slick-descripcion">
							<p>Trabajo Terminado</p>
						</div>
					</div>
					
					<!--<div class="slide" data-title="Obras de Acceso">
						<h2 class="slick-titulo">Obras de Acceso</h2>
						<img src="<?=""// GPR_ROOT_PATH ?>images/avances/avances-obra1-08set2020.jpg">
						<div class="slick-descripcion">
							<p>Trabajo Terminado</p>
						</div>
					</div>-->
					<div class="slide" data-title="Reservorio">
						<h2 class="slick-titulo">Reservorio</h2>
						<img src="<?= GPR_ROOT_PATH ?>images/avances/avance_obra_reservorio_29set2020.jpg">
						<div class="slick-descripcion">
							<p>Inicio Julio 2020</p>
							<p>Plazo de Ejecución: 9 meses</p>
						</div>
					</div>
					<div class="slide" data-title="Urbanizaci&oacute;n">
						<h2 class="slick-titulo">Urbanizaci&oacute;n</h2>
						<img src="<?= GPR_ROOT_PATH ?>images/avances/avance_obra_urbanizacion_29set2020.jpg">
						<div class="slick-descripcion">
							<p>Inicio Julio 2020</p>
							<p>Plazo de Ejecución: 9 meses</p>
						</div>
					</div>
					<div class="slide" data-title="Edificación">
						<h2 class="slick-titulo">Edificaci&oacute;n</h2>
						<img src="<?= GPR_ROOT_PATH ?>images/avances/avances-obra6.jpg">
						<div class="slick-descripcion">
							<p>Inicio Julio 2020</p>
							<p>Plazo de Ejecución: 9 meses</p>
						</div>
					</div>
					<div class="slide" data-title="Departamentos">
						<h2 class="slick-titulo">Departamentos</h2>
						<img src="<?= GPR_ROOT_PATH ?>images/avances/avance_obra_departamentos_29set2020.jpg">
						<div class="slick-descripcion">
							<p>Inicio Julio 2020</p>
							<p>Plazo de Ejecución: 9 meses</p>
						</div>
					</div>
					<!--
					<div class="slide" data-title="Movimiento de Tierra Segunda Etapa">
						<h2 class="slick-titulo">Movimiento de Tierra Segunda Etapa</h2>
						<img src="<?="" // GPR_ROOT_PATH ?>images/avances/avances-obra4.jpg">
						<div class="slick-descripcion">
							<p>Inicio Julio 2020</p>
							<p>Plazo de Ejecución: 9 meses</p>
						</div>
					</div>	-->		
				</div>
		</section>


<?php require 'include/footer2.php'; ?>