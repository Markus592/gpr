<?php require(__DIR__ . "/../include/config.php"); ?>
<?php
define('og_image', 'images/logo.png');
define('og_title', 'CÓMO POSTULAR A MI VIVIENDA Y TRABAJAR CON GPR INMOBILIARIA. ');
define('og_type','website');
define('og_desc','En algunos casos, comprar una propiedad puede convertirse en un proyecto personal o familiar relativamente costoso, que sería imposible de alcanzar sin un préstamo. Por esta razón, se hace necesario el financiamiento bajo la figura de crédito hipotecario, o préstamo para la compra de vivienda, que es la forma clásica que utilizan los bancos para respaldar las adquisiciones. Cualquier persona que sienta interés por conseguir este tipo de crédito, puede decidir entre muchas ofertas que se encuentran en el mercado, sin embargo, en algunos casos, tienen características diferentes como el tipo de tasa de interés y los plazos de pago, entre otros aspectos.');
define('keywords', ',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
define('title',GPR_TITLE_NOVEDADES.og_title);
define('GPR_ACTUAL_URL',GPR_ROOT_PATH."novedades/como-postular-a-mi-vivienda-y-trabajar-con-gpr-inmobiliaria-NOUSAR.php");
define('GPR_SECTION_CLASS','novedades01'); 
?>
<?php require(__DIR__ . "/../include/header.php"); ?>
<section id="agent-page" class="header-margin-base fixed-no-header page-blog">

	<div class="hero-page">
		<div class="info-hero">
			<h1 class="title-name name">CÓMO POSTULAR A MI VIVIENDA Y TRABAJAR CON GPR INMOBILIARIA. </h1>
			<div class="info-name cotizar-btn">
				<span class="title">Cotiza ahora tu casa</span>
				<a href="#cotizar"><img src="<?=GPR_ROOT_PATH?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>"  alt="arrow-up" /></a>
			</div>
		</div>
	</div>

	<div class="container">
		<!-- <div class="row indice-content">
			<div class="col-md-12">
				<div  class="section-title">
					<h2 class="title title-grand">Indice de Contenidos</h2>
				</div>
				<li><a href="#indice1">¿Qué Es Nuevo Crédito MI VIVIENDA?</a></li>
				<li><a href="#indice2">¿Por Qué Elegir Nuevo Crédito MI VIVIENDA?</a></li>
				<li><a href="#indice3">¿Cuáles son los Requisitos que necesito?</a></li>
			</div>
		</div> -->
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-sm-12 col-md-12">

						<?php include "../include/sharebutton.php" ?>

						<!-- <h1 class="name">Financiar tu nuevo hogar es más sencillo con Crédito Mi Vivienda</h1> -->
						<div class="bs-callout callout-info">

							<p class="text">En algunos casos, comprar una propiedad puede convertirse en un proyecto personal o familiar relativamente costoso, que sería imposible de alcanzar sin un préstamo. Por esta razón, se hace necesario el financiamiento bajo la figura de crédito hipotecario, o préstamo para la compra de vivienda, que es la forma clásica que utilizan los bancos para respaldar las adquisiciones. Cualquier persona que sienta interés por conseguir este tipo de crédito, puede decidir entre muchas ofertas que se encuentran en el mercado, sin embargo, en algunos casos, tienen características diferentes como el tipo de tasa de interés y los plazos de pago, entre otros aspectos.</p>
						</div>
						<p>Adicionalmente a los préstamos que ofrece el mercado bancario, en diversos países del mundo existen planes gubernamentales de subsidio que tienen como propuesta básica mejorar la calidad de vida de sus ciudadanos. En Perú, por ejemplo, tenemos la posibilidad de elegir algunos de los subsidios que nos presenta el Fondo Mi Vivienda (FMV), cuyo objetivo es “Promover el acceso a la vivienda única y adecuada, principalmente de las familias con menores ingresos, a través de la articulación entre el Estado y los Sectores Inmobiliario y Financiero, impulsando su desarrollo.” Entre sus alternativas se encuentra el <b> Nuevo Crédito Mi Vivienda</b> (NCMV) que se gestiona a través de Instituciones Financieras Intermediarias (IFI), ubicadas en todo el territorio nacional. </p>
						<p>Para optar o ser elegible a este financiamiento, tenemos que cumplir con ciertos requisitos:</p>
						<ul>
							<li>Tener 18 años cumplidos (mayoría de edad).</li>
							<li>Haber obtenido el calificativo como beneficiario del crédito por parte de alguna Institución Financiera Intermediaria (IFI). En GPR INMOBILIARIA trabajamos directamente con el BBVA.</li>
							<li>Estar solvente. No tener ningún crédito pendiente de pago por el Fondo Mi Vivienda. Es importante destacar que toda persona que solicita un préstamo, será verificada en la lista de Infocorp para conocer su historial crediticio. Si usted se encuentra dentro de este registro con notas de morosidad sobre alguna deuda, la solicitud del crédito puede verse comprometida de forma desfavorable.</li>
							<li>No puede ser propietario o co-propietario de otra vivienda a nivel nacional. El solicitante y/o, en su caso, su cónyuge o su conviviente legalmente reconocido independientemente de su régimen patrimonial e hijos menores de edad, no pueden ser propietarios o copropietarios de otra vivienda en cualquier localidad del país.</li>
							<li>Contar con una cuota mínima inicial del 7,5% del valor de la vivienda que se quiere comprar. Al respecto, los montos de financiamiento se ubican a partir de S/ 60,000 hasta S/ 427,600. A manera de modelo, si usted desea comprar una vivienda por el valor de S/ 84,100 debe tener una inicial de S/ 6,307.5</li>
							<li>Los plazos de pago están ubicados entre 05 y 20 años.</li>
							<li>La tasa de interés es fija y su pago es en Soles.</li>
						</ul>
						<div id=indice1 class="section-title">
							<h2 class="title">¿Por qué trabajar con GPR INMOBILIARIA?</h2>
						</div>
						<p>En GPR INMOBILIARIA comprendemos el significado de la combinación perfecta entre calidad y economía, por eso hemos desarrollado en todo el país diversos sistemas constructivos habitacionales a bajos precios, que ofrecen mejores condiciones de vida en espacios que van desde 35M<sup>2</sup> hasta 93 M<sup>2</sup>, con las siguientes características:</p>
						<ul>
							<li>Todas las casas son elaboradas en concreto, y en áreas de terreno de 90M<sup>2</sup>. Esto constituye una maravillosa ventaja, ya que le permitirá (de forma inmediata o a largo plazo) construir una nueva planta en la parte superior del inmueble.</li>
							<li>Las viviendas están acondicionadas con todos los servicios básicos necesarios como agua, desagües y electricidad.</li>
							<li>En su totalidad, estas residencias disponen de áreas verdes recreativas y jardinería.</li>
							<li>Poseen un puesto de estacionamiento.</li>
							<li>Son viviendas de corte ecológico, que permiten el aprovechamiento del entorno natural.</li>
						</ul>
						<p>GPR INMOBILIARIA trabaja con los programas crediticios que otorga el Fondo Mi Vivienda, manteniendo además alianzas con el BBVA.</p>
						<p>Disponemos de oficinas a nivel nacional, donde recibirá un trato personalizado. Recuerde que, a través de nuestros asesores comerciales, le guiamos y orientamos para que su compra pueda ser efectiva.</p>
						<ul>
							<li>Maria : <b> <a href="tel:+51 923 104 967">+51 923 104 967</a></b>
							</li>
							<li>Daniel : <b> <a href="tel:+51 947 327 082">+51 947 327 082</a></b>
							</li>
							<li>Lidia : <b> <a href="tel:+51 947 326 649">+51 947 326 649</a></b>
							</li>
						</ul>
						<p>Contáctenos y solicite su cotización.</p>

							<div class="cotizar-btn">
								<span class="title">Cotiza ahora tu casa</span>
								<a href="#cotizar"><img src="<?=GPR_ROOT_PATH?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>"  alt="arrow-up" /></a>
							</div>
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div>

		</div><!-- ./row -->
	</div>

	<br /><br /><br />
	<div id=cotizar class="cotizar-vivienda">
		<div class="container">
			<div class="row">
				<div class="avanze1 col-sm-4 col-md-3">
					<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div>
				<div class="col-sm-12 col-md-9">
					<!-- ===================== 
												SEARCH 
										====================== -->
					<div class="line-style no-margin">
						<h2 class="title-grand">Cotizar Vivienda</h2>
					</div>
					<div class="right-box no-margin">
						<div class="row">
							<?php require(__DIR__ . "/../include/form-cotizar.php"); ?>
						</div><!-- ./row 2 -->
					</div><!-- ./search -->

				</div>
				<div class="avanze2 col-sm-4 col-md-3">
					<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div><!-- /.col-md-12 -->
			</div>
			<!--row-->
		</div>
	</div>
	<br /><br /><br />
	<div class="container">
		<div class="section-title line-style no-margin">
			<h2 class="title">Elige tu nuevo hogar</h2>
		</div>

		<div class="my-property" data-navigation=".my-property-nav">
			<div class="crsl-wrap">
				<?php require(__DIR__ . "/../include/grid-propiedades.php"); ?>
			</div>
			<div class="my-property-nav">
				<p class="button-container">
					<a href="#" class="next">siguiente</a>
					<a href="#" class="previous">anterior</a>
				</p>
			</div>
		</div><!-- /.my-property slide -->

	</div><!-- ./container -->
</section><!-- /#about-us -->
<?php require(__DIR__ . "/../include/footer2.php"); ?>