<?php require(__DIR__ . "/../include/config.php"); ?>
<?php
define('og_image', 'images/novedades/06adquieretuvivienda.jpg');
define('og_title', '¡Adquiere tu vivienda, te ayudamos!');
define('og_type', 'website');
define('og_desc', 'Nos enfrentamos a una nueva realidad mundial debido a la pandemia, pero esto también nos lleva a buscar nuevas oportunidades de avances. Esta crisis, que sufrimos todos, nos ha mostrado lo importante que es tener una vivienda propia: sin pagos de alquileres, con habitaciones para teletrabajo, con espacios para albergar o aislar a familiares con salud comprometida; además con las posibilidades de distracción en zonas verdes. Todo esto te lo ofrecemos en el proyecto inmobiliario Lomas de Yura.');
define('keywords', ',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
define('title',GPR_TITLE_NOVEDADES.og_title);
define('GPR_ACTUAL_URL',GPR_ROOT_PATH."novedades/adquiere-tu-vivienda-te-ayudamos.php");
define('GPR_SECTION_CLASS','novedades06');
?>

<?php require(__DIR__ . "/../include/header.php"); ?>

<section id="agent-page" class="header-margin-base fixed-no-header page-blog">

	<div class="hero-page">
		<div class="info-hero">
			<h1 class="title-name name">¡Adquiere tu vivienda, te ayudamos!</h1>
			<div class="info-name cotizar-btn">
				<span class="title">Cotiza ahora tu casa</span>
				<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" alt="arrow-up" title="<?= GPR_ROOT_PATH ?>como-postular-a-mi-vivienda-y-las-ventajas-que-ofrece-gpr-inmobiliaria" /></a>
			</div>
		</div>
	</div>

	<div class="container">
		<!-- <div class="row indice-content">
			<div class="col-md-12">
				<div  class="section-title">
					<h2 class="title title-grand">Indice de Contenidos</h2>
				</div>
				<li><a href="#indice1">¿Qué Es Nuevo Crédito MI VIVIENDA?</a></li>
				<li><a href="#indice2">¿Por Qué Elegir Nuevo Crédito MI VIVIENDA?</a></li>
				<li><a href="#indice3">¿Cuáles son los Requisitos que necesito?</a></li>
			</div>
		</div> -->
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-sm-12 col-md-12">

						<p><?php include "../include/sharebutton.php" ?></p>

						<!-- <h1 class="name">Financiar tu nuevo hogar es más sencillo con Crédito Mi Vivienda</h1> -->
						<p>Nos enfrentamos a una nueva realidad mundial debido a la pandemia, pero esto también nos lleva a buscar nuevas oportunidades de avances. Esta crisis, que sufrimos todos, nos ha mostrado lo importante que es tener una vivienda propia: sin pagos de alquileres, con habitaciones para teletrabajo, con espacios para albergar o aislar a familiares con salud comprometida; además con las posibilidades de distracción en zonas verdes. Todo esto te lo ofrecemos en el proyecto inmobiliario Lomas de Yura.</p>
						<p>Recientemente, para ayudar a la población, el Gobierno emitió el Decreto Legislativo Nº 1464, mediante el cual se amplían los beneficios del programa Techo Propio, que tiene como finalidad incentivar al pueblo peruano a la compra de viviendas nuevas, que mejoren su calidad de vida. Este bono es totalmente gratuito, pero hay que cumplir una serie de requisitos como tener un ahorro mínimo; sin embargo, esta nueva norma excluye a los beneficiarios (hasta el 31 de diciembre de 2020) de cumplir con este requerimiento.</p>
						<p>Debido al estado de emergencia sanitaria, y a la cuarentena promulgada por el Gobierno nacional, muchos de los potenciales beneficiarios del Bono Familiar Habitacional <b>(BFH)</b> han tenido que disponer de sus recursos o ahorros, incrementando así su situación de vulnerabilidad. Por este motivo, el Ministerio de Vivienda Construcción y Saneamiento (MVCS), aumentó el BFH que se otorga para la <b>modalidad de Adquisición de Vivienda Nueva</b> en el marco del programa Techo Propio. De acuerdo al portal del Fondo MIVIVIENDA, el monto <b>se ha ubicado en S/ 37,625.</b> </p>
						<p>La nueva norma señala que <i>“es necesario que se adopten medidas que permitan que todos puedan acceder a una vivienda adecuada que les dé cobijo y protección, para que a través de Techo Propio, el cual se desarrolla con el otorgamiento del BHF, se establezcan disposiciones que faciliten su atención”</i>.</p>
						<p>Lo primero que debes hacer para beneficiarte del programa Techo Propio, es registrar a tu Grupo Familiar en las oficinas que encontrarás en la página <b> <a href="http://www.mivivienda.com.pe./">www.mivivienda.com.pe.</a> </b></p>
						<p>Allí deberás consignar el DNI del jefe de familia y cónyuge, además de los datos de todos los miembros del Grupo Familiar (nombre, DNI, fecha de nacimiento).</p>
						<p>
							Nosotros también podemos ayudarte ya que somos Centro Autorizado por el Fondo Mi Vivienda.
						</p>
						<p>No dejes pasar esta oportunidad. Puedes optar a nuestras casas o apartamentos en Lomas de Yura, sin trámites complejos y con la mejor orientación que sólo puede ofrecerte GPR INMOBILIARIA.</p>
						<p>Acércate a cualquiera de nuestras oficinas ubicadas en Arequipa, o llámanos a través de los números telefónicos:</p>
						<ul>
							<li>Maria : <b> <a href="tel:+51 923 104 967">+51 923 104 967</a></b>
							</li>
							<li>Daniel : <b> <a href="tel:+51 947 327 082">+51 947 327 082</a></b>
							</li>
							<li>Lidia : <b> <a href="tel:+51 947 326 649">+51 947 326 649</a></b>
							</li>
						</ul>
						<p>Serás atendido directamente por nuestros Asesores de Ventas.</p>
						<div class="cotizar-btn">
							<span class="title">Cotiza ahora tu casa</span>
							<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>como-postular-a-mi-vivienda-y-las-ventajas-que-ofrece-gpr-inmobiliaria" alt="arrow-up" /></a>
						</div>
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div>

		</div><!-- ./row -->
	</div>
	<br /><br /><br />
	<div id=cotizar class="cotizar-vivienda">
		<div class="container">
			<div class="row">
				<div class="avanze1 col-sm-4 col-md-3">
					<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div>
				<div class="col-sm-12 col-md-9">
					<!-- ===================== 
												SEARCH 
										====================== -->
					<div class="line-style no-margin">
						<h2 class="title-grand">Cotizar Vivienda</h2>
					</div>
					<div class="right-box no-margin">
						<div class="row">
							<?php require(__DIR__ . "/../include/form-cotizar.php"); ?>
						</div><!-- ./row 2 -->
					</div><!-- ./search -->

				</div>
				<div class="avanze2 col-sm-4 col-md-3">
					<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div><!-- /.col-md-12 -->
			</div>
			<!--row-->
		</div>
	</div>
	<br /><br /><br />
	<div class="container">
		<div class="section-title line-style no-margin">
			<h2 class="title">Elige tu nuevo hogar</h2>
		</div>

		<div class="my-property" data-navigation=".my-property-nav">
			<div class="crsl-wrap">
				<?php require(__DIR__ . "/../include/grid-propiedades.php"); ?>
			</div>
			<div class="my-property-nav">
				<p class="button-container">
					<a href="#" class="next">siguiente</a>
					<a href="#" class="previous">anterior</a>
				</p>
			</div>
		</div><!-- /.my-property slide -->

	</div><!-- ./container -->
</section><!-- /#about-us -->
<?php require(__DIR__ . "/../include/footer2.php"); ?>