<?php require(__DIR__ . "/../include/config.php"); ?>
<?php
define('og_image', 'images/novedades/aleli-11.jpg');
define('og_title', 'Conoce los beneficios del Nuevo Crédito MIVIVIENDA');
define('og_type', 'website');
define('og_desc', 'El Nuevo Crédito MIVIVIENDA es un préstamo hipotecario que permite financiar la compra de vivienda terminada, en construcción o en proyecto, que sean de primera venta o usadas.');
define('keywords', ',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
define('title',GPR_TITLE_NOVEDADES.og_title);
define('GPR_ACTUAL_URL',GPR_ROOT_PATH."novedades/blog-nuevo-credito-mi-vivienda-beneficios.php");
define('GPR_SECTION_CLASS','novedades02');
?>
<?php require(__DIR__ . "/../include/header.php"); ?>


<section id="agent-page" class="header-margin-base fixed-no-header page-blog">
	
	<div class="hero-page">
		<div class="info-hero">
			<h1 class="title-name name">Conoce los beneficios del Nuevo Crédito MIVIVIENDA</h1>
			<div class="info-name cotizar-btn">
				<span class="title">Cotiza ahora tu casa</span>	
				<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>" alt="arrow-up" /></a>
			</div>
		</div> 
	</div>
	
	<div class="container">
		<div class="row indice-content">
			<div class="col-md-12">
				<p><?php include "../include/sharebutton.php" ?></p>
				<div  class="section-title">
					<h2 class="title title-grand">Indice de Contenidos</h2>
				</div>
				<li><a href="#indice1">Acceso A Cualquier Tipo De Vivienda</a></li>
				<li><a href="#indice2">Bono Del Buen Pagador (BBP)</a></li>
				<li><a href="#indice3">Bono MIVIVIENDA Verde (BMV)</a></li>
				<li><a href="#indice4">Financiamiento</a></li>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
				<div class="col-sm-12 col-md-12">



								<div class="bs-callout callout-info">
									<h4 class="title">El Nuevo Crédito MIVIVIENDA es un préstamo hipotecario que permite financiar la compra de vivienda terminada, en construcción o en proyecto, que sean de primera venta o usadas.</h4>
									<p class="text">Este financiamiento aplica para todas las personas sin importar su nivel de ingresos o nivel socioeconómico. Además, ofrece los siguientes beneficios:</p>
								</div>
								<div id="indice1" class="section-title"><h2 class="title">Acceso a cualquier tipo de vivienda</h2></div>
								<p>Sea nueva o usada, cuyo valor este en el rango de S/58,800 y S/419,600</p>
								<div id="indice2" class="section-title"><h2 class="title">Bono del Buen Pagador (BBP)</h2></div>
								<p>El beneficio principal del Nuevo Crédito MIVIVIENDA es el BBP, un bono de hasta S/.17,700 que varía de acuerdo al precio de casa o departamento que se adquiere.</p>
								<div id="indice3" class="section-title"><h2 class="title">Bono MIVIVIENDA Verde (BMV)</h2></div>
								<p>El Fondo MIVIVIENDA otorga este bono como un porcentaje (3% o 4%) del valor del financiamiento según el grado de sostenibilidad para la adquisición de una vivienda verde en un proyecto certificado.</p>							
								<div class="panel panel-default">
									<!-- Default panel contents -->
									<div class="panel-heading">MIVIVIENDA también ofrece el Premio al Buen Pagador como complemento de la cuota inicial (PBP)</div>								
								</div>							
								<div id="indice4" class="section-title"><h2 class="title">Financiamiento </h2></div>								
								<ul>
									<li>Te financiamos como máximo hasta el 90% del valor de vivienda.</li>
									<li><b>Tu cuota de pago siempre será la misma</b>.</li>
									<li><b>Puedes realizar prepagos sin penalidad</b>.</li>
								</ul>	
								<div class="cotizar-btn">
							<span class="title">Cotiza ahora tu casa</span>
							<a href="#cotizar"><img src="<?= GPR_ROOT_PATH ?>images/arrow-up.png" title="<?= GPR_ROOT_PATH ?>" alt="arrow-up" /></a>
						</div>							
							</div><!-- /.col-md-12 -->
							</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div>

		</div><!-- ./row -->
	</div>
	<br /><br /><br />
	<div id=cotizar class="cotizar-vivienda">
		<div class="container">
			<div class="row">
				<div class="avanze1 col-sm-4 col-md-3">
				<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div>
				<div class="col-sm-12 col-md-9">
					<!-- ===================== 
												SEARCH 
										====================== -->
					<div class="line-style no-margin">
						<h2 class="title-grand">Cotizar Vivienda</h2>
					</div>
					<div class="right-box no-margin">
						<div class="row">
						<?php require(__DIR__ . "/../include/form-cotizar.php"); ?>
						</div><!-- ./row 2 -->
					</div><!-- ./search -->

				</div>
				<div class="avanze2 col-sm-4 col-md-3">
				<?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
				</div><!-- /.col-md-12 -->
			</div>
			<!--row-->
		</div>
	</div>
	<br /><br /><br />
	<div class="container">
		<div class="section-title line-style no-margin">
			<h2 class="title">Elige tu nuevo hogar</h2>
		</div>

		<div class="my-property" data-navigation=".my-property-nav">
			<div class="crsl-wrap">
			<?php require(__DIR__ . "/../include/grid-propiedades.php"); ?>
			</div>
			<div class="my-property-nav">
				<p class="button-container">
					<a href="#" class="next">siguiente</a>
					<a href="#" class="previous">anterior</a>
				</p>
			</div>
		</div><!-- /.my-property slide -->

	</div><!-- ./container -->
</section><!-- /#about-us -->




<?php require(__DIR__ . "/../include/footer2.php"); ?>