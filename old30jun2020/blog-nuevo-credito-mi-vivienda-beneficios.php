<?php require 'include/header.php'; ?>


		<section id="agent-page" class="header-margin-base fixed-no-header">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-sm-8 col-md-8 col-sm-push-4">
								<h1 class="name">Conoce los beneficios del Nuevo Crédito MIVIVIENDA</h1>
								<div class="bs-callout callout-info">
									<h4 class="title">El Nuevo Crédito MIVIVIENDA es un préstamo hipotecario que permite financiar la compra de vivienda terminada, en construcción o en proyecto, que sean de primera venta o usadas.</h4>
									<p class="text">Este financiamiento aplica para todas las personas sin importar su nivel de ingresos o nivel socioeconómico. Además, ofrece los siguientes beneficios:</p>
								</div>
								<div class="section-title"><h3 class="title">Acceso a cualquier tipo de vivienda</h3></div>
								<p>Sea nueva o usada, cuyo valor este en el rango de S/58,800 y S/419,600</p>
								<div class="section-title"><h3 class="title">Bono del Buen Pagador (BBP)</h3></div>
								<p>El beneficio principal del Nuevo Crédito MIVIVIENDA es el BBP, un bono de hasta S/.17,700 que varía de acuerdo al precio de casa o departamento que se adquiere.</p>
								<div class="section-title"><h3 class="title">Bono MIVIVIENDA Verde (BMV)</h3></div>
								<p>El Fondo MIVIVIENDA otorga este bono como un porcentaje (3% o 4%) del valor del financiamiento según el grado de sostenibilidad para la adquisición de una vivienda verde en un proyecto certificado.</p>							
								<div class="panel panel-default">
									<!-- Default panel contents -->
									<div class="panel-heading">MIVIVIENDA también ofrece el Premio al Buen Pagador como complemento de la cuota inicial (PBP)</div>								
								</div>							
								<div class="section-title"><h3 class="title">Financiamiento </h3></div>								
								<ul>
									<li>Te financiamos como máximo hasta el 90% del valor de vivienda.</li>
									<li><b>Tu cuota de pago siempre será la misma</b>.</li>
									<li><b>Puedes realizar prepagos sin penalidad</b>.</li>
								</ul>								
							</div><!-- /.col-md-8 -->
							<div class="col-sm-4 col-md-4 col-sm-pull-8">																
								<?php require 'include/avance-de-obra.php'; ?>
							</div><!-- /.col-md-4 -->							
						</div><!-- /.row -->
					</div>
					<div class="col-sm-12 col-md-3">
						<!-- ===================== 
								  SEARCH 
						====================== -->
						<div class="section-title line-style no-margin">
							<h3 class="title">Cotizar Vivienda</h3>
						</div>
						<div class="right-box no-margin">
							<div class="row">							
								<?php require 'include/form-cotizar.php'; ?>								
							</div><!-- ./row 2 -->	
						</div><!-- ./search -->

					</div><!-- ./col-md-3 -->
				</div><!-- ./row -->

				<br /><br /><br /> 
				
				<div class="section-title line-style no-margin">
					<h3 class="title">Elige tu nuevo hogar</h3>
				</div>

				<div class="my-property" data-navigation=".my-property-nav">
					<div class="crsl-wrap">
						<?php require 'include/grid-propiedades.php'; ?>					
					</div>
					<div class="my-property-nav">
						<p class="button-container">
							<a href="#" class="next">siguiente</a>
							<a href="#" class="previous">anterior</a>
						</p>
					</div>
				</div><!-- /.my-property slide -->

			</div><!-- ./container -->
		</section><!-- /#about-us -->





<?php require 'include/footer.php'; ?>