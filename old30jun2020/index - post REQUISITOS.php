<?php require 'include/header.php'; ?>
		

		<section id="home-slide" class="header-margin-base hidden-xs">
			<div class="home-slider carousel" data-navigation=".home-slider-nav">
				<div class="crsl-wrap">				
				
					<figure class="crsl-item" data-image="images/s1-banner-principal.jpg">
						<div class="container slider-box">
							<div class="content"><h2>PROYECTO</h2></div>
							<div class="content"><h1>LAS LOMAS DE YURA</h1></div>
						</div>
					</figure>

					<figure class="crsl-item" data-image="images/s2-primera-etapa.jpg">
						<div class="container slider-box">							
						</div>
					</figure>
					<figure class="crsl-item" data-image="images/s3-misti-fachada.jpg">
						<div class="container slider-box">							
						</div>
					</figure>
					<figure class="crsl-item" data-image="images/s4-aleli-sala.jpg">
						<div class="container slider-box">							
						</div>
					</figure>

					<figure class="crsl-item" data-image="images/s5-capuli-comedor.jpg">
						<div class="container slider-box">
							
						</div>
					</figure>
					<figure class="crsl-item" data-image="images/s6-texao-cocina.jpg">
						<div class="container slider-box">
							
						</div>
					</figure>	
					<figure class="crsl-item" data-image="images/s7-wititi-fachada.jpg">
						<div class="container slider-box">
							
						</div>
					</figure>	
					<figure class="crsl-item" data-image="images/s8-yaravi-dormitorio.jpg">
						<div class="container slider-box">
							
						</div>
					</figure>					
				</div>
				<p class="home-slider-nav previus">
					<a href="#" class="previous">anterior</a>
				</p>
				<p class="home-slider-nav next">
					<a href="#" class="next">sigiente</a>
				</p>
			</div>
		</section>
		

		<section id="search-box" class="no-margin hidden-xs">
			<div class="container search-container fixed-map">
				<div class="search-options sample-page">
					<span class="botton-options"><i class="fa fa-chevron-down"></i> Cotizar inmuebles</span>
					<div class="searcher">
						<FORM method="post" action="contact.php" class="form-large" role="form" data-toggle="validator">
						<input type="hidden" id="proyecto" name="proyecto" value="LAS LOMAS DE YURA">
						<div class="row margin-div">
							<div class="col-sm-6 col-md-3 margin-bottom">
								<select class="dropdown" data-settings='{"cutOff": 4}' name="vivienda" id="vivienda" required >
									<option value="">-- Elige tu vivienda --</option>
									<option value="Casa Misti">Casa Misti (93 m2)</option>
									<option value="Casa Aleli">Casa Aleli (78 m2)</option>
									<option value="Casa Capuli">Casa Capuli (42 m2)</option>
									<option value="Casa Texao">Casa Texao (35 m2)</option>
									<option value="Dpto. Wititi">Dpto. Wititi (53 m2)</option>
									<option value="Dpto. Yaravi">Dpto. Yaravi (46 m2)</option>
								</select>
							</div>
							<div class="col-sm-6 col-md-3 margin-bottom">
								<input class="form-control" type="text" name="nombre" id="nombre" placeholder="Nombres" required />
							</div>
							<div class="col-sm-6 col-md-3">
								<input class="form-control" type="text" name="apellido" id="apellido" placeholder="Apellidos" required />
							</div>		
							<div class="col-sm-6 col-md-3 margin-bottom">
								<input class="form-control" type="email" name="email" id="email" placeholder="Email" required />
							</div>
						
						</div><!-- ./row 1 -->
						<div class="row margin-div sercher-margin-bottom">						
							<div class="col-sm-6 col-md-3">
								<input class="form-control" type="text" name="telefono" id="telefono" placeholder="Tel&eacute;fono" required />
							</div>							
							<div class="col-sm-6 col-md-3">
								<select class="dropdown" data-settings='{"cutOff": 3}' name="tipodocumento" id="tipodocumento" required >
									<option value="">-- Tipo de documento --</option>
									<option value="DNI" selected="selected">DNI</option>
									<option value="RUC">RUC</option>
									<option value="CE">Carné de extranjería</option>
									<option value="PASAPORTE">Pasaporte</option>                        
								</select>
							</div>
							<div class="col-sm-6 col-md-3">
								<input class="form-control" type="text" name="nrodocumento" id="nrodocumento" placeholder="Nro. de documento" required />
							</div>
							<div class="col-sm-6 col-md-3">
								<select class="dropdown" data-settings='{"cutOff": 3}' name="ciudad" id="ciudad" required >
									<option value="">-- Ciudad --</option>
									<option value="Amazonas">Amazonas</option>
									<option value="Ancash">Ancash</option>
									<option value="Apurimac">Apurimac</option>
									<option value="Arequipa" selected="selected">Arequipa</option>
									<option value="Ayacucho">Ayacucho</option>
									<option value="Cajamarca">Cajamarca</option>
									<option value="Callao">Callao</option>
									<option value="Cusco">Cusco</option>
									<option value="Huancavelica">Huancavelica</option>
									<option value="Huanuco">Huanuco</option>
									<option value="Ica">Ica</option>
									<option value="Junín">Junín</option>
									<option value="La Libertad">La Libertad</option>
									<option value="Lambayeque">Lambayeque</option>
									<option value="Lima">Lima</option>
									<option value="Loreto">Loreto</option>
									<option value="Madre de Dios">Madre de Dios</option>
									<option value="Moquegua">Moquegua</option>
									<option value="Pasco">Pasco</option>
									<option value="Piura">Piura</option>
									<option value="Puno">Puno</option>
									<option value="San Martin">San Martin</option>
									<option value="Tacna">Tacna</option>
									<option value="Tumbes">Tumbes</option>
									<option value="Ucayali">Ucayali</option>
								</select>
							</div>
							
						</div><!-- ./row 2 -->
						<div class="row filter hide-filter hidden-xs hidden-sm">
							<div class="col-xs-6 col-sm-4 col-md-3"><input class="labelauty" type="checkbox" data-labelauty="dssadsa asda" checked></div>
							<div class="col-xs-6 col-sm-4 col-md-3"><input class="labelauty" type="checkbox" data-labelauty="asda asdsad" checked></div>
							<div class="col-xs-6 col-sm-4 col-md-3"><input class="labelauty" type="checkbox" data-labelauty="asdsad asda asda"></div>
							<div class="col-xs-6 col-sm-4 col-md-3"><input class="labelauty" type="checkbox" data-labelauty="asdsad asda" checked></div>
							<div class="col-xs-6 col-sm-4 col-md-3"><input class="labelauty" type="checkbox" data-labelauty="asdad-asdadasdsa" checked></div>
							<div class="col-xs-6 col-sm-4 col-md-3"><input class="labelauty" type="checkbox" data-labelauty="asdsad asdsads"></div>
							<div class="col-xs-6 col-sm-4 col-md-3"><input class="labelauty" type="checkbox" data-labelauty="sadsad sadasd" checked></div>
							<div class="col-xs-6 col-sm-4 col-md-3"><input class="labelauty" type="checkbox" data-labelauty="asdsad asdsa"></div>
							<div class="col-xs-6 col-sm-4 col-md-3"><input class="labelauty" type="checkbox" data-labelauty="sadsad" checked></div>
							<div class="col-xs-6 col-sm-4 col-md-3"><input class="labelauty" type="checkbox" data-labelauty="sadsasd"></div>
							<div class="col-xs-6 col-sm-4 col-md-3"><input class="labelauty" type="checkbox" data-labelauty="asdsad sadsa" checked></div>
							<div class="col-xs-6 col-sm-4 col-md-3"><input class="labelauty" type="checkbox" data-labelauty="sadsad sadad" checked></div>
							<div class="col-xs-6 col-sm-4 col-md-3"><input class="labelauty" type="checkbox" data-labelauty="sadsa"></div>
							<div class="col-xs-6 col-sm-4 col-md-3"><input class="labelauty" type="checkbox" data-labelauty="sadasd ada" checked></div>
							<div class="col-xs-6 col-sm-4 col-md-3"><input class="labelauty" type="checkbox" data-labelauty="asdasd asdd"></div>
							<div class="col-xs-6 col-sm-4 col-md-3"><input class="labelauty" type="checkbox" data-labelauty="asdads asdasd"></div>
							<div class="col-xs-6 col-sm-4 col-md-3"><input class="labelauty" type="checkbox" data-labelauty="asdasd" checked></div>
							<div class="col-xs-6 col-sm-4 col-md-3"><input class="labelauty" type="checkbox" data-labelauty="adsadas" checked></div>
						</div><!-- ./filter -->
						<div class="margin-div footer">
						
							<input id="submit" name="submit" type="submit" value="COTIZAR" class="btn btn-default search-button">
							
						</div><!-- ./footer -->
						</FORM>
					</div><!-- ./searcher -->
				</div><!-- search-options -->
			</div><!-- search-container fixed-map -->
		</section>


		
		<section class="visible-xs">
			<div class="container"  style="margin-top:150px; padding-right: 0; padding-left: 0;">
				<div class="row">
					<div class="col-md-12">
						<div class="image">
							<img src="images/slide-thumb.jpg" alt="Proyecto: Las Lomas de Yura" class="img-responsive" />
						</div>					
					</div>
				</div>
			</div>
		</section>
		
		
		<section id="agency">
			<div class="section-detail">
				<h1>PROYECTO: LAS LOMAS DE YURA</h1>				
			</div>
			<div class="container">
				<div class="row agency-box">
					<div class="col-sm-6 col-md-5">
						<div class="fotorama">
						  <a href="https://www.youtube.com/watch?v=ZOR7Ix_qZcw">Proyecto: LAS LOMAS DE YURA</a>  
						</div>						
					</div>
					<div class="col-sm-6 col-md-7 center-box " style="padding-top:15px; padding-bottom:18px;">
						<h1 class="title"><a href="#">Una Gran Ciudad en Yura</a></h1>
						<h3 class="subtitle"><i class="fa fa-map-marker"></i> Arequipa - Perú.</h3>
						<br><p>Nuestro proyecto Lomas de Yura contara con 7 etapas, la primera con 288 viviendas. Ubicado en el Km 17 de la carretera Arequipa - Puno, sector Las Laderas, distrito de Yura.</p>
						<p>Pensando en cada familia peruana hemos realizado diferentes tipos de vivienda, todas nuestras viviendas cuentan con áreas verdes, cocheras y áreas en común para que podamos desarrollarnos plenamente.</p>
						<p>¡Ideal para todo tipo de familia!</p>
						<p>No esperes más y ven a visitarnos.</p>						
					</div>
				</div>
			</div>
		</section>




		<section id="sidebar">
			<div class="container">		
			
			
			
				<div class="row">
					<div class="col-sm-8 col-md-9">
					
						<div class="list-box-title">
							<span><i class="icon fa fa-plus-square"></i>Casas de 2 pisos</span>
						</div>									
						<div class="row">
							<div class="col-md-6">
								<div class="box-ads box-home">
									<a class="hover-effect image image-fill" href="propiedad-detalle.php?vivienda=misti">
										<span class="cover"></span>
										<img alt="Vivienda" src="images/misti_406x200.jpg">
										<h3 class="title">Casa Misti 93 m<sup>2</sup></h3>
									</a><!-- /.hover-effect -->
									<span class="price"> Desde S/.281,776</span>
									<span class="address"><i class="fa fa-map-marker"></i> Dos pisos, Yura, Arequipa</span>
									<span class="description"><strong>Primer Nivel:</strong> Sala, comedor, cocina, lavandería, baño completo, 01 estudio, escaleras interiores, cochera, áreas verdes.<br><strong>Segundo Nivel:</strong> 01 dormitorio principal, 01 baño principal, 01 walking closet, 02 dormitorios secundarios, baño secundario.</span>
									<dl class="detail">
										<dt class="area">Área construida:</dt><dd><span>93 m<sup>2</sup></span></dd>
										<dt class="area">Terreno:</dt><dd><span>90 m<sup>2</sup></span></dd>
										<dt class="bed">Dormitorios:</dt><dd><span>3</span></dd>
										<dt class="bath">Baños:</dt><dd><span>3</span></dd>
										<dt class="status">Cochera:</dt><dd><span>1</span></dd>
									</dl><!-- /.detail -->
									<div class="footer">
										<a class="btn btn-reverse" href="propiedad-detalle.php?vivienda=misti">Cotizar</a>
									</div>
								</div><!-- /.box-home .box-ads -->
							</div><!-- ./col-md-4 -->
							<div class="col-md-6">
								<div class="box-ads box-home">
									<a class="hover-effect image image-fill" href="propiedad-detalle.php?vivienda=aleli">
										<span class="cover"></span>
										<img alt="Vivienda" src="images/aleli_406x200.jpg">
										<h3 class="title">Casa Aleli 78 m<sup>2</sup></h3>
									</a><!-- /.hover-effect -->
									<span class="price"> Desde S/.187,988</span>
									<span class="address"><i class="fa fa-map-marker"></i> Dos pisos, Yura, Arequipa</span>
									<span class="description"><strong>Primer Nivel:</strong> Sala, Comedor, Cocina, Lavandería, Baño completo, 01 Estudio, Escaleras interiores, Cochera, Áreas verdes.<br><strong>Segundo Nivel:</strong> 03 Dormitorios, Baño completo y Sala de estar.<br>&nbsp;</span>
									<dl class="detail">
										<dt class="area">Area construida:</dt><dd><span>78 m<sup>2</sup></span></dd>
										<dt class="area">Terreno:</dt><dd><span>90 m<sup>2</sup></span></dd>
										<dt class="bed">Dormitorios:</dt><dd><span>3</span></dd>
										<dt class="bath">Baños:</dt><dd><span>2</span></dd>
										<dt class="status">Cochera:</dt><dd><span>1</span></dd>
									</dl><!-- /.detail -->
									<div class="footer">
										<a class="btn btn-reverse" href="propiedad-detalle.php?vivienda=aleli">Cotizar</a>
									</div>
								</div><!-- /.box-home .box-ads -->
							</div><!-- ./col-md-4 -->								
						</div>
						<div class="list-box-title">
							<span><i class="icon fa fa-plus-square"></i>Casas de 1 piso</span>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="box-ads box-home">
									<a class="hover-effect image image-fill" href="propiedad-detalle.php?vivienda=capuli">
										<span class="cover"></span>
										<img alt="Vivienda" src="images/capuli_406x200.jpg">
										<h3 class="title">Casa Capuli 42 m<sup>2</sup></h3>
									</a><!-- /.hover-effect -->
									<span class="price"> Desde S/.135,000</span>
									<span class="address"><i class="fa fa-map-marker"></i> Un piso, Yura, Arequipa</span>
									<span class="description">Sala, comedor, cocina, lavandería, baño completo, 02 dormitorios, terraza, cochera, áreas verdes.</span>
									<dl class="detail">
										<dt class="area">Área construida:</dt><dd><span>42 m<sup>2</sup></span></dd>
										<dt class="area">Terreno:</dt><dd><span>90 m<sup>2</sup></span></dd>
										<dt class="bed">Dormitorios:</dt><dd><span>2</span></dd>
										<dt class="bath">Baños:</dt><dd><span>1</span></dd>
										<dt class="status">Cochera:</dt><dd><span>1</span></dd>
									</dl><!-- /.detail -->
									<div class="footer">
										<a class="btn btn-reverse" href="propiedad-detalle.php?vivienda=capuli">Cotizar</a>
									</div>
								</div><!-- /.box-home .box-ads -->
							</div><!-- ./col-md-4 -->
							<div class="col-md-6">
								<div class="box-ads box-home">
									<a class="hover-effect image image-fill" href="propiedad-detalle.php?vivienda=texao">
										<span class="cover"></span>
										<img alt="Vivienda" src="images/texao_406x200.jpg">
										<h3 class="title">Casa Texao 35 m<sup>2</sup></h3>
									</a><!-- /.hover-effect -->
									<span class="price"> Desde S/.84,100</span>
									<span class="address"><i class="fa fa-map-marker"></i> Un piso, Yura, Arequipa</span>
									<span class="description">Sala, comedor, cocina americana, lavandería, baño completo, 02 dormitorios, terraza, cochera, áreas verdes.</span>
									<dl class="detail">
										<dt class="area">Área construida:</dt><dd><span>35 m<sup>2</sup></span></dd>
										<dt class="area">Terreno:</dt><dd><span>90 m<sup>2</sup></span></dd>
										<dt class="bed">Dormitorios:</dt><dd><span>2</span></dd>
										<dt class="bath">Baños:</dt><dd><span>1</span></dd>
										<dt class="status">Cochera:</dt><dd><span>1</span></dd>
									</dl><!-- /.detail -->
									<div class="footer">
										<a class="btn btn-reverse" href="propiedad-detalle.php?vivienda=texao">Cotizar</a>
									</div>
								</div><!-- /.box-home .box-ads -->
							</div><!-- ./col-md-4 -->								
						</div>						
						<div class="list-box-title">
							<span><i class="icon fa fa-plus-square"></i>Tipo de departamentos</span>
						</div>						
						<div class="row">											
							<div class="col-md-6">
								<div class="box-ads box-home">
									<a class="hover-effect image image-fill" href="propiedad-detalle.php?vivienda=wititi">
										<span class="cover"></span>
										<img alt="Vivienda" src="images/wititi_406x200.jpg">
										<h3 class="title">Dpto. Wititi 53 m<sup>2</sup></h3>
									</a><!-- /.hover-effect -->
									<span class="price"> Precio S/.125,900</span>
									<span class="address"><i class="fa fa-map-marker"></i> Yura, Arequipa</span>
									<span class="description">Sala, comedor, cocina, lavandería, baño completo, 03 dormitorios.</span>
									<dl class="detail">
										<dt class="area">Área:</dt><dd><span>53 m<sup>2</sup></span></dd>
										<dt class="bed">Dormitorios:</dt><dd><span>3</span></dd>
										<dt class="bath">Baños:</dt><dd><span>1</span></dd>
									</dl><!-- /.detail -->
									<div class="footer">
										<a class="btn btn-reverse" href="propiedad-detalle.php?vivienda=wititi">Cotizar</a>
									</div>
								</div><!-- /.box-home .box-ads -->
							</div><!-- ./col-md-4 -->
							<div class="col-md-6">
								<div class="box-ads box-home">
									<a class="hover-effect image image-fill" href="propiedad-detalle.php?vivienda=yaravi">
										<span class="cover"></span>
										<img alt="Vivienda" src="images/yaravi_406x200.jpg">
										<h3 class="title">Dpto. Yaravi 46 m<sup>2</sup></h3>
									</a><!-- /.hover-effect -->
									<span class="price"> Precio S/.105,000</span>
									<span class="address"><i class="fa fa-map-marker"></i> Yura, Arequipa</span>
									<span class="description">Sala, comedor, cocina, lavandería, baño completo, 02 dormitorios.</span>
									<dl class="detail">
										<dt class="area">Área:</dt><dd><span>46 m<sup>2</sup></span></dd>
										<dt class="bed">Dormitorios:</dt><dd><span>2</span></dd>
										<dt class="bath">Baños:</dt><dd><span>1</span></dd>										
									</dl><!-- /.detail -->
									<div class="footer">
										<a class="btn btn-reverse" href="propiedad-detalle.php?vivienda=yaravi">Cotizar</a>
									</div>
								</div><!-- /.box-home .box-ads -->
							</div><!-- ./col-md-4 -->								
						</div>
						
					</div>
					<div class="col-sm-4 col-md-3">					
						<?php require 'include/sidebar.php'; ?>
					</div>
				</div>
			</div>
		</section>


		<section id="service" class="section-color">
			
			<div class="container">				
				<div class="section-detail">
					<h1>Como adquirir tu vivienda</h1>
					<h2>¿Sabes los beneficios que te da comprar tu inmueble mediante Mi Vivienda o Techo Propio?</h2>		
				</div>			
				<div class="row" style="margin-top:-30px;">					
					<div class="col-md-6">
						<div class="content-box">
							<i class="icon-box icon1"></i>
							<h3>
								<i class="icon-quote fa fa-quote-left"></i>
								Mi Vivienda
								<i class="icon-quote fa fa-quote-right"></i>
							</h3>
							<h4>En GPR puedes acceder al financiamiento del Crédito Mi Vivienda de forma rápida y segura, obteniendo los beneficios de esta, como el Bono al Buen Pagador (BBP) que es un bono para facilitar la compra de tu vivienda, ya sea una casa o un departamento.</h4>
						</div>
					</div>
					<div class="col-md-6">
						<div class="content-box">
							<i class="icon-box icon2"></i>
							<h3>
								<i class="icon-quote fa fa-quote-left"></i>
								Techo Propio
								<i class="icon-quote fa fa-quote-right"></i>
							</h3>
							<h4>Puedes acceder al programa Techo Propio igual de rápido y fácil, siendo uno de los programas que más beneficios te dan, como es el Bono Familiar Habitacional (BFH) que es un subsidio otorgado por el estado a las familias que tienen como meta tener una vivienda.</h4>
						</div>
					</div>
				</div>
			</div>
		</section>


		<section id="recent-news">			
			<div class="container" id="blog">				
				<div class="section-detail">
					<h1>Noticias</h1>
					<h2>Somos GPR! Venimos construyendo hogares y realizando metas a lo largo de más de 40 años, brindando calidad y comodidad para todas las familias en cada proyecto.</h2>					
				</div>			
				<div class="row" style="padding-top:30px;">
					<div class="col-md-6">
						<div class="blog-list masonry-post">
							<h2 class="title"><a href="blog-que-es-nuevo-credito-mi-vivienda.php">Mi Vivienda</a></h2>
							<div class="image">
								<img class="img-responsive" alt="GPR" src="images/blog-11.jpg">
								<div class="social">
									<span class="date">&nbsp;<span>&nbsp;</span></span>
								</div>
							</div>
							<div class="text">
								<h3 class="subtitle">¿Qué es Nuevo Crédito MI VIVIENDA?</h3>
								Es un préstamos hipotecario que facilita la compra de una vivienda completa, finalizada, en proceso de constucción o el proyecto. Así mismo, la vivienda puede ser de primera venta o usada; en donde el valor mínimo de dicho inmueble sea de S/ 58,800 hasta los S/ 419,600.
								<br><br><a href="blog-que-es-nuevo-credito-mi-vivienda.php" class="btn btn-default button-read">Leer m&aacute;s</a>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="blog-list masonry-post">
							<h2 class="title"><a href="blog-nuevo-credito-mi-vivienda-beneficios.php">Beneficios</a></h2>
							<div class="image">
								<img class="img-responsive" alt="GPR" src="images/blog-12b.jpg">
								<div class="social">
									<span class="date">&nbsp;<span>&nbsp;</span></span>									
								</div>
							</div>
							<div class="text">
								<h3 class="subtitle">Conoce los beneficios del Nuevo Crédito MIVIVIENDA</h3>
								El beneficio principal que brinda Nuevo Crédito Mi Vivienda es el Bono al Buen Pagador, que vendría a ser un bono de hasta S/. 17,700 que es otorgado para poder adquirir una casa o departamento.<br />&nbsp;
								<br><br><a href="blog-nuevo-credito-mi-vivienda-beneficios.php" class="btn btn-default button-read">Leer m&aacute;s</a>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="blog-list masonry-post">
							<h2 class="title"><a href="blog-no-tengo-boleta-de-pago-podre-acceder-a-un-credito.php">REQUISITOS</a></h2>
							<div class="image">
								<img class="img-responsive" alt="GPR" src="images/blog-13.jpg">
								<div class="social">
									<span class="date">&nbsp;<span>&nbsp;</span></span>									
								</div>
							</div>
							<div class="text">
								<h3 class="subtitle">No tengo boleta de pago ¿Podré acceder a un crédito?</h3>
								Olvídate de las trabas. Acceder a un préstamo sin contar con un trabajo formal es una realidad.
								<br><br><a href="blog-no-tengo-boleta-de-pago-podre-acceder-a-un-credito.php" class="btn btn-default button-read">Leer m&aacute;s</a>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="blog-list masonry-post">
							<h2 class="title"><a href="encuentra-tu-casa-propia-en-las-lomas-de-yura.php">LAS LOMAS DE YURA</a></h2>
							<div class="image">
								<img class="img-responsive" alt="GPR" src="images/blog-14.jpg">
								<div class="social">
									<span class="date">&nbsp;<span>&nbsp;</span></span>									
								</div>
							</div>
							<div class="text">
								<h3 class="subtitle">Encuentra tu casa propia en Las Lomas de Yura</h3>
								Entre los bienes que tiene una persona a lo largo de su vida, podría decirse que lo más importante es una vivienda.
								<br><br><a href="encuentra-tu-casa-propia-en-las-lomas-de-yura.php" class="btn btn-default button-read">Leer m&aacute;s</a>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</section>


<?php require 'include/footer.php'; ?>