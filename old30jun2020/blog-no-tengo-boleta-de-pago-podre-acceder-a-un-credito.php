<?php require 'include/header.php'; ?>


		<section id="agent-page" class="header-margin-base fixed-no-header">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-sm-8 col-md-8 col-sm-push-4">
								<h1 class="name">No tengo boleta de pago ¿Podré acceder a un crédito?</h1>
								<div class="bs-callout callout-info">
									<h4 class="title">Olvídate de las trabas. Acceder a un préstamo sin contar con un trabajo formal es una realidad.</h4>									
								</div>
								<p class="text">¿Cuentas con un trabajo pero no sabes cómo sustentarlo? Está claro que la mayoría de los trabajadores peruanos no recibe una boleta de pago mensual, requisito básico que piden los bancos para acceder a un crédito hipotecario.</p>
								<p class="text">Sin embargo, no tienes de qué preocuparte. Las Sociedades Financieras brindan las facilidades para aquellos consumidores de nivel socioeconómico C y D que no tienen posibilidad de estar en planilla.</p>								
							</div><!-- /.col-md-8 -->
							<div class="col-sm-4 col-md-4 col-sm-pull-8">																
								<?php require 'include/avance-de-obra.php'; ?>
							</div><!-- /.col-md-4 -->							
						</div><!-- /.row -->
					</div>
					<div class="col-sm-12 col-md-3">
						<!-- ===================== 
								  SEARCH 
						====================== -->
						<div class="section-title line-style no-margin">
							<h3 class="title">Cotizar Vivienda</h3>
						</div>
						<div class="right-box no-margin">
							<div class="row">							
								<?php require 'include/form-cotizar.php'; ?>								
							</div><!-- ./row 2 -->	
						</div><!-- ./search -->

					</div><!-- ./col-md-3 -->
				</div><!-- ./row -->

				<br /><br /><br /> 
				
				<div class="section-title line-style no-margin">
					<h3 class="title">Elige tu nuevo hogar</h3>
				</div>

				<div class="my-property" data-navigation=".my-property-nav">
					<div class="crsl-wrap">
						<?php require 'include/grid-propiedades.php'; ?>					
					</div>
					<div class="my-property-nav">
						<p class="button-container">
							<a href="#" class="next">siguiente</a>
							<a href="#" class="previous">anterior</a>
						</p>
					</div>
				</div><!-- /.my-property slide -->

			</div><!-- ./container -->
		</section><!-- /#about-us -->





<?php require 'include/footer.php'; ?>