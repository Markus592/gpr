<?php require 'include/header.php'; ?>


		<section id="agent-page" class="header-margin-base fixed-no-header">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-sm-8 col-md-8 col-sm-push-4">
							
							
								<h1 class="name">¡Adquiere tu vivienda, te ayudamos!</h1>
								<div class="bs-callout callout-info">
									<h4 class="title">La crisis ha puesto en evidencia nuevas necesidades en la vivienda: habitaciones para teletrabajo, posibilidad de aislar a uno de los habitantes y zonas verdes. El proyecto Lomas de Yura te ofrece estos espacios. Para reducir el impacto de la crisis sanitaria y ayudar a la población, el Gobierno emitió el Decreto Legislativo Nº 1464 a través del cual amplía los beneficios del programa Techo Propio.</h4>
								</div>								
								<p class="text">Techo Propio es un programa del Estado para promover la compra de viviendas. Este bono es totalmente gratuito, pero hay que cumplir una serie de requisitos como tener un ahorro mínimo. Esta nueva norma excluye a los beneficiarios (hasta el 31 de diciembre de 2020) de cumplir este requerimiento.</p>
								<p class="text">Debido al estado de emergencia y a la cuarentena impuesta por el Gobierno, muchos de los potenciales beneficiarios del Bono Familiar Habitacional (BFH) han tenido que disponer de sus recursos, lo que incrementa su situación de vulnerabilidad. Por este motivo, el Ministerio de Vivienda Construcción y Saneamiento (MVCS) aumentó el BFH que se otorga para las modalidades de adquisición de vivienda nueva o construcción en terreno propio, en el marco de los programas de vivienda Techo Propio.</p>
								<p class="text">La nueva norma señala que “es necesario que se adopten medidas que permitan que todos puedan acceder a una vivienda adecuada que les de cobijo y protección, para que a través de Techo Propio, el cual se desarrolla con el otorgamiento del BHF, se establezcan disposiciones que faciliten su atención”.</p>
								<p class="text">Según el portal del Fondo Mivivienda, los nuevos valores del BFH serán los siguientes:</p>
								<p class="text"></p>
								<ul>
									<li><b>BFH en la modalidad de aplicación de Construcción en Terreno Propio:</b> S/ 25,800.</li>
									<li><b>BFH en la modalidad de aplicación de Adquisición de Vivienda Nueva:</b> S/ 37,625.</li>
								</ul>
								<p class="text">Lo primero que debes hacer para acceder al programa Techo Propio es registrar a tu Grupo Familiar en cualquiera de las oficinas o centros autorizados a nivel nacional, que encontrarás en la página www.mivivienda.com.pe.</p>
								<p class="text">Allí deberás consignar el DNI del jefe de familia y cónyuge, además de los datos de todos los miembros del Grupo Familiar (nombre, DNI, fecha de nacimiento).</p>
								
								
							</div><!-- /.col-md-8 -->
							<div class="col-sm-4 col-md-4 col-sm-pull-8">																
								<?php require 'include/avance-de-obra.php'; ?>
							</div><!-- /.col-md-4 -->							
						</div><!-- /.row -->
					</div>
					<div class="col-sm-12 col-md-3">
						<!-- ===================== 
								  SEARCH 
						====================== -->
						<div class="section-title line-style no-margin">
							<h3 class="title">Cotizar Vivienda</h3>
						</div>
						<div class="right-box no-margin">
							<div class="row">							
								<?php require 'include/form-cotizar.php'; ?>								
							</div><!-- ./row 2 -->	
						</div><!-- ./search -->

					</div><!-- ./col-md-3 -->
				</div><!-- ./row -->

				<br /><br /><br /> 
				
				<div class="section-title line-style no-margin">
					<h3 class="title">Elige tu nuevo hogar</h3>
				</div>

				<div class="my-property" data-navigation=".my-property-nav">
					<div class="crsl-wrap">
						<?php require 'include/grid-propiedades.php'; ?>					
					</div>
					<div class="my-property-nav">
						<p class="button-container">
							<a href="#" class="next">siguiente</a>
							<a href="#" class="previous">anterior</a>
						</p>
					</div>
				</div><!-- /.my-property slide -->

			</div><!-- ./container -->
		</section><!-- /#about-us -->





<?php require 'include/footer.php'; ?>