<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="google-site-verification" content="QGM-H8UXKi1bZOXzRIfOtBmaEF_0OI8CVP8IWqBNw-c" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
	<meta name="author" content="ID MEDIA" />
	<meta name="description" content="GPR INMOBILIARIA, Construyendo calidad de vida, Proyecto Las Lomas de Yura, Km 17 de la carretera Arequipa Puno, sector Las Laderas, distrito de Yura AREQUIPA">
    <title>GPR INMOBILIARIA | Proyecto: Las Lomas de Yura</title>

	<!-- Global site tag (gtag.js) - Google Ads: 728315411 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-728315411"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-728315411'); </script> 

	<?php
	if( $_REQUEST['send'] == "ok" ){?>
		<!-- Event snippet for GRACIAS conversion page --> <script> gtag('event', 'conversion', {'send_to': 'AW-728315411/clsECMH2mqgBEJPspNsC'}); </script> 
		<?php
	}?>
	
	<!-- Facebook Pixel Code colocado el 22jun2020 -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '891375301380000');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=891375301380000&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->


	<!-- Facebook Pixel Code antiguo-->
    <!--<script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '335978600719686');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=335978600719686&ev=PageView&noscript=1"
    /></noscript>-->
    <!-- End Facebook Pixel Code -->
	

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-K8B7F5Q');</script>
	<!-- End Google Tag Manager -->

	<link rel="stylesheet" href="css/bootstrap.min.css">                            <!-- Bootstrap -->
    <link rel="stylesheet" href="css/vendor/font-awesom/css/font-awesome.min.css">  <!-- Font Awesome -->
	<link rel="stylesheet" href="css/vendor/mmenu/jquery.mmenu.all.css" />          <!-- Menu Responsive -->
	<link rel="stylesheet" href="css/vendor/animate-wow/animate.css">               <!-- Animation WOW -->

    <link rel="stylesheet" href="css/vendor/labelauty/labelauty.css">               <!-- Checkbox form Style -->
	<link rel="stylesheet" href="css/vendor/nouislider/nouislider.min.css">         <!-- Slider Price -->
    <link rel="stylesheet" href="css/vendor/easydropdown/easydropdown.css">         <!-- Select form Style -->
    <link rel="stylesheet" href="css/ui-spinner.css">                               <!-- Spinner -->

	<link rel="stylesheet" href="css/menu.css">                                     <!-- Include Menu stylesheet -->
	<link rel="stylesheet" href="css/custom.css">                                   <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="css/media-query.css">                              <!-- Media Query -->

	<!------- *** ----->
	<link rel="stylesheet" href="css/vendor/fotorama/fotorama.css">                 <!-- Fotorama Gallery Effect -->
	
	<?php
	if( $_REQUEST['vivienda'] != "" ){?>
		<!-- Start: propiedad-detalle.php -->
		<link rel="stylesheet" href="css/vendor/labelauty/labelauty.css">               <!-- Checkbox form Style -->		
		<!-- End: propiedad-detalle.php -->
		<?php
	}?>
	

	<!-- Use Iconifyer to generate all the favicons and touch icons you need: http://iconifier.net -->
	<link rel="shortcut icon" href="images/favicon/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" href="images/favicon/apple-touch-icon.png" />
	<link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-touch-icon-152x152.png" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script src="script/modernizr.min.js"></script> <!-- Modernizr -->

  </head>
  <body class="fixed-header">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K8B7F5Q"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->


	<div id="page-container">
	
		<header class="menu-base" id="header-container-box">
			<div class="info"><!-- info -->
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<a href="#mobile-menu" id="mobile-menu-button" class="visible-xs"><i class="fa fa-bars"></i></a>
							<a href="call:+51933876285" class="hidden-xs"><i class="icon fa fa-phone"></i> 933 876 285</a>
							<a href="mailto:ventas@gprperu.com" data-section="modal-contact" d____ata-target="#modal-contact999999" data-toggle="modal" class="hidden-xs"><i class="icon fa fa-envelope-o"></i> ventas@gprperu.com</a>
						</div>
						<div id="login-pan" class="col-md-6 hidden-xs">							
							<a href="index.php" class="hidden-xs"><i class="icon fa fa-home"></i> Inicio</a>
						</div>
					</div>
				</div>			
			</div><!-- /.info -->
			<div class="logo     ">
				<a href="index.php"><img id="logo-header" src="images/logo.png" alt="GPR Inmobiliaria" /></a>
			</div><!-- /.logo -->
			<div class="menu-navbar">
				<div class="container" id="menu-nav">
					<nav id="navigation">
						<ul>
							<li class=""><a href="index.php">Inicio</a></li>
							<li class=""><a href="quienes-somos.php">¿Quiénes Somos?</a></li>
							<li class="has_submenu">
								<a href="#">Elige tu vivienda</a>
								<ul>
									<li><a href="propiedad-detalle.php?vivienda=misti">Casa Misti (93 m<sup>2</sup>)</a></li>
									<li><a href="propiedad-detalle.php?vivienda=aleli">Casa Aleli (78 m<sup>2</sup>)</a></li>									
									<li><a href="propiedad-detalle.php?vivienda=capuli">Casa Capuli (42 m<sup>2</sup>)</a></li>									
									<li><a href="propiedad-detalle.php?vivienda=texao">Casa Texao (35 m<sup>2</sup>)</a></li>									
									<li><a href="propiedad-detalle.php?vivienda=wititi">Dpto. Wititi (53 m<sup>2</sup>)</a></li>									
									<li><a href="propiedad-detalle.php?vivienda=yaravi">Dpto. Yaravi (46 m<sup>2</sup>)</a></li>									
								</ul>
							</li>						
							<li class=""><a href="contacto.php">Nuestras oficinas</a></li>					
														
						</ul>
					</nav>
				</div>
			</div><!-- /.menu -->
			<a href="#" class="fixed-button top"><i class="fa fa-chevron-up"></i></a>
			<!--
			<a href="#" class="hidden-xs fixed-button email" data-toggle="modal" data-target="#modal-contact9999999999999" data-section="modal-contact"><i class="fa fa-envelope-o"></i></a>
			-->
		</header>




