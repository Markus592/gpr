
						<figure class="crsl-item">
							<div class="box-ads box-grid">
								<a class="hover-effect image image-fill" href="propiedad-detalle.php?vivienda=misti">
									<span class="cover"></span>
									<img alt="Vivienda" src="images/misti_408x251.jpg">
									<h3 class="title">Casa Misti 93 m<sup>2</sup></h3>
								</a>
								<span class="price">S/.283,776</span>
								<div class="footer">
									<a class="btn btn-default" href="propiedad-detalle.php?vivienda=misti">Cotizar</a>
								</div>
							</div>
						</figure>
						<figure class="crsl-item">
							<div class="box-ads box-grid">								
								<a class="hover-effect image image-fill" href="propiedad-detalle.php?vivienda=aleli">
									<span class="cover"></span>
									<img alt="Vivienda" src="images/aleli_408x251.jpg">
									<h3 class="title">Casa Aleli 78 m<sup>2</sup></h3>
								</a>
								<span class="price">S/.189,988</span>						
								<div class="footer">
									<a class="btn btn-default" href="propiedad-detalle.php?vivienda=aleli">Cotizar</a>
								</div>
							</div>
						</figure>
						<figure class="crsl-item" >
							<div class="box-ads box-grid">								
								<a class="hover-effect image image-fill" href="propiedad-detalle.php?vivienda=capuli">
									<span class="cover"></span>
									<img alt="Vivienda" src="images/capuli_408x251.jpg">
									<h3 class="title">Casa Capuli 42 m<sup>2</sup></h3>
								</a>
								<span class="price">S/.137,000</span>						
								<div class="footer">
									<a class="btn btn-default" href="propiedad-detalle.php?vivienda=capuli">Cotizar</a>
								</div>
							</div>
						</figure>
						<figure class="crsl-item" >
							<div class="box-ads box-grid">
								<a class="hover-effect image image-fill" href="propiedad-detalle.php?vivienda=texao">
									<span class="cover"></span>
									<img alt="Vivienda" src="images/texao_408x251.jpg">
									<h3 class="title">Casa Texao 35 m<sup>2</sup></h3>
								</a>
								<span class="price">S/.84,100</span>						
								<div class="footer">
									<a class="btn btn-default" href="propiedad-detalle.php?vivienda=texao">Cotizar</a>
								</div>
							</div>
						</figure>
						<figure class="crsl-item" >
							<div class="box-ads box-grid">
								<a class="hover-effect image image-fill" href="propiedad-detalle.php?vivienda=wititi">
									<span class="cover"></span>
									<img alt="Vivienda" src="images/wititi_408x251.jpg">
									<h3 class="title">Dpto. Wititi 53 m<sup>2</sup></h3>
								</a>
								<span class="price">S/.127,900</span>						
								<div class="footer">
									<a class="btn btn-default" href="propiedad-detalle.php?vivienda=wititi">Cotizar</a>
								</div>
							</div>
						</figure>
						<figure class="crsl-item" >
							<div class="box-ads box-grid">
								<a class="hover-effect image image-fill" href="propiedad-detalle.php?vivienda=yaravi">
									<span class="cover"></span>
									<img alt="Vivienda" src="images/yaravi_408x251.jpg">
									<h3 class="title">Dpto. Yaravi 46 m<sup>2</sup></h3>
								</a>
								<span class="price">S/.105,000</span>						
								<div class="footer">
									<a class="btn btn-default" href="propiedad-detalle.php?vivienda=yaravi">Cotizar</a>
								</div>
							</div>
						</figure>
						
						