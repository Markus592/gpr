
					
							
								<FORM method="post" action="contact.php" class="form-large" role="form" data-toggle="validator">								
								<input type="hidden" id="proyecto" name="proyecto" value="LAS LOMAS DE YURA">

								<div class="col-md-12 space-div">
									<select class="dropdown" data-settings='{"cutOff": 7}' name="vivienda" id="vivienda" required >
										<option value="">-- Elige tu vivienda --</option>
										<option value="Casa Misti">Casa Misti (93 m2)</option>
										<option value="Casa Aleli">Casa Aleli (78 m2)</option>
										<option value="Casa Capuli">Casa Capuli (42 m2)</option>
										<option value="Casa Texao">Casa Texao (35 m2)</option>
										<option value="Dpto. Wititi">Dpto. Wititi (53 m2)</option>
										<option value="Dpto. Yaravi">Dpto. Yaravi (46 m2)</option>
									</select>
								</div>
								<div class="col-md-12 space-div">
									<input class="form-control" type="text" name="nombre" id="nombre" placeholder="Nombres" required />
								</div>
								<div class="col-md-12 space-div">
									<input class="form-control" type="text" name="apellido" id="apellido" placeholder="Apellidos" required />
								</div>	
								<div class="col-md-12 space-div">
									<input class="form-control" type="email" name="email" id="email" placeholder="Email" required />
								</div>
								<div class="col-md-12 space-div">
									<input class="form-control" type="text" name="telefono" id="telefono" placeholder="Tel&eacute;fono" required />
								</div>
								<div class="col-md-12 space-div">
									<select class="dropdown" data-settings='{"cutOff": 5}' name="tipodocumento" id="tipodocumento" required >
										<option value="">-- Tipo de documento --</option>
										<option value="DNI" selected="selected">DNI</option>
										<option value="RUC">RUC</option>
										<option value="CE">Carné de extranjería</option>
										<option value="PASAPORTE">Pasaporte</option>                        
									</select>
								</div>
								<div class="col-md-12 space-div">
									<input class="form-control" type="text" name="nrodocumento" id="nrodocumento" placeholder="Nro. de documento" required />
								</div>								
								<div class="col-md-12 space-div">
									<select class="dropdown" data-settings='{"cutOff": 7}' name="ciudad" id="ciudad" required >
										<option value="">-- Ciudad --</option>
										<option value="Amazonas">Amazonas</option>
										<option value="Ancash">Ancash</option>
										<option value="Apurimac">Apurimac</option>
										<option value="Arequipa" selected="selected">Arequipa</option>
										<option value="Ayacucho">Ayacucho</option>
										<option value="Cajamarca">Cajamarca</option>
										<option value="Callao">Callao</option>
										<option value="Cusco">Cusco</option>
										<option value="Huancavelica">Huancavelica</option>
										<option value="Huanuco">Huanuco</option>
										<option value="Ica">Ica</option>
										<option value="Junín">Junín</option>
										<option value="La Libertad">La Libertad</option>
										<option value="Lambayeque">Lambayeque</option>
										<option value="Lima">Lima</option>
										<option value="Loreto">Loreto</option>
										<option value="Madre de Dios">Madre de Dios</option>
										<option value="Moquegua">Moquegua</option>
										<option value="Pasco">Pasco</option>
										<option value="Piura">Piura</option>
										<option value="Puno">Puno</option>
										<option value="San Martin">San Martin</option>
										<option value="Tacna">Tacna</option>
										<option value="Tumbes">Tumbes</option>
										<option value="Ucayali">Ucayali</option>
									</select>
								</div>
								<div class="col-md-12 space-div">									
									<textarea class="form-control" rows="3" name="mensaje" id="mensaje" placeholder="Consultas y/o comentarios"></textarea>
								</div>							
								<div class="col-md-12 space-div">									
									<input id="submit" name="submit" type="submit" value="COTIZAR" class="btn btn-default">
								</div>
								</FORM>
								