<?php require 'include/header.php'; ?>


		<section id="agent-page" class="header-margin-base fixed-no-header">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-sm-8 col-md-8 col-sm-push-4">
								<h1 class="name">Financiar tu nuevo hogar es más sencillo con Crédito Mi Vivienda</h1>
								<div class="bs-callout callout-info">
									<h4 class="title">Cada vez más peruanos apuestan por la compra de una casa o departamento, por ende, necesitan de apoyos financieros que hagan esto posible.</h4>
									<p class="text">Es por eso que vemos importante que se sepa qué es Nuevo Crédito MI VIVIENDA y en qué consiste.</p>
								</div>
								<div class="section-title"><h3 class="title">¿Qué es Nuevo Crédito MI VIVIENDA?</h3></div>
								<p>Es un préstamo hipotecario que facilita la compra de una vivienda completa, finalizada, en proceso de constucción o el proyecto. Así mismo, la vivienda puede ser de primera venta o usada; en donde el valor mínimo de dicho inmueble sea de S/ 58,800 hasta los S/ 419,600.</p>
								<div class="section-title"><h3 class="title">¿Por qué elegir Nuevo Crédito MI VIVIENDA?</h3></div>
								<p>El beneficio principal que brinda Nuevo Crédito Mi Vivienda es el Premio al Buen Pagador, que vendría a ser un descuento de S/. 17,700 o S/. 6,400, que es otorgado como un premio a la puntualidad en el pago de las cuotas mensuales.</p>
								<div class="section-title"><h3 class="title">¿Cuáles son los requisitos que necesito?</h3></div>
								<p>Los requisitos a cumplir para acceder a Nuevo Crédito Mi Vivienda son los siguientes:</p>
								<ul>
									<li>Ser <b>mayor de edad</b>.</li>
									<li>Ser <b>calificado</b> por la <b>Entidad Financiera</b> para el crédito MIVIVIENDA.</li>
									<li>No tener <b>ningún crédito pendiente de pago con el FMV</b>.</li>
									<li><b>No ser propietario o copropietario</b> de otra vivienda a nivel nacional.</li>
									<li>Contar con una <b>cuota inicial mínima del 10%</b> del valor de la vivienda que vas a comprar.</li>
								</ul>
								<p>Para acceder a este crédito hipotecario es muy sencillo y útil, ya que el trámite es corto y los beneficios son muchos.</p>
							</div><!-- /.col-md-8 -->
							<div class="col-sm-4 col-md-4 col-sm-pull-8">																
								<?php require 'include/avance-de-obra.php'; ?>
							</div><!-- /.col-md-4 -->							
						</div><!-- /.row -->
					</div>
					<div class="col-sm-12 col-md-3">
						<!-- ===================== 
								  SEARCH 
						====================== -->
						<div class="section-title line-style no-margin">
							<h3 class="title">Cotizar Vivienda</h3>
						</div>
						<div class="right-box no-margin">
							<div class="row">							
								<?php require 'include/form-cotizar.php'; ?>								
							</div><!-- ./row 2 -->	
						</div><!-- ./search -->

					</div><!-- ./col-md-3 -->
				</div><!-- ./row -->

				<br /><br /><br /> 
				
				<div class="section-title line-style no-margin">
					<h3 class="title">Elige tu nuevo hogar</h3>
				</div>

				<div class="my-property" data-navigation=".my-property-nav">
					<div class="crsl-wrap">
						<?php require 'include/grid-propiedades.php'; ?>					
					</div>
					<div class="my-property-nav">
						<p class="button-container">
							<a href="#" class="next">siguiente</a>
							<a href="#" class="previous">anterior</a>
						</p>
					</div>
				</div><!-- /.my-property slide -->

			</div><!-- ./container -->
		</section><!-- /#about-us -->





<?php require 'include/footer.php'; ?>