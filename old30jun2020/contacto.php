<?php require 'include/header.php'; ?>
		
		<section id="header-page" class="header-margin-base">
			<div id="map-canvas" class="header-map"></div>
			<div id="breadcrumb">
				<div class="container">
					<ol class="breadcrumb">
						<li><a href="#"><i class="fa fa-home"></i></a></li>
						<li><a href="#">Inicio</a></li>
						<li class="active">Contacto</li>
					</ol>
				</div>
			</div><!-- /#breadcrumb -->
			<span class="cover"></span>
		</section><!-- /#header -->
		
		<section id="property-content" style="padding-top:30px;">
			<div class="container">
				<div class="row">
					<div class="col-md-9">

						<!-- /.Secondo Row -->
						<div class="row">
							<div class="col-md-4">
								<!-- 9. Mortage -->
								<div class="section-title line-style">
									<h3 class="title">Cotizar Vivienda</h3>
								</div>
								<div class="search-box-page">
									<div class="row">
										<?php require 'include/form-cotizar.php'; ?>
									</div><!-- ./row -->
								</div><!-- ./.search -->								
							</div>
							<div class="col-md-8">							
								<!-- 8. Maps -->
								<div class="section-title line-style">
									<h3 class="title">Visítanos</h3>
								</div>
								<?php require 'include/oficinas-listado.php'; ?>								
							</div>
						</div>
						
					</div>
					<div class="col-md-3">
						<?php require 'include/visita-guiada.php'; ?>						
					</div>
				</div>
			</div>
		</section>
		

<?php require 'include/footer.php'; ?>