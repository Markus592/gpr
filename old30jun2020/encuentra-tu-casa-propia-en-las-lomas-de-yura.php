<?php require 'include/header.php'; ?>


		<section id="agent-page" class="header-margin-base fixed-no-header">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-sm-8 col-md-8 col-sm-push-4">
								<h1 class="name">Encuentra tu casa propia en Las Lomas de Yura</h1>
								<div class="bs-callout callout-info">
									<h4 class="title">Entre los bienes que tiene una persona a lo largo de su vida, podría decirse que lo más importante es una vivienda. Un hogar es más que un simple inmueble y es el sentido de propiedad lo que realmente nos da tranquilidad para el futuro.</h4>
								</div>
								<p class="text">Una casa no es solo un techo que nos protege, es el lugar donde pasamos la mayor parte de nuestra vida, donde vivimos plenos con nuestra familia y el sitio donde más seguros nos sentimos. Ser propietario de una vivienda es, muchas veces, un sueño.</p>
								<p class="text">Que se cumpla tu sueño parece difícil, pero en Lomas de Yura hemos diseñado 2,157 viviendas acogidas al Fondo MiVivienda. Este bono te ayuda a la compra de tu casa y es un beneficio para que toda la población tenga un espacio propio con buenos servicios para vivir.</p>
								<p class="text">El proyecto será construido en siete etapas. En estos momentos nos encontramos en la primera, donde se entregarán 288 casas. Todas están adaptadas a las distintas necesidades de cada familia y todas cuentan con un factor común: lograr el bienestar y confort de sus dueños.</p>
								<p class="text">Las viviendas disponen de estacionamiento, zonas comunes y zonas verdes. Todos los espacios han sido pensados para disfrutar de momentos únicos con la familia y amigos. Como nos gusta cuidar hasta el mínimo detalle de tu futuro hogar, las viviendas cuentan con sala, comedor, habitaciones cómodas y espacios para estacionar el auto en la puerta. </p>
								<p class="text">El proyecto Las Lomas de Yura, se encuentra en una zona muy bien comunicada: en el Km 17 de la carretera Arequipa - Puno, sector Las Laderas, distrito de Yura. Muy cerca de la ciudad y todos sus servicios pero alejada del ruido.</p>
								<p class="text">Tener un título de propiedad es necesario e importante, ya que te otorga seguridad jurídica como propietario de la vivienda, lo que realmente representa y valida tu inversión. Además de evitar conflictos, favorece tu historial crediticio y con tranquilidad tus descendientes la podrán heredar.  </p>
								<p class="text">Elige la casa que mejor se adapte a tu estilo de vida. Puedes adquirirla con el 10% del valor de la vivienda que más te guste. Entra a a nuestra web, ingresa tus datos ¡y uno de nuestros asesores te contactará!</p>								
							</div><!-- /.col-md-8 -->
							<div class="col-sm-4 col-md-4 col-sm-pull-8">																
								<?php require 'include/avance-de-obra.php'; ?>
							</div><!-- /.col-md-4 -->							
						</div><!-- /.row -->
					</div>
					<div class="col-sm-12 col-md-3">
						<!-- ===================== 
								  SEARCH 
						====================== -->
						<div class="section-title line-style no-margin">
							<h3 class="title">Cotizar Vivienda</h3>
						</div>
						<div class="right-box no-margin">
							<div class="row">							
								<?php require 'include/form-cotizar.php'; ?>								
							</div><!-- ./row 2 -->	
						</div><!-- ./search -->

					</div><!-- ./col-md-3 -->
				</div><!-- ./row -->

				<br /><br /><br /> 
				
				<div class="section-title line-style no-margin">
					<h3 class="title">Elige tu nuevo hogar</h3>
				</div>

				<div class="my-property" data-navigation=".my-property-nav">
					<div class="crsl-wrap">
						<?php require 'include/grid-propiedades.php'; ?>					
					</div>
					<div class="my-property-nav">
						<p class="button-container">
							<a href="#" class="next">siguiente</a>
							<a href="#" class="previous">anterior</a>
						</p>
					</div>
				</div><!-- /.my-property slide -->

			</div><!-- ./container -->
		</section><!-- /#about-us -->





<?php require 'include/footer.php'; ?>