<?php require 'include/header.php'; ?>


		<section id="agent-page" class="header-margin-base fixed-no-header">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-sm-8 col-md-8 col-sm-push-4">
								<h1 class="name">Visión & Misión</h1>
								<span class="text">
									GPR tiene el compromiso de crecer en forma responsable y segura. En un ambiente laboral estimulante, participativo y cruzado por valores definidos, siendo los más importantes.
								</span>
								<div class="bs-callout callout-info">
								  <h4 class="title">El respeto, la honestidad y el compromiso.</h4>
								  <p class="text">GPR se propone alcanzar el liderazgo en el mercado inmobiliario de viviendas económicas a lo largo de Perú, usando la tecnología y el máximo de las capacidades de la organización para entregar calidad de vida y mayor valor a nuestros clientes.</p>
								</div>							
								<div class="section-title">
									<h3 class="title">Nuestro Equipo</h3>
								</div>
								<p>Desde sus inicios la empresa ha tenido especial preocupación por el equipo de trabajo, por lo que ha hecho continuos esfuerzos para cuidar el ambiente laboral, tener una política clara de incentivos y ofrecer a sus empleados espacios de encuentro y capacitación. Una vez al año se realiza el encuentro GPR que reúne a nuestros trabajadores de todo el país, en el que se combinan espacios de capacitación junto a momentos de entretenimiento y conocimiento entre el equipo.</p>
								<p>Con una planta de trabajadores al servicio de la construcción, hemos ido trabajando y desarrollando sistemas constructivos con el fin de mejorar la calidad de nuestras viviendas y seguir construyendo calidad de vida a lo largo de Perú.</p>								
							</div><!-- /.col-md-8 -->
							<div class="col-sm-4 col-md-4 col-sm-pull-8">																
								<?php require 'include/avance-de-obra.php'; ?>
							</div><!-- /.col-md-4 -->
						</div><!-- /.row -->
					</div>
					<div class="col-sm-12 col-md-3">
						<!-- ===================== 
								  SEARCH 
						====================== -->
						<div class="section-title line-style no-margin">
							<h3 class="title">Cotizar Vivienda</h3>
						</div>
						<div class="right-box no-margin">
							<div class="row">							
								<?php require 'include/form-cotizar.php'; ?>								
							</div><!-- ./row 2 -->	
						</div><!-- ./search -->

					</div><!-- ./col-md-3 -->
				</div><!-- ./row -->

				<div class="section-title line-style no-margin">
					<h3 class="title">Elige tu nuevo hogar</h3>
				</div>

				<div class="my-property" data-navigation=".my-property-nav">
					<div class="crsl-wrap">
						<?php require 'include/grid-propiedades.php'; ?>					
					</div>
					<div class="my-property-nav">
						<p class="button-container">
							<a href="#" class="next">siguiente</a>
							<a href="#" class="previous">anterior</a>
						</p>
					</div>
				</div><!-- /.my-property slide -->

			</div><!-- ./container -->
		</section><!-- /#about-us -->





<?php require 'include/footer.php'; ?>