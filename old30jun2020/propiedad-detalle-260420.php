<?php require 'include/header.php'; ?>


		<section id="property-content">

			<div class="container">
				<div class="row">
					<div class="col-md-9">

						<!-- 2. Price -->
						<span class="large-price">														
							<?php
							switch ($_REQUEST['vivienda']) {
								case 'misti':
									echo '<sup><small><small><small>Desde </small></small></small></sup>S/. 281,776';
									break;		
								case 'aleli':
									echo '<sup><small><small><small>Desde </small></small></small></sup>S/. 187,988';
									break;		
								case 'capuli':
									echo '<sup><small><small><small>Desde </small></small></small></sup>S/. 135,000';
									break;		
								case 'texao':
									echo 'S/. 84,100';
									break;		
								case 'wititi':
									echo 'S/. 125,900';
									break;		
								case 'yaravi':
									echo 'S/. 105,000';
									break;		
								default:												
									break;
							}//end switch										
							?>	
						</span>

						<!-- 1. Images gallery -->
						<div class="fotorama" data-autoplay="3000" data-stopautoplayontouch="false" data-width="100%" data-fit="cover" data-max-width="100%" data-nav="thumbs" data-transition="crossfade">
						
							<?php
							switch ($_REQUEST['vivienda']) {
								case 'misti':
									echo '
										<img src="images/misti-11.jpg" alt="Misti">
										<img src="images/misti-12.jpg" alt="Misti">
										<img src="images/misti-13.jpg" alt="Misti">
										<img src="images/misti-14.jpg" alt="Misti">
										<img src="images/misti-15.jpg" alt="Misti">
										<img src="images/misti-16.jpg" alt="Misti">
										<img src="images/misti-17.jpg" alt="Misti">									
										<img src="images/misti-1_850x510.jpg" alt="Misti">
										<img src="images/misti-2_850x510.jpg" alt="Misti">	
										<img src="images/misti-3.jpg" alt="Misti">
										<img src="images/aleli-6_850x510.jpg" alt="Misti">										
										<img src="images/misti-6.jpg" alt="Misti">
										<img src="images/capuli-6_850x510.jpg" alt="Misti">
										<img src="images/texao-6_850x510.jpg" alt="Misti">
										<img src="images/wititi-4_850x510.jpg" alt="Misti">
										<img src="images/yaravi-3_850x510.jpg" alt="Misti">
										<img src="images/yaravi-4_850x510.jpg" alt="Misti">										
										<img src="images/misti-1.jpg" alt="Misti">
									';
									break;		
								case 'aleli':
									echo '
										<img src="images/aleli-11.jpg" alt="aleli">
										<img src="images/aleli-12.jpg" alt="aleli">
										<img src="images/aleli-13.jpg" alt="aleli">
										<img src="images/aleli-14.jpg" alt="aleli">
										<img src="images/aleli-15.jpg" alt="aleli">
										<img src="images/aleli-1_850x510.jpg" alt="aleli">
										<img src="images/aleli-6_850x510.jpg" alt="aleli">
										<img src="images/aleli-5.jpg" alt="aleli">
										<img src="images/misti-2_850x510.jpg" alt="aleli">	
										<img src="images/aleli-4.jpg" alt="aleli">										
										<img src="images/capuli-6_850x510.jpg" alt="aleli">
										<img src="images/wititi-4_850x510.jpg" alt="aleli">
										<img src="images/texao-6_850x510.jpg" alt="aleli">
										<img src="images/yaravi-4_850x510.jpg" alt="aleli">
										<img src="images/yaravi-3_850x510.jpg" alt="aleli">										
										<img src="images/aleli-1.jpg" alt="aleli">
									';
									break;		
								case 'capuli':
									echo '
										<img src="images/capuli-11.jpg" alt="capuli">
										<img src="images/capuli-12.jpg" alt="capuli">
										<img src="images/capuli-13.jpg" alt="capuli">																		
										<img src="images/capuli-1_850x510.jpg" alt="capuli">
										<img src="images/capuli-6_850x510.jpg" alt="capuli">
										<img src="images/capuli-3_850x510.jpg" alt="capuli">										
										<img src="images/wititi-4_850x510.jpg" alt="capuli">
										<img src="images/texao-6_850x510.jpg" alt="capuli">
										<img src="images/yaravi-4_850x510.jpg" alt="capuli">
										<img src="images/yaravi-3_850x510.jpg" alt="capuli">
										<img src="images/capuli-2_850x510.jpg" alt="capuli">
									';
									break;		
								case 'texao':
									echo '
										<img src="images/texao-11.jpg" alt="texao">
										<img src="images/texao-12.jpg" alt="texao">
										<img src="images/texao-13.jpg" alt="texao">									
										<img src="images/texao-2_850x510.jpg" alt="texao">
										<img src="images/texao-6_850x510.jpg" alt="texao">
										<img src="images/texao-3_850x510.jpg" alt="texao">										
										<img src="images/capuli-6_850x510.jpg" alt="texao">
										<img src="images/wititi-4_850x510.jpg" alt="texao">
										<img src="images/yaravi-4_850x510.jpg" alt="texao">
										<img src="images/yaravi-3_850x510.jpg" alt="texao">										
										<img src="images/texao-1_850x510.jpg" alt="texao">
									';
									break;		
								case 'wititi':
									echo '
										<img src="images/dpto-11.jpg" alt="wititi">
										<img src="images/dpto-12.jpg" alt="wititi">
										<img src="images/dpto-13.jpg" alt="wititi">
										<img src="images/dpto-14.jpg" alt="wititi">
										<img src="images/dpto-15.jpg" alt="wititi">
										<img src="images/wititi-5_850x510.jpg" alt="wititi">
										<img src="images/capuli-6_850x510.jpg" alt="wititi">
										<img src="images/wititi-6_850x510.jpg" alt="wititi">
										<img src="images/texao-6_850x510.jpg" alt="wititi">										
										<img src="images/wititi-7_850x510.jpg" alt="wititi">
										<img src="images/yaravi-3_850x510.jpg" alt="wititi">										
										<img src="images/wititi-8_850x510.jpg" alt="wititi">
										<img src="images/yaravi-4_850x510.jpg" alt="wititi">										
										<img src="images/wititi-4_850x510.jpg" alt="wititi">
										<img src="images/wititi-1.jpg" alt="wititi">
									';
									break;		
								case 'yaravi':
									echo '
										<img src="images/dpto-11.jpg" alt="wititi">
										<img src="images/dpto-12.jpg" alt="wititi">
										<img src="images/dpto-13.jpg" alt="wititi">
										<img src="images/dpto-14.jpg" alt="wititi">
										<img src="images/dpto-15.jpg" alt="wititi">
										<img src="images/yaravi-5_850x510.jpg" alt="yaravi">										
										<img src="images/yaravi-6_850x510.jpg" alt="yaravi">
										<img src="images/texao-6_850x510.jpg" alt="yaravi">
										<img src="images/yaravi-7_850x510.jpg" alt="yaravi">
										<img src="images/capuli-6_850x510.jpg" alt="yaravi">
										<img src="images/yaravi-3_850x510.jpg" alt="yaravi">
										<img src="images/wititi-4_850x510.jpg" alt="yaravi">
										<img src="images/yaravi-4_850x510.jpg" alt="yaravi">
										<img src="images/yaravi-1.jpg" alt="yaravi">
									';
									break;		
								default:												
									break;
							}//end switch										
							?>							
						</div>

						<!-- /.Secondo Row -->
						<div class="row">
							<div class="col-md-4">
								<!-- 9. Mortage -->
								<div class="section-title line-style">
									<h3 class="title">Cotizar Vivienda</h3>
								</div>
								<div class="search-box-page">
									<div class="row">
										<?php require 'include/form-cotizar.php'; ?>
									</div><!-- ./row -->
								</div><!-- ./.search -->								
							</div>
							<div class="col-md-8">

								<!-- 6. Description -->
								<div class="section-title line-style">
									<h3 class="title">									
										<?php
										switch ($_REQUEST['vivienda']) {
											case 'misti':
												echo 'Casa Misti (93 m<sup>2</sup>)';
												break;		
											case 'aleli':
												echo 'Casa Aleli 78 m<sup>2</sup>';
												break;		
											case 'capuli':
												echo 'Casa Capuli 42 m<sup>2</sup>';
												break;		
											case 'texao':
												echo 'Casa Texao 35 m<sup>2</sup>';
												break;		
											case 'wititi':
												echo 'Dpto. Wititi 53 m<sup>2</sup>';
												break;		
											case 'yaravi':
												echo 'Dpto. Yaravi 46 m<sup>2</sup>';
												break;		
											default:												
												break;
										}//end switch										
										?>	
									</h3>
								</div>
							
							
								<div class="description">								
									<?php
									switch ($_REQUEST['vivienda']) {
										case 'misti':
											echo '<strong>Primer Nivel:</strong> Sala, comedor, cocina, lavandería, baño completo, 01 estudio, escaleras interiores, cochera, áreas verdes.<br /><br /><strong>Segundo Nivel:</strong> 01 dormitorio principal, 01 baño principal, 01 walking closet, 02 dormitorios secundarios, baño secundario.';
											break;		
										case 'aleli':
											echo '<strong>Primer Nivel:</strong> Sala, Comedor, Cocina, Lavandería, Baño completo, 01 Estudio, Escaleras interiores, Cochera, Áreas verdes.<br /><br /><strong>Segundo Nivel:</strong> 03 Dormitorios, Baño completo y Sala de estar.';
											break;
										case 'capuli':
											echo 'Sala, comedor, cocina, lavandería, baño completo, 02 dormitorios, terraza, cochera, áreas verdes.';
											break;		
										case 'texao':
											echo 'Sala, comedor, cocina americana, lavandería, baño completo, 02 dormitorios, terraza, cochera, áreas verdes.';
											break;		
										case 'wititi':
											echo 'Sala, comedor, cocina, lavandería, baño completo, 03 dormitorios.';
											break;		
										case 'yaravi':
											echo 'Sala, comedor, cocina, lavandería, baño completo, 02 dormitorios.';
											break;		
										default:												
											break;
									}//end switch										
									?>									
								</div>
								
								
								<!-- 6. Description -->
								<div class="section-title line-style">
									<h3 class="title">Resumen</h3>
								</div>
								<div class="description">																
									<div class="box-ads box-home">	


										<?php
										switch ($_REQUEST['vivienda']) {
											case 'misti':
												echo '
												<dl class="detail" style="min-height: 150px;">
													<dt class="area" style="background-position: left 0 top -5px;">Nro. de pisos:</dt><dd><span>2</span></dd>
													<dt class="area">Área construida:</dt><dd><span>93 m<sup>2</sup></span></dd>
													<dt class="area">Terreno:</dt><dd><span>90 m<sup>2</sup></span></dd>
													<dt class="bed">Dormitorios:</dt><dd><span>3</span></dd>
													<dt class="bath">Baños:</dt><dd><span>3</span></dd>
													<dt class="status">Cochera:</dt><dd><span>1</span></dd>
												</dl>												
												';
												break;		
											case 'aleli':
												echo '
												<dl class="detail" style="min-height: 150px;">
													<dt class="area" style="background-position: left 0 top -5px;">Nro. de pisos:</dt><dd><span>2</span></dd>
													<dt class="area">Área construida:</dt><dd><span>78 m<sup>2</sup></span></dd>
													<dt class="area">Terreno:</dt><dd><span>90 m<sup>2</sup></span></dd>
													<dt class="bed">Dormitorios:</dt><dd><span>3</span></dd>
													<dt class="bath">Baños:</dt><dd><span>2</span></dd>
													<dt class="status">Cochera:</dt><dd><span>1</span></dd>
												</dl>												
												';
												break;		
											case 'capuli':
												echo '
												<dl class="detail" style="min-height: 150px;">
													<dt class="area" style="background-position: left 0 top -5px;">Nro. de pisos:</dt><dd><span>1</span></dd>
													<dt class="area">Área construida:</dt><dd><span>42 m<sup>2</sup></span></dd>
													<dt class="area">Terreno:</dt><dd><span>90 m<sup>2</sup></span></dd>
													<dt class="bed">Dormitorios:</dt><dd><span>2</span></dd>
													<dt class="bath">Baños:</dt><dd><span>1</span></dd>
													<dt class="status">Cochera:</dt><dd><span>1</span></dd>
												</dl>												
												';
												break;		
											case 'texao':
												echo '
												<dl class="detail" style="min-height: 150px;">
													<dt class="area" style="background-position: left 0 top -5px;">Nro. de pisos:</dt><dd><span>1</span></dd>
													<dt class="area">Área construida:</dt><dd><span>35 m<sup>2</sup></span></dd>
													<dt class="area">Terreno:</dt><dd><span>90 m<sup>2</sup></span></dd>
													<dt class="bed">Dormitorios:</dt><dd><span>2</span></dd>
													<dt class="bath">Baños:</dt><dd><span>1</span></dd>
													<dt class="status">Cochera:</dt><dd><span>1</span></dd>
												</dl>
												';
												break;		
											case 'wititi':
												echo '
												<dl class="detail" style="min-height: 80px;">													
													<dt class="area">Área:</dt><dd><span>53 m<sup>2</sup></span></dd>
													<dt class="bed">Dormitorios:</dt><dd><span>3</span></dd>
													<dt class="bath">Baños:</dt><dd><span>1</span></dd>
												</dl>												
												';
												break;		
											case 'yaravi':
												echo '
												<dl class="detail" style="min-height: 80px;">													
													<dt class="area">Área:</dt><dd><span>46 m<sup>2</sup></span></dd>
													<dt class="bed">Dormitorios:</dt><dd><span>2</span></dd>
													<dt class="bath">Baños:</dt><dd><span>1</span></dd>
												</dl>												
												';
												break;		
											default:												
												break;
										}//end switch										
										?>

									</div>							
								</div>

								<!-- 8. Maps -->
								<div class="section-title line-style">
									<h3 class="title">Visítanos</h3>
								</div>
								<div class="map-container" id="map-canvas"></div>
								<br /><br />
								<?php require 'include/oficinas-listado.php'; ?>								

							</div>
						</div>


					</div>
					<div class="col-md-3">
						<?php require 'include/avance-de-obra.php'; ?>
						<!-- Other property -->
						<div class="section-title line-style line-style">
							<h3 class="title">Otras propiedades</h3>
						</div>						
						<?php require 'include/propiedades-thumbs.php'; ?>
					</div>
				</div>
			</div>
		</section>







<?php require 'include/footer.php'; ?>