<?php require(__DIR__ . "/../include/config.php"); ?>
<?php
define('og_image', 'images/wititi-11.jpg');
define('og_title', '[Casa Wititi] - Las Lomas de Yura -GPR Inmobiliaria Arequipa');
define('og_type', 'website');
define('og_desc', 'Continuamos con el proyecto
En Las Lomas de Yura seguimos avanzando con nuestro proyecto, instalando campamentos para verificar y 	monitorear el proceso de construcción. Estamos muy orgullosos y felices de trabajar de manera 	responsable y consecuente.');
define('keywords', ',Casas en arequipa, arequipa, inmobiliaria, inmobiliaria nexo, condominios, venta mini departamento.');
define('title', 'Depaartamento Wititi' . GPR_TITLE_PROPIEDAD);
define('GPR_SECTION_CLASS','departamento-wititi');
?>
<?php require(__DIR__ . "/../include/header.php"); ?>
<section id="agent-page" class="property-content header-margin-base fixed-no-header page-blog">

    <div class="container">
        <div class="row">
            <div class="col-md-12 heroProducts">

                <!-- 2. Price -->
                <span class="large-price">
                    <sup><small class="little">Desde</small></sup>S/ <?= GPR_PRICE_WITITI_FINAL ?>
                </span>
                <!-- 1. Images gallery -->
                <div class="containerSliderProperty">
                    <div class="fotorama" data-autoplay="3000" data-stopautoplayontouch="false" data-width="100%" data-fit="cover" data-max-width="100%" data-nav="thumbs" data-transition="crossfade">
                        <img src="<?= GPR_ROOT_PATH ?>images/dpto-11.jpg" title="<?GPR_ROOT_PATH?>propiedad/casa/wititi" alt="wititi">
                        <img src="<?= GPR_ROOT_PATH ?>images/dpto-12.jpg" title="<?GPR_ROOT_PATH?>propiedad/casa/wititi" alt="wititi">
                        <img src="<?= GPR_ROOT_PATH ?>images/dpto-13.jpg" title="<?GPR_ROOT_PATH?>propiedad/casa/wititi" alt="wititi">
                        <img src="<?= GPR_ROOT_PATH ?>images/dpto-14.jpg" title="<?GPR_ROOT_PATH?>propiedad/casa/wititi" alt="wititi">
                        <img src="<?= GPR_ROOT_PATH ?>images/dpto-15.jpg" title="<?GPR_ROOT_PATH?>propiedad/casa/wititi" alt="wititi">
                        <img src="<?= GPR_ROOT_PATH ?>images/images/wititi-1.jpg" title="<?GPR_ROOT_PATH?>propiedad/casa/wititi" alt="wititi">
                    </div>
                    <div class="title-absolute">
                        <p>Casa Wititi 53M <sup>2</sup></p>
                    </div>
                </div>

                <p class="descriptionProperty">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus exercitationem autem a delectus nostrum ipsam culpa non cupiditate, pariatur earum vitae quibusdam adipisci, quo recusandae voluptates eveniet, ullam molestias necessitatibus.
                </p>
                <!-- /.Secondo Row -->
                <div class="row">
                    <div class="col-md-4">
                        <!-- 9. Mortage -->
                        <div class="section-title line-style ocultDesktop">
                            <h2 class="title">Cotizar Vivienda</h2>
                        </div>
                        <div class="search-box-page ocultDesktop">
                            <div class="row">
                                <?php require(__DIR__ . "/../include/form-cotizar.php"); ?>
                            </div><!-- ./row -->
                        </div><!-- ./.search -->
                        <div class="section-title line-style">
                            <?php require(__DIR__ . "/../include/avance-de-obra.php"); ?>
                        </div>
                    </div>
                    <div class="col-md-8">

                        <!-- 6. Description -->
                        <div class="section-title line-style">
                            <h1 class="title">
                                Casa Wititi (53 m<sup>2</sup>)
                            </h1>
                        </div>


                        <div class="description">
                            Sala, comedor, cocina, lavandería, baño completo, 03 dormitorios.
                        </div>


                        <!-- 6. Description -->
                        <div class="section-title line-style">
                            <h2 class="title">Resumen</h2>
                        </div>
                        <div class="description">
                            <div class="box-ads box-home">
                                <dl class="detail" style="min-height: 80px;">
                                    <dt class="area">Área:</dt>
                                    <dd><span>53 m<sup>2</sup></span></dd>
                                    <dt class="bed">Dormitorios:</dt>
                                    <dd><span>3</span></dd>
                                    <dt class="bath">Baños:</dt>
                                    <dd><span>1</span></dd>
                                </dl>
                            </div>
                        </div>
                        <!-- 8. Maps -->
                        <div class="section-title line-style">
                            <h2 class="title">Visítanos</h2>
                        </div>
                        <div class="map-container" id="map-canvas"></div>
                        <br /><br />
                        <?php require(__DIR__ . "/../include/oficinas-listado.php"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container ocultResponsive">
        <div class="col-md-12">
                            <!-- 9. Mortage -->
                            <div class="section-title line-style">
                                <h2 class="title">Cotizar Vivienda</h2>
                            </div>
                            <div class="search-box-page">
                                <div class="row">
                                    <?php require(__DIR__ . "/../include/form-cotizar.php"); ?>
                                </div><!-- ./row -->
                            </div><!-- ./.search -->
                            
                        </div>
    </div>
    <br /><br /><br />
    <div class="container">
        <div class="section-title line-style no-margin">
            <h2 class="title">Elige tu nuevo hogar</h2>
        </div>

        <div class="my-property" data-navigation=".my-property-nav">
            <div class="crsl-wrap">
                <?php require(__DIR__ . "/../include/grid-propiedades.php"); ?>
            </div>
            <div class="my-property-nav">
                <p class="button-container">
                    <a href="#" class="next">siguiente</a>
                    <a href="#" class="previous">anterior</a>
                </p>
            </div>
        </div><!-- /.my-property slide -->
    </div><!-- ./container -->
</section>
<?php require(__DIR__ . "/../include/footer2.php"); ?>